\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{The FMO complex and open quantum systems}{3}{0}{1}
\beamer@sectionintoc {2}{Non-equilibrium quantum field theory}{9}{0}{2}
\beamer@sectionintoc {3}{Numerical Tensor Network Method}{14}{0}{3}
\beamer@sectionintoc {4}{Comparsion}{19}{0}{4}
\beamer@sectionintoc {5}{Summary and outlook}{27}{0}{5}
