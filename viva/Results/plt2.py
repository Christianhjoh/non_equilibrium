import h5py
import pickle
from numpy import (array, exp, trace, dot, eye, kron, zeros, 
	real, linspace,std, append, all, shape, mean, std)
from numpy.linalg import eig
from qutip import sigmap, sigmaz, sigmax
from time import time
import matplotlib.pyplot as plt
import mpmath as mp
mp.pretty = True
Temp = [0.4,0.3,0.2]
dataK= h5py.File('Keldysh_data.h5','r')
print(list(dataK.keys()))
TempK = dataK.get('Temp').value
OcDK = dataK.get('OcDif').value
CurK = dataK.get('Cur').value
Oc1K = dataK.get('Oc1').value

N = len(Temp)
data = []
for ii in range(N):
	NAME = '2S2E_weak_R2_T'+str(Temp[ii]).replace('.','')
	data.append(pickle.load(open('Tempo_'+NAME+'_statedat_dkm32prec55.pickle','rb')))

data2 = []
for ii in range(N):
	NAME = '2S1L_weak2_T'+str(Temp[ii]).replace('.','')
	data2.append(pickle.load(open('Tempo_'+NAME+'_statedat_dkm32prec70.pickle','rb')))
	data3 = []
for ii in range(N):
	NAME = '2S1L_weak_T'+str(Temp[ii]).replace('.','')
	data3.append(pickle.load(open('Tempo_'+NAME+'_statedat_dkm32prec70.pickle','rb')))

#Function creating matrix representaion of relevant operators 
iD = eye(2)
sp = sigmap().full()
sp2 = kron(iD,sp)
sp1 = kron(sp,iD)
sz = sigmaz().full()
sx = sigmax().full()
Ex = dot(sp,sp.T)
ET2 = kron(eig(sx)[1],eig(sx)[1])+ 1j*zeros([4,4])
ET = kron(eig(sx)[1],eig(eye(2))[1])+ 1j*zeros([4,4])

h=0.5
def EigT(A,ET):
	return dot(dot(ET,A),ET.T)
Ex1 = kron(Ex,iD)
Ex2 = kron(iD,Ex)
ODif = Ex2-Ex1
Current= -1j *h* (dot(sp1.T,sp2)-dot(sp2.T,sp1))

OcD = []
Oc1 =[]
Cur = []
OcDL = []
Oc1L =[]
Oc2L = []
OcDL2 = []
CurL = []
CurL2 = []
TIME = []
for i in range(N):
	Oc1L.append(real(trace(dot(EigT(Ex1,ET),data2[i][1][-1].reshape([4,4])))))
	OcDL.append(real(trace(dot(EigT(ODif,ET),data2[i][1][-1].reshape([4,4])))))
	CurL.append(real(trace(dot(EigT(Current,ET),data2[i][1][-1].reshape([4,4])))))
	Oc2L.append(real(trace(dot(EigT(Ex1,ET),data3[i][1][-1].reshape([4,4])))))
	OcDL2.append(real(trace(dot(EigT(ODif,ET),data3[i][1][-1].reshape([4,4])))))
	CurL2.append(real(trace(dot(EigT(Current,ET),data3[i][1][-1].reshape([4,4])))))
	Oc1.append([])
	OcD.append([])
	Cur.append([])
	TIME.append([])

for i in range(N):
	tempOc1 =[]
	tempD = []
	tempC = []
	tempt = []
	for j in range(len(data[i][0])):
		tempOc1.append(real(trace(dot(EigT(Ex1,ET2),data[i][1][j].reshape([4,4])))))
		tempD.append(real(trace(dot(EigT(ODif,ET2),data[i][1][j].reshape([4,4])))))
		tempC.append(real(trace(dot(EigT(Current,ET2),data[i][1][j].reshape([4,4])))))
		tempt.append(data[i][0][j])
	Oc1[i].append(array(tempOc1,dtype=float))
	OcD[i].append(array(tempD,dtype=float))
	Cur[i].append(array(tempC,dtype=float))
	TIME[i].append(tempt)

#average values
avgOc1 = []
avgOcDif= []
avgCur = []
ErrOc1 = []
ErrDif = []
ErrCur = []
cutOff = 10
for i in range(N):
	avgOc1.append(mean(Oc1[i][0][-cutOff:]))
	ErrOc1.append(std(Oc1[i][0][-cutOff:]))
	avgOcDif.append(mean(OcD[i][0][-cutOff:]))
	ErrDif.append(std(OcD[i][0][-cutOff:]))
	avgCur.append(mean(Cur[i][0][-cutOff:]))
	ErrCur.append(std(Cur[i][0][-cutOff:]))

#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})
col=['C0--','C1','C3']
# plt.figure(1)
# plt.errorbar(Temp,avgOc1,yerr=ErrOc1,fmt='C0o',label='avg. NTNM')
# plt.ylabel(r'$n_1$')
# plt.xlabel(r'$T_1 \;[\omega_c]$')
# plt.plot(TempK, Oc1K, 'C1',label = 'NEQFT')
# plt.scatter(Temp,Oc1L,c='C0',marker='x',label='NTNM 1 Markov. $(\\omega_c)$')
# plt.scatter(Temp,Oc2L,c='C5',marker='^',label='NTNM 1 Markov.  $(\\omega_c-\\epsilon)$')
# lgd=plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=5, loc=2, borderaxespad=0.5)
# plt.savefig('Oc1.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.figure(2)
plt.errorbar(Temp,avgCur,yerr=ErrCur,fmt='C0o',label='avg. NTNM')
plt.scatter(Temp,CurL2,c='C0',marker='x',label='NTNM: 1 Markov. $(\\omega_c)$')
plt.scatter(Temp,CurL,c='C5',marker='^',label='NTNM: 1 Markov. $(\\omega_c-\\epsilon)$')
plt.ylabel(r'$I\; [\omega_c] $')
plt.xlabel(r'$T_1 \;[\omega_c]$')
plt.plot(TempK, CurK, 'C1',label = 'NEQFT')
lgd=plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=1, loc=2, borderaxespad=0.5)
plt.savefig('Cur.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.figure(3)
plt.errorbar(Temp,avgOcDif,yerr=ErrDif,fmt='C0o',label='avg. NTNM')
plt.ylabel(r'$n_2-n_1$')
plt.xlabel(r'$T_1 \;[\omega_c]$')
plt.plot(TempK, OcDK, 'C1',label = 'NEQFT')
plt.scatter(Temp,OcDL,c='C0',marker='x',label='NTNM: 1 Markov. $(\\omega_c)$')
plt.scatter(Temp,OcDL2,c='C5',marker='^',label='NTNM: 1 Markov. $(\\omega_c-\\epsilon)$')
lgd=plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=1, loc=2, borderaxespad=0.5)
plt.savefig('OcD.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.show()