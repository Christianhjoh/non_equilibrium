\chapter{2-site System}
 
\section{Markovian Theory}
Considering the specific 2LS chain described by the Hamiltonian \eqref{KC_eq:2LS_Hamilton}, it is seen that the environment-system interaction operators ($\sigma_\lambda^x$) do not commute with the chain Hamiltonian ($H_c$). In this case there are no shared eigenstates between the interaction and the system Hamiltonian. This makes it necessary to compute all the projection elements of $\sigma_\lambda^x$.
The chain Hamiltonian for the 2-site system in Fig. \ref{2site_fig:2site} is given by
\begin{equation}
\begin{gathered}
H_c=\omega_c\left(\sigma^+_1\sigma^-_1+\sigma^+_2\sigma^-_2\right)+\epsilon \left(\sigma^+_1 \sigma^-_2+\sigma^+_2 \sigma^-_1 \right),\;H_E=\sum_k\left(\nu_{1,k} d^\dagger_{1,k}d_{1,k}+\nu_{2,k} d^\dagger_{2,k}d_{2,k}\right)\\
H_I=A_1\otimes B_1 +A_2\otimes B_2,\text{ with }A_i=\sigma_i^x \text{ and } B_i=\sum_{k}g_{i,k} \left(d_{i,k}^\dagger+d_{i,k}\right).
\end{gathered}
\end{equation}
The interaction is naturally written in a summation form \eqref{MME_eq:HInt_decomposition} where the different operators are Hermitian. As the two environments are not interacting with each other nor internally, moving to the interaction is a simple procedure using e.g. the spectral theorem one $H_E$. The result is  
\begin{equation}
B_j(s)=\sum_{k}g_{j,k} \left(\me{-is\nu_{j,k}}d_{j,k}+\me{is\nu_{j,k}}d^\dagger_{j,k}\right).
\end{equation}
The form of the interaction operator also satisfies the condition, that the trace over the i'th environment thermal state with $B\_i(s)$ is zero. This is satisfied because the thermal state is strictly diagonal opposed to $B\_i(s)$, which has no finite diagonal elements in a number state representation.\par With the interaction picture form of $B_i$ the environment correlation functions \eqref{MME_eq:EnvCorrelationFct} can be evaluated
\begin{equation}\label{2site_eq:MarkovEnvCorr}
\begin{aligned}
\Lambda_{j,\alpha,\beta}(s)&=\sum_{k,k'}\Tr_{E_j} \left[B_{j,k}(s)B_{j,k'}(0)\mu_{T_j}\right]=\sum_k \Tr_{E_j}\left[g_{j,k}^2\left(\me{-is\nu_{j,k}}d_{j,k}d^\dagger_{j,k}+\me{is\nu_{j,k}}d^\dagger_{j,k}d_{j,k}\right)\right],\\&=\sum_k g_{j,k}^2\left(\me{-is\nu_{j,k}}(n_{T_j,k}+1)+\me{is\nu_{j,k}}n_{T_j,k}\right)=\int_0^\infty d\nu J_j(\nu)\Big(\me{-is\nu}(n_{T_j}( \nu)+1)+\me{is\nu}n_{T_j}(\nu)\Big),
\end{aligned}
\end{equation}
where it has been used that the modes commute and that the thermal state is diagonal. Here $n_{T_j}(\nu)=(\me{\nu/T_j}-1)^{-1}$ is the Bose occupation and the spectral density of the environments \eqref{KC_eq:selfEnergies}, have been used to change the summation into an integral.\par The next step it to find the energy representation of the interaction operators. The chain Hamiltonian can be diagonalised by hand and one finds 
\begin{equation}\label{2site_eq:DiagonalisingHc}
\begin{gathered}
H_c=\omega_c\begin{pmatrix}
2&0&0&0\\
0&1&\epsilon/\omega_c&0\\
0&\epsilon/\omega_c&1&0\\
0&0&0&1
\end{pmatrix}\to E_T H_c E_T^\dagger=\begin{pmatrix}
2\omega_c&0&0&0\\
0&\omega_c+\epsilon&0&0\\
0&0&\omega_c-\epsilon&0\\
0&0&0&0
\end{pmatrix},\\\;E_T=\begin{pmatrix}
1&0&0&0\\
0&2^{-1/2}&2^{-1/2}&0\\
0&2^{-1/2}&-2^{-1/2}&0\\
0&0&0&1
\end{pmatrix}.
\end{gathered}
\end{equation} 
The rotation $E_T$ contains the eigenstates of $H_c$ in the columns. All the eigenstates have the common feature that they are completely delocalisedin the sense that they consists of linear combinations, with an even occupation in both sites. This originates from the two sites having the same energy splitting. Had this not been the case, then the center part of $E_T$ would explicitly contain the energies of the two sites.\par Transforming  $A_i$ operators into the the $H_c$ basis is done using $E_T$. This leads to the interaction picture form of the system-environment operators
\begin{equation}
A_i(-s)=\me{is(\omega_c+\epsilon)}a_{i1}+\me{is(\omega_c-\epsilon)}a_{i2}+h.c.,
\end{equation}
where the four different matrices are given by
\begin{equation}\label{2site_eq:MarkovHIDecomposition}
a_{11}=\frac{1}{2}\left(\sigma_1^-\mathds{1}_2-\sigma^z_1\sigma_2^-\right),\;a_{12}=\frac{1}{2}\left(\sigma_1^-\mathds{1}_2-\sigma^z_1\sigma_2^-\right) ,\;a_{21}=\frac{1}{2}\left(\mathds{1}_1\sigma_2^- -\sigma_1^-\sigma^z_2\right),\;a_{22}=\frac{1}{2}\left(\mathds{1}_1\sigma_2^- +\sigma_1^-\sigma^z_2\right), 
\end{equation} 
where the Pauli matrices and 2LS annihilation and creation operators have been used.    
As the time-dependence have been isolated from the matrix structure, it is now possible to evaluate the rate integrals \eqref{MME_eq:RateOp} where all terms have the form
\begin{equation}
\Gamma_i(\alpha)=\int_{0}^\infty ds\, d\nu\,\me{\pm is (\nu\pm \alpha)}f(\nu)=\lim_{\eta\to0^+}\int_{0}^\infty ds\, d\nu\,\me{\pm is (\nu\pm \alpha\pm i\eta)}f(\nu)= \lim_{\eta\to0^+}\int_{0}^\infty d\nu\,\frac{\pm i f(\nu)}{\nu\pm\alpha\pm i\eta},
\end{equation}
where an infinitesimal positive contribution have been added to ensure convergence of the integral and the time integral have been performed. This form is equivalent to the integral solved for the retarded self-energies \eqref{KC_eq:Sokhotski} and the rate is therefore given by
\begin{equation}
\Gamma_i(\alpha)=\pm i \mathscr{P}\int_{0}^{\infty}d\nu \frac{f(\nu)}{\nu\pm\alpha}+\int_{0}^{\infty}d\nu\pi\delta(\nu\pm\alpha)f(\nu).
\end{equation}
With the rates and interaction picture operators calculated the Liouvillian can be build \todo{Add all the other rates to Appendix}. A sanity check for the rate operator can be done by inspecting the relationship between rates and their operator \eqref{2site_eq:MarkovHIDecomposition}. This shows that the rates belonging to any of the four operators in \eqref{2site_eq:MarkovHIDecomposition} are related to a factor of $(n_{T_i}+1)$. This means that even at zero temperature there will be a non vanishing rate. This is sensible because all operators in \eqref{2site_eq:MarkovHIDecomposition} describe some sort of emission due to the environments. The opposite process of creating excitations through the environment interaction is described by the transpose of these operators. The rates paired with all these process' is seen to be related to $n_{T_I}$ and thus vanishes for zero temperature as one would expect.\par As the superoperator space has 16 dimensions this is done numerically. Having found the Liouvillian the time evolution is given by \eqref{MME_eq:LiouvilleTimeEvolution}. As the steady-state is independent of the initial state it is more efficient to compute this directly than evolving the system using \eqref{MME_eq:LiouvilleTimeEvolution}. The steady state is defined by not changing over time which means the time derivative of the state must be zero
\begin{equation}
\partial_t \rho(t)=\mathscr{L} \me{\mathscr{L}t}\rho_0=\mathscr{L}\rho(t)=0.
\end{equation}
This is only true if $\rho(t)$ is an eigenstate of the Liouvillian with eigenvalue 0. The steady state of the system can therefore be identified by diagonalising $\mathscr{L}$. 
\subsection{Secular Approixmation Effects}
The simplest Liouvillian is found by enforcing the secular approximation which puts the BM ME dissipator on a Lindblad form \eqref{MME_eq:LindbladDisMESecularised}. Considering the form the dissipator one sees that it has the effect of        
This has a big impact on the behaviour of the secular approximation. To see this, consider the form of the lindblad dissipator in the secularised BM ME \eqref{MME_eq:LindbladDisMESecularised}. The Dissipation terms for a specific   This means that the secular approximation will not going to give rise to any current in the system. The reason this simple observation leads to such a strong statement 
\section{Keldysh Theory}
Now steady-state observables will be derived using the Keldysh framework\todo{Decide what to do about writing the energy dependence explicitly}. It was found in chapter \ref{chp_KC} that the main element needed is the retarded propagator, which is found by inverting \eqref{KC_eq:inverseRetardedProp}.
For the 2-site system the inverse retarded propagator is just $2\times 2$ matrix given by
\begin{equation}
(\mathds{G}^{R})^{-1}=\epsilon\pmqty{R & -1  \\ -1 & R},\text{ with } R=\frac{E-a+ib}{\epsilon} \text{ and } a=\omega_c-\mathscr{P}\int_0^\infty \frac{J(x)}{x-E},\;b=-i\pi J(E).
\end{equation}
where $J$ is the spectral density of the environments \eqref{KC_eq:SpectralFunction}. To avoid an unwieldy notation the energy dependence of $a$ and $b$ will not be written explicitly.\par Inverting this matrix one finds the full retarded propagator for the system
\begin{equation}
\mathds{G}^R=\frac{1}{\epsilon\left(R^2-1\right)}\pmqty{R&1\\1&R}.
\end{equation}
\subsection{Spectral Function}
With this propagator the spectral function for an excitation in the first site is given by  
\begin{equation}
A_{1}(E)=-2\Im G^{R}_{11}(E)=-\frac{-b\Big((E-a)^2+b^2+\epsilon^2\Big)}{\Big((E-a)^2-b^2-\epsilon^2\Big)^2+4(E-a)^2b^2}.
\end{equation}
Using a partial fraction decomposition \cite{Arfken} this can be written as
\begin{equation}\label{2site_eq:SpectralFunc}
A_{1}(E)=\frac{b}{(E-a-\epsilon)^2+b^2}+\frac{b}{(E-a+\epsilon)^2+b^2}.
\end{equation}
This spectral function is showing what happens to a single excitation in the first site. For this system it is seen that it splits up into two peaked contribution centred around $a\pm\epsilon$ respectively. Being centred around these energies is expected considering the eigenstates of the chain Hamiltonian \eqref{2site_eq:DiagonalisingHc}. Here it is seen that the single excitation sector, is spanned by two linear combinations of the two sites with an equal occupation. These eigenstates have the energy $\omega_c\pm\epsilon$. The two terms in the spectral function is seen to correspond these two eigenstates. The spectral function also shows that the principal value part of the self energy gives rise to a renormalisation of the eigenstates energy. This is included in the BM ME through the Lamb shift.\par Considering one term in \eqref{2site_eq:SpectralFunc} it is seen that, for constant spectral densities ($J(E)=c$), it is a Lorentzian with a FWHM of $4c$. Coupling to the environments leads to a broadening of eigenstates. This is understood as the chain eigenstates can mix with the many DOF for the environments. This mixing also means that the eigenstates of the chain no longer is stationary states, but acquire a finite lifetime inversely proportional to the broadening. \par In the case of a constant spectral density the spectral function normalisation \eqref{KC_eq:SpectalFuncNormalisation} can be verified analytically as the Lorentzian distributions in $A_1(E)$ is normalised if divided by $\pi$.\par Computing the spectral function allows for a more quantitative condition for the validity of BM local approximation discussed in chapter \ref{chp_MME}. Here it was found that the environment had to relax much faster than the reduced system. The relaxation time of the system is related to the width of the spectral function peaks whereas the relaxation of the environment is related to the width of spectral density. For BM to be valid the broadest peak in $A_1(\omega)$ must be significantly smaller than the width of the environment spectral density. The stronger the environment coupling the broader the spectral function which mean that for the BM to valid the environment must be broad and weakly coupled. 
\subsection{Occupation and Current}
The next correlation function we consider is the occupation of the sites. This is given by \eqref{KC_eq:Occupation}
\begin{equation}
\left<n_i\right>=\frac{1}{2}\left(\int\frac{d E}{2\pi}i\mathds{G}^K_{ii}(E)-1\right).
\end{equation}
To compute this quantity one must first find the specific element of the Keldysh propagator. This is given by \eqref{KC_eq:GeneralKeldyshChain}. For the first site the Keldysh element is thus given by
\begin{equation}
\mathds{G}^K_{11}=\abs{\mathds{G}^R_{11}}^2\Sigma^K_1+\abs{\mathds{G}^R_{12}}^2\Sigma^K_2=\frac{1}{\epsilon^2 \abs{R^2-1}^2}\left(\abs{R}^2\Sigma^K_1+\Sigma^K_2\right).
\end{equation}
Using the specific form the Keldysh self-energy derived in \eqref{KC_eq:selfEnergies} the propagator element is found to be
\begin{equation}
\mathds{G}^K_{11}=-i2\pi J(E)\frac{\left(b^2+(E-a)^2\right)\coth(E/2T_1)+\epsilon^2 \coth(E/2T_2)}{\Big((E-a)^2-b^2-\epsilon^2\Big)+4b^2(E-a)^2}.
\end{equation}
If the occupation of the second site is considered then the result is identical except for exchanging the the temperatures. Due to the energy dependence of both $a$ and $b$ solving this integral is not tractable by hand and is done numerically.
\\\par To compute the steady-state current it is necessary to find an off-diagonal element in the Keldysh propagator \eqref{KC_eq:Current6}. This off-diagonal element is given by 
\begin{equation}
\mathds{G}^K_{12}=\mathds{G}^R_{11}\Sigma^K_1\bar{\mathds{G}}^R_{21}+\mathds{G}^R_{12}\Sigma^K_2\bar{\mathds{G}}^R_{22}=\frac{-2i\pi J(E)}{\abs{R^2-1}^2\epsilon^2}\left(R\coth(E/2T_1)+\bar{R}\coth(E/2T_2)\right).
\end{equation} 
The real part of this element if easily found to be 
\begin{equation}
\Re\mathds{G}^K_{12}=\frac{2\pi J(E)}{\abs{R^2-1}^2\epsilon^2}\left(R\coth(E/2T_1)+\bar{R}\coth(E/2T_2)\right).
\end{equation}
\subsection{Results} 
\begin{figure} 
	\centering
	\includegraphics[width=1\linewidth]{Keldyshplt1.png}
	\caption{\textit{Values: $\omega_c=1,4$, $T_2=0.001$, $A=0.02$, $\sigma=1.5$}}
	\label{fig:Kplt1}
\end{figure}    