\section{Influence functional derivation}
In this appendix some details of the influence functional is derived. 
Having performed the Gaussian integral and the following matrix product in \eqref{ParIS_eq:InfluenceGaussian} the result is  
\begin{equation}
\begin{aligned}
\mathcal{I}=\exp\Biggl[-\lvert g\rvert^2\int_0^T dt dt'& s^+(t)G^{++}(t,t')s^+(t')+s^-(t)G^{--}s^{-}(t')\\
&-s^+(t)G^{+-}(t,t')s^-(t')-s^-(t)G^{-+}(t,t')s^+(t')\Biggr]
\end{aligned}
\end{equation}
Using the derived form of the free bosonic continuum propagators \eqref{KFT_eq:PMContinuumPropagators} the influence functional takes the form
\begin{equation}
\begin{aligned}
\mathcal{I}&=\exp\Biggl[-\lvert g\rvert^2\int_0^T\int_0^T dt dt' s^+(t)\exp\left[-i\nu(t-t')\right]\biggl(\theta(t-t')(1+n_T)+\theta(t'-t)n_T\biggr)s^+(t')\\&\qquad\qquad+s^-(t)\exp\left[-i\nu(t-t')\right]\biggl(\theta(t'-t)(1+n_T)+\theta(t-t')n_T\biggr)s^{-}(t')\\
&\qquad\qquad-s^+(t)\exp\left[-i\nu(t-t')\right]n_Ts^-(t')-s^-(t)\exp\left[-i\nu(t-t')\right](1+n_T)s^+(t')\Biggr],\\
&=\exp\Biggl[-\lvert g\rvert^2\int_0^T\int_0^t dt dt'\biggl\{ s^+(t)\biggl((1+n_T)e^{-i\nu(t-t')}+e^{i\nu(t-t')}n_T\biggr)s^+(t')\\&\qquad\qquad+s^-(t)\biggl(e^{i\nu(t-t')}(1+n_T)+e^{-i\nu(t-t')}n_T\biggr)s^{-}(t')\biggr\}\\
&\qquad\qquad-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)e^{-i\nu(t-t')}n_Ts^-(t')-s^-(t)e^{-i\nu(t-t')}(1+n_T)s^+(t')\biggr\}\Biggr].\\
\end{aligned}
\end{equation} 
Using the autocorrelation function \eqref{ParIS_eq:alphaSinglemode} and rewriting it slightly 
\begin{equation}
\begin{aligned}
\alpha(t-t')&=\lvert g\rvert^2\left(\coth \frac{\beta \nu}{2}\cos\left(\nu (t-t')\right) -i \sin\left(\nu (t-t')\right)\right)\\&=\lvert g\rvert^2\left(n_B e^{-i\nu(t-t')}+n_B e^{i\nu (t-t')}+e^{-i\nu(t-t')}\right),
\end{aligned}
\end{equation} 
the diagonal terms can be directly written in terms of this function
\begin{equation}
\begin{aligned}\label{eq:I_expression1}
\mathcal{I}&=\exp\Biggl[-\int_0^T\int_0^t dt dt' s^+(t)\alpha(t-t')s^+(t')+s^-(t)\alpha(t-t')s^{-}(t')\\
&\qquad\qquad-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)e^{-i\nu(t-t')}n_Bs^-(t')-s^-(t)e^{-i\nu(t-t')}(1+n_B)s^+(t')\biggr\}\Biggr].
\end{aligned}
\end{equation}
Notice that the off-diagonal terms in \eqref{eq:I_expression1} are integrated over the entire "square" area while the diagonals has a dynamic cut-off.
As the $dt$ and $dt'$ integrals are identical the variables can be renamed without affecting the integrals. 
Writing the off-diagonal elements (collective denoted as $\chi$) in terms of $\alpha$ one finds
\begin{equation}
\begin{aligned}
\chi&=-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)e^{-i\nu(t-t')}n_Ts^-(t')-s^-(t)e^{-i\nu(t-t')}(1+n_T)s^+(t')\biggr\},\\&=-\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)\bar{\alpha}(t-t')s^-(t')-s^-(t)\alpha(t-t')s^+(t')\biggr\}\\
&-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{s^+(t)\left(n_T+1\right) e^{i\nu(t-t')}s^-(t')+s^-(t)e^{i\nu(t-t')}n_Ts^+(t')\biggr\}.
\end{aligned}
\end{equation}  
Exchanging the names of the variables gives
\begin{equation}
\begin{aligned}
\chi&=-\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)\bar{\alpha}(t-t')s^-(t')-s^-(t)\alpha(t-t')s^+(t')\biggr\}\\&\qquad\qquad-\int_0^T\int_0^T dt dt'\biggl\{s^+(t)\bar{\alpha}(t-t')s^-(t')\biggr\},\\
&=-\int_0^T\int_0^T dt dt'-s^-(t)\alpha(t-t')s^+(t').
\end{aligned}
\end{equation}
Next the integration limit can be changed by splitting up the integral in two parts
\begin{equation}
\chi=-\int_0^T\int_0^t dt dt'-s^-(t)\alpha(t-t')s^+(t')-\int_0^T\int_t^T dt dt'-s^-(t)\alpha(t-t')s^+(t').
\end{equation}
The first term has an integration region which corresponds to the blue area in figure \ref{fig:flip}, while the second term has an integration region corresponding to the red one. Because the total region is square, transforming the points from the red region to the blue region is achieved by exchanging $t$ and $t'$. 
\begin{figure} 
	\centering
	\includegraphics[width=0.3\linewidth]{flip.png}
	\caption[Integration domain for the influence functional]{\textit{To transform a point from the red region to the blue region, by flipping it along the black line $a(t)=t$, one simply has to exchange the $t$ and $t'$ values.}}
	\label{fig:flip}
\end{figure}
Doing this transformation and using that $\alpha(t'-t)=\bar{\alpha}(t-t')$, which is seen from the definition \eqref{ParIS_eq:alphaSinglemode}, one arrives at
\begin{equation}
\chi=-\int_0^T\int_0^t dt dt'-s^-(t)\alpha(t-t')s^+(t')-s^+(t)\bar{\alpha}(t-t')s^-(t').
\end{equation}
Inserting this result in \eqref{eq:I_expression1} the influence functional is given by
\begin{equation}\label{eq:InfluenceFunc}
\begin{aligned}
\mathcal{I}=\exp\left[-\int_0^T dt\int_0^t dt'\Bigl(s^+(t)-s^-(t)\Bigr)\Bigl(\alpha(t-t')s^+(t')-\bar{\alpha}(t-t')s^-(t')\Bigr)\right],
\end{aligned}
\end{equation}
\section{Rewriting the overlap}\label{App_Influence2}
In this appendix we will write the density matrix overlap on a more useful form. The algebraic manipulations follow the same principle as the ones presented in \cite{TempoSpecialKursus}.
The starting point is \eqref{ParIS_eq:overlap1}
\begin{equation}
\begin{aligned}
\Lbraket{s_N,I_N}{\rho(\Delta N)}= \sum_{\mathclap{\substack{S_0...S_{N-1}\\ I_0...I_{N-1}}}}\;\;\;\;&\prod_{j=1}^{N}\Big(\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{j-1},I_{j-1}} \mathcal{I}_{1}\{S_{1,j}\}\mathcal{I}_{N_c}\{S_{N_c,j}\}\Big)\Lbraket{S_0,I_0}{\rho_0}.
\end{aligned}
\end{equation}
Using the Influence phases for the two environments, the product can be written as 
\begin{equation}
\begin{aligned}
&\prod_{j=1}^{N}\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{j-1},I_{j-1}}\mathcal{I}_{1}\{S_{1,j}\}\mathcal{I}_{N_c}\{S_{N_c,j}\}\\&=\prod_{j=1}^{N}\prod_{k=1}^{j}\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{j-1},I_{j-1}}\me{-\phi^1_{j,k}}\me{-\phi^{N_c}_{j,k}},\\
&=\prod_{j=2}^{N}\prod_{k=1}^{j}\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{j-1},I_{j-1}}\me{-\phi^1_{j,k}}\me{-\phi^{N_c}_{j,k}}\Lbra{S_1,I_1}\me{\mathcal{L}_0\Delta}\Lket{S_{0},I_{0}}\me{-\phi^1_{1,1}}\me{-\phi^{N_c}_{1,1}},\\
&=\prod_{j=2}^{N}\prod_{k=1}^{j}\bigg(\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{k},I_{k}}\delta_{j-1,k}+(1-\delta_{j-1,k})\bigg)\me{-\phi^1_{j,k}}\me{-\phi^{N_c}_{j,k}}\\&\hspace{1.6cm}\times\me{-\phi^1_{1,1}}\me{-\phi^{N_c}_{1,1}}\Lbra{S_1,I_1}\me{\mathcal{L}_0\Delta}\Lket{S_{0},I_{0}},\\
&=\prod_{j=2}^{N}\prod_{\gamma=1}^{j-1}\bigg(\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{k},I_{k}}\delta_{\gamma,1}+(1-\delta_{\gamma})\bigg)\bigg(\prod_{x=1,N_c}\me{-\phi_{x,\gamma}(S_{j},S_{j-\gamma})}\me{-\phi_{x,0}(S_{j},S_{j})}\bigg)\\&\hspace{1.6cm}\times\bigg(\prod_{x=1,N_c}\me{-\phi_{x,0}(S_{1},S_{1})}\bigg)\Lbra{S_1,I_1}\me{\mathcal{L}_0\Delta}\Lket{S_{0},I_{0}},
\end{aligned}
\end{equation}
where the influence phase, for environment $x$, has been redefined as
\begin{equation}\label{App_Influence1_eq:InfluencePhase}
\phi_{x,\gamma}(S_{i},S_{j})=\Bigl(s^+_{x,i}- s^-_{x,i}\Bigr)\Bigl(\alpha_{x,\gamma}s^+_{x,j}-\bar{\alpha}_{x,\gamma}s^-_{x,j}\Bigr),
\end{equation} 
In the last step the product have been changed to be over the difference $\gamma=j-k$ instead of $k$.
Inserting this into the overlap computation one finds
\begin{equation}
\begin{aligned}
\Lbraket{s_N,I_N}{\rho(\Delta N)}= \sum_{\mathclap{\substack{S_0...S_{N-1}\\ I_0...I_{N-1}}}}\;\;\;\;&\prod_{j=2}^{N}\prod_{\gamma=1}^{j-1}\bigg(\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{k},I_{k}}\delta_{\gamma,1}+(1-\delta_{\gamma})\bigg)\bigg(\prod_{x=1,N_c}\me{-\phi_{x,\gamma}(S_{j},S_{j-\gamma})}\me{-\phi_{x,0}(S_{j},S_{j})}\bigg)\\&\hspace{1.4cm}\times\bigg(\prod_{x=1,N_c}\me{-\phi_{x,0}(S_{1},S_{1})}\bigg)\Lbra{S_1,I_1}\me{\mathcal{L}_0\Delta}\Lket{S_{0},I_{0}}\Lbraket{S_0,I_0}{\rho_0}.
\end{aligned}
\end{equation}
A direct consequence of the environment not sampling the first identity, is that the reduced system can be evolved on step forward and the sum over $(S_0,I_0)$ can be used to generate an identity. Furthermore a new element can be defined 
\begin{equation}
M_\gamma(C_j,C_k)=\begin{cases}
\Lbra{S_j,I_j}\me{\mathcal{L}_0\Delta}\Lket{S_{k},I_{k}}\bigg(\prod_{x=1,N_c}\me{-\phi_{x,1}(S_{j},S_{k})-\phi_{x,0}(S_j,S_j)}\bigg),&\gamma=1\\
{\displaystyle\prod_{x=1,N_c}}\me{-\phi_{x,\gamma}(S_i,S_j)},&\gamma\neq1
\end{cases}.
\end{equation}
Here the large collective index for the entire reduced system $C_i$ is again introduced \eqref{ParIS_eq:reducedIdentity}, now in the superoperator space which means it has $2^{2N_c}$ different inputs.
As before this is nothing but a shorthand notation, such that when computations are actually done it is decomposed into $S_i$ and $I_i$.
The overlap then takes the form
\begin{equation}
\Lbraket{C_N}{\rho(\Delta N)}= \sum_{C_1...C_{N-1}}\prod_{j=2}^{N}\prod_{\gamma=1}^{j-1}M_\gamma(C_j,C_{j-\gamma}) M_0(C_1,C_1)\Lbraket{C_1}{\rho(\Delta)}
\end{equation}
The $j$-product can be thought of as what actually evolves the system forward, while the $\gamma$-product contains all the memory effects of the environment due to previous interactions.