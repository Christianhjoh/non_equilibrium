\chapter{The linear bosonic chain model}\label{chp_KC}
To go beyond the many restrictions found for the MME, the NEQFT will now be used to solve the chain system.
In this chapter different approaches to making the exciton chain model \eqref{Intro_eq:2LS_Hamilton} tractable for a field theory will be discussed.
The main difficulty is that the 2LS operators are not behaving like bosons as the local Hilbert space is only two dimensional.
They do not behave as fermions either as different sites commute opposed to anti-commute.\par
For the field theoretic framework presented in chp. \ref{chp_KFT}, one uses coherent states to turn operators into numbers.
This proves useful because the free theory for both bosons and fermions ends up being Gaussian functional integrals.
These can be solved analytically and one can include interactions either perturbatively or non-perturbativley \cite{Altland}.
For the case of spins one can use spin coherent states \cite{Nagaosa}, however their completeness relation is not covering the entire real axis.
The resulting integrals are therefore not Gaussian.
The consequence is that the integrals can not be solved analytically. The most often used approach is therefore to map the spins to either bosons or fermions. 
\section{Fermion mapping}
Looking at the anti-commutation relation for the spin operators \eqref{KC_eq:2LSAntiComm}, it is very tempting to map each spin site to a fermion.
To make this work it is necessary to correct for the anti-commuting nature between different sites which is a defining feature for fermions.
In one dimensional systems this is accomplished by including a non-local phase factor such that the spin operators transform as 
\begin{equation}\label{KC_eq:JW_transformation}
\sigma_j^-\to c_j \me{-i\pi\sum_{k=1}^{j-1}c^\dagger_k c_k},
\end{equation}  
where $c_j$ is the fermionic annihilation operator for the $j$'th site. This transformation is known as the Jordan-Wigner transformation \cite{Tsvelik}. 
\par This transformation have been used extensively for spin chains as the non-local phase factors cancels out in specific models.
For the pure 2LS part $H_c$ in \eqref{Intro_eq:2LS_Hamilton} this cancellation takes place.
However the phase factors appearing from transforming the environment interaction operators, $(\sigma^+_\lambda+\sigma^-_\lambda)$, does not vanish.
One could imagine that using the gauge invariance of phase operator could alleviate this problem. If the phase string is shifted like 
\begin{equation}
\sigma_i^+\rightarrow\begin{cases}
c_i^\dagger \exp\left[i\pi\left(\sum_{j=1}^{i-1}c_j^\dagger c_j+c_{N_c}^\dagger c_{N_c}\right)\right], & i\neq {N_c} \\
c_{N_c}^\dagger, & i={N_c}
\end{cases},
\end{equation}
then operators belonging to the $N_c'$th site will not have any phase factors.
For the other end of the chain there will be a simple phase factor of the form
\begin{equation}
\me{\pm i\pi c_{N_c}^\dagger c_{N_c}}=(1-2c_{N_c}^\dagger c_{N_c}).
\end{equation}
This phase factor is normal ordered and can therefore be approached with the outlined methods.
The only place were this choice of gauge can give rise to trouble are at the operators coupling to the $N_c$'th site.
Consider therefore the hopping term between the $(N_c-1)$'th site and $N_c$
\begin{equation}\label{eq:phase1}
\sigma^+_{N_c-1}\sigma^-_{N_c}\rightarrow c_{N_c-1}^\dagger \exp\left[i\pi\left(\sum_{j=1}^{N-2}c_j^\dagger c_j+c_{N_c}^\dagger c_{N_c}\right)\right]c_{N_c}=c_{N-1}^\dagger c_{N_c} \exp\left[i\pi\sum_{j=1}^{N_c-2}c_j^\dagger c_j\right].
\end{equation}         
Here the phase is neither cancelled nor normal ordered.
As one can not approximate the phase factor in a controlled manner one has to solve it exactly.
If the total Hamiltonian is number conserving and the chain has periodic boundary conditions, one can solve the system in the two different sectors with either an odd or even number of excitations \cite{XX_circle}.
This is not possible in the transport scenario considered here, because the boundary is not periodic and the environments moves the chain between the odd/even sectors.
In the case of non-periodic boundary condition the end phase factors can still be eliminated, if the thermodynamic limit is taken.
As short chains are considered in this project this limit can not be taken.
These considerations point to the fact that it is not tractable to map the Hamiltonian \eqref{Intro_eq:2LS_Hamilton} to fermions using a Jordan-Wigner transformation.\par
The Jordan-Wigner transformation is by far the most used method for mapping spin/2LS to fermions but there exists a number of other methods.
One is to map each site to two fermions \cite{SemiFermionic}.
This ensures that different sites commute but introduces the difficulty of having a local Hilbert space with twice the dimensionality and more structure.
This means that two-body interactions is turned into four-body interactions, such that the theory is no longer quadratic.
It also necessary to constrain the Hilbert space to the one excitation subspace.
In the case of an equilibrium system this can be done by adding Lagrange multipliers to the chemical potential, but is less clear in the case of non-equilibrium.
Another approach is to map to fermions and Majorana particles \cite{Demler}, which in this case leads to new questions regarding working with Majorana particles in a field theortical formalism.
None of these methods seems directly applicable without first developing a deeper understanding of the methods.
Given the time constraints of the project a simpler approach is therefore chosen.
\section{Bosonic mapping}
The simpler approach is to map each spin to a boson
\begin{equation}
\sigma^-_i\to b_i,
\end{equation}
where $b_i$ is the bosonic annihilation operator for the $i'$th site. These satisfy satisfy the bosonic commutation relations \eqref{Intro_eq:BosonCommutation} which ensures that different sites commute. 
The main difficulty is that the local Hilbert space is infinite instead of two dimensional. 
To get around this one can introduce an energy penalty when more than one excitation is at each site.
In the case where the penalty becomes infinite this mapping becomes valid independently of all the other finite parameters in the model.
Having an infinitely strong non-linear interaction is however highly non-trivial as perturbative approaches are guaranteed to fail and the validity of non-perturabtive methods like the Hubbard-Stratonovich transformation \cite{Kamenev} are not very well understood in this limit.
Due to these complications a less ideal method is proposed; constricting the parameter regime to one with a low occupation number.
The intuitive hypothesis is that in such a regime the penalty is not necessary as it will never be enforced.
The effect is that the project will be constricted to the low temperature regime.\par
The bosonic mapping leads to the Hamiltonian            
\begin{equation}\label{KC_eq:LinearBosonHamiltonian0}
H =H_c + H_E,
\end{equation}
with 
\begin{equation}\label{eq:Schrodinger_Hamiltonian_terms}
\begin{gathered}
H_c= \omega_c\sum_{i=1}^{N_c} b^\dagger_i b_i+\epsilon\sum_{i=1}^{N_c-1}\left(b_i^\dagger b_{i+1}+b_{i+1}^\dagger b_i\right), \\
H_E=\sum_{\lambda=1,N_c}\sum_{k}\left(b_\lambda^\dagger+b_\lambda\right)\left(\bar{g}_{\lambda,k}  d_{\lambda,k}+g_{\lambda,k} d_{\lambda,k}^\dagger \right)+\sum_{\lambda,k}\nu_{\lambda,k} d_{\lambda,k}^\dagger d_{\lambda,k},
\end{gathered}
\end{equation}
where the parameters and the notation is identical to that in \eqref{Intro_eq:2LS_Hamilton}.
Inspecting the interaction between the chain and the environments, it is seen that it contain terms of the form $b d$ and $b^\dagger d^\dagger$.
These terms remove/add an excitations from both the bath and the chain.
A single process of removing two excitations violates energy conservation, but as the inverse process of adding two particles happens equally often, the expectation value of the Hamiltonian will be constant such that energy is conserved.
The non-excitation preserving terms are necessary to correctly describe a dipole interaction.
As these terms do not contain both annihilation and creation operators, it is necessary to move to a higher dimensional representation to get a theory on the correct form \cite{Siebierer}.
This representation is known as the Nambu space and means that the dimensionality is increased from $D$ to $2 D$.
Alternatively in the case where the environment and system is close to resonance, an interaction picture analysis shows that the non-number conserving terms are happening much more frequently than the number conserving terms.
This means that on the time scale of the slow number conserving dynamics, the non-number conserving terms effectively averages to zero.
This approximation is often known as the rotating wave approximation \cite{Agarwal}.
Enforcing this approximation gives rise to the linear bosonic chain Hamiltonian interaction
\begin{equation}\label{KC_eq:LinearBosonHamiltonian}
H_E=\sum_{\lambda=1,N_c}\sum_{k}\left(\bar{g}_{\lambda,k} b_\lambda^\dagger d_{\lambda,k}+g_{\lambda,k} d_{\lambda,k}^\dagger b_\lambda \right)+\sum_{\lambda,k}\nu_{\lambda,k} d_{\lambda,k}^\dagger d_{\lambda,k}.
\end{equation} 
\section{Effective partition function}\label{KC_sec:effPartFunc}
With the Hamiltonian \eqref{KC_eq:LinearBosonHamiltonian} consisting of quadratic terms of bosonic operators, the Keldysh field theory framework derived in chp. \ref{chp_KFT} is directly applicable. First considering the continuum form in the $\phi^\pm$ basis, the partition is given by
	\begin{equation}
	\begin{split}
	\mathcal{Z} = \int& \prod_{i, \lambda,k}\Dm[^\pm_i]{\phi},\Dm[^\pm_{\lambda,k}]{\theta}\exp\Bigg[\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t i G^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'}\\&+\int_{-\infty}^{\infty}\dd{t}\dd{t'}\sum_{\lambda,k}\pmqty{\bar{\theta}_{\lambda,k}^+ & \bar{\theta}_{\lambda,k}^-}_t i D^{-1}_{\lambda,k;t,t'}\pmqty{\theta^+_{\lambda,k} \\ \theta^-_{\lambda,k}}_{t'}\\&+i  \int_{-\infty}^{\infty}\dd{t}\sum_{\lambda,k}\Bigg( g_{\lambda,k}\pmqty{-\bar{\phi}^+_\lambda & \bar{\phi}^-_\lambda}_t\pmqty{\theta_{\lambda,k}^+  \\\theta_{\lambda,k}^-}_t+\bar{g}_{\lambda,k}\pmqty{\bar{\theta}_{\lambda,k}^+ &\bar{\theta}_{\lambda,k}^-}_t\pmqty{-\phi^+_\lambda \\ \phi^-_\lambda}_t\Bigg)\Bigg] \mathcal{N}.
	\end{split}
	\end{equation}
Here $\theta_{\lambda,k}$ are the eigenvalues of the coherent states for the $k$'th mode in the environment, which is coupled to site $\lambda$ in the chain.
$\phi_i$ is the coherent field from the $i'$th site in the chain.
$G\,(D)$ is the pure propagator of the chain (environment). 
The environments consists of non-interacting modes which means the propagator is given by \eqref{KFT_eq:PMContinuumPropagators} for each mode.
$G$ contains the free propagators for each site but also the hopping between different sites.
The interaction between chain and environments are captured by the third term in the action.
Here the signs in the $\phi$-vectors originate from the difference between $U(t,t')$ and $U(t,t')^\dagger$.
The last factor $\mathcal{N}$ is the normalisation which comes from the uncorrelated initial thermal states.\par
As the environment-chain interaction is linear the Gaussian functional integral over the bath fields can be done by finding the inverse of $D^{-1}$ and its determinant.
The determinant directly cancels the normalisation factors from the environments as $H_E$ is unchanged along the entire contour.
Integrating out the environments an effective partition function which only depends on the chain fields is found as
\begin{equation} \mathcal{Z}=\int\prod_{i=1}^{N_c}\Dm[^{\pm}_i]{\phi}\exp\left[i\mathcal{S}\right]/(1-\me{-\omega_c/T_c})^{N_c},
\end{equation} 
with the action given by
\begin{equation}\label{KFT_eq:pmAction}
\begin{split}
\mathcal{S}=&\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t  G^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'} - \\ &\int \dd{t}\dd{t'}\sum_{\lambda,k}\abs{g_{\lambda,k}}^2 \pmqty{-\bar{\phi}^+_\lambda & \bar{\phi}^-_\lambda}_tD_{\lambda,k;t,t'}\pmqty{-\phi^+_\lambda \\\phi^-_\lambda}_{t'}.
\end{split}
\end{equation}  
For all elements in $D$ to be similar to the derived free theory propagators \eqref{KFT_eq:PMContinuumPropagators}, it is necessary that the environments are always in their thermal state.
This is justified as long as the environments are much larger than the system.
This means that the entire state of the environment is not affected noticeably by the chain.
Such an approximation is not the same as having a Markovian environment, as system excitations can still propagate around in the environment as long as the environment state is not noticeably affected.
In practice this effect can be achieved by keeping the temperature of the environments constant.\par
These approximations are obviously not applicable to the chain propagator, as it is not in equilibrium and because there is interaction between the different modes (sites).
To derive the propagators for the free chain, $G$, the pure chain part of the Hamiltonian, $H_c$ \eqref{KC_eq:LinearBosonHamiltonian}, is used.
Because finite and small chains are considered this Hamiltonian can not be diagonalised by a position-momentum Fourier transform.
Using the Keldysh technique the continuum form of the action, for the "free" chain is found to be
\begin{equation}
\begin{aligned}
\mathcal{S}_c =&\sum_{i,j}\int \dd t\,\dd t' \pmqty{\bar{\phi}_i^+&\bar{\phi}_i^-}_t i \mathcal{G}^{-1}_{0;t,t'}\delta_{i,j}\pmqty{\phi^+_j\\\phi^-_j}_{t'}\\
&+i\epsilon \sum_i \int \dd t\left(-\bar{\phi}^+_i\phi^+_{i+1}-\bar{\phi}^+_{i+1}\phi^+_{i}+\bar{\phi}^-_i\phi^-_{i+1}+\bar{\phi}^-_{i+1}\phi^-_{i}\right).
\end{aligned}
\end{equation}  
Now is a convenient time to perform the Keldysh transformation \eqref{KFT_eq:KeldyshTransformGeneral}.
As described the nomenclature in this basis, for bosonic fields, is that the classical field ($\phi^{cl}$) is the sum while the quantum field ($\phi^q$) is the difference of the two branch fields ($\phi^{\pm}$).
To visualise the multimode Keldysh transform consider a chain consisting of two sites.
The transformation then takes the matrix form 
\begin{equation}
\label{eq:Keldysh2site}
U_k = \frac{1}{\sqrt{2}}\begin{pmatrix}
1 & 0 & 1 & 0 \\
0 & 1 & 0 & 1 \\
1 & 0 & -1 & 0 \\
0 & 1 & 0 &-1\\
\end{pmatrix}=U_k^\dagger, \quad U_k U_k^\dagger =\mathds{1}.
\end{equation}
Writing the hopping interaction for the same two site system yields
\begin{equation}
\begin{pmatrix}
\phi^+_1 \\
\phi^+_2 \\
\phi^-_1 \\
\phi^-_2\\
\end{pmatrix}^\dagger
\begin{pmatrix}
0 & -1 &0 & 0\\
-1&0&0&0\\
0&0&0&1\\
0&0&1&0\\
\end{pmatrix}\begin{pmatrix}
\phi^+_1 \\
\phi^+_2 \\
\phi^-_1 \\
\phi^-_2\\
\end{pmatrix}=\Phi^\dagger \mathcal{I} \Phi.
\end{equation}
The Keldysh rotation is applied by inserting the identity relation \eqref{eq:Keldysh2site}
\begin{equation}\label{eq:InteractionKeldyshform}
\Phi^\dagger U_k U_k^\dagger\mathcal{I} U_k U_k^\dagger\Phi
=\begin{pmatrix}
\phi^{cl}_1 \\
\phi^{cl}_2 \\
\phi^q_1 \\
\phi^q_2\\
\end{pmatrix}^\dagger
\begin{pmatrix}
0 & 0 &0 & -1\\
0&0&-1&0\\
0&-1&0&0\\
-1&0&0&0\\
\end{pmatrix}\begin{pmatrix}
\phi^{cl}_1 \\
\phi^{cl}_2 \\
\phi^q_1 \\
\phi^q_2\\
\end{pmatrix}.
\end{equation}
In the Keldysh basis the interaction still gives rise to hopping between the sites but now only between contour fields of opposite types. A classical field excitation on a site can excite either of its neighbouring quantum fields. This generalises to arbitrary long chain lengths, which means that the dimensionality of the anti-diagonal form just grows according to the number of sites present in the chain. The non-interacting multimode chain propagator in the Keldysh basis was previously found \eqref{KFT_eq:FreeBosonAction} to be 
\begin{equation}
G^{-1}_{t,t'}\delta_{i,j} =\begin{pmatrix}
0 & (G^A)^{-1}_{t,t'}\delta_{i,j}\\
(G^R)^{-1}_{t,t'}\delta_{i,j} &  (G^{-1})^K_{t,t'}\delta_{i,j}
\end{pmatrix}.
\end{equation}
Because of the anti-diagonal form of the interaction it will only affect the advanced and retarded elements ($A$ and $R$) and leaves the Keldysh ($K$) and quantum-quantum propagators unperturbed.
This means that the normalisation of the partition is maintained as expected.
Performing the Keldysh rotation on the full action \eqref{KFT_eq:pmAction} it takes the form 
\begin{equation}
S=\sum_{i,j}\int \dd t\;\dd t' \pmqty{\bar{\phi}^{cl}_i & \bar{\phi}^q_i}_t\mathds{G}^{-1}_{i,j;t,t'}\pmqty{\phi^{cl}_j\\\phi^q_j}_{t'},
\end{equation}
with the effective inverse propagator $\mathds{G}$ being given by 
\begin{equation}
\mathds{G}^{-1}_{i,j;t,t'}= \pmqty{0 & (G^A_{i,j;t,t'})^{-1}-\Sigma^A_{i,j;t,t'}\\(G^R_{i,j;t,t'})^{-1}-\Sigma^R_{i,j;t,t'}&(G^{-1}_{t,t'})^{K}\delta_{i,j}-\Sigma^K_{i,j;t,t'}}.
\end{equation}  
Here $\Sigma = \sum_{\lambda, k}\abs{g_{\lambda,k}}^2\sigma^x D_{\lambda, k; t,t'}\sigma_x$ is the self-energy due to the environment interaction.
As all the factors only depend on the time separation $t-t'$ , Fourier transforming removes one of the time integrals.
Using the form of the free propagator, the self energies $\Sigma$, are found in the energy representation to be
\begin{equation}\label{KC_eq:selfEnergies}
\begin{aligned}
\Sigma_{E;i,j}^{K}&=\Sigma_{E,1}^{K}+\Sigma_{E,N_c}^{K},\\&=-i2\pi\sum_{\lambda=1,N_c}\int_{0}^{\infty}\dd \nu J_\lambda(\nu)\delta(E-\nu)\\&\qquad\times\coth(\nu/2T_\lambda)\delta_{i,j}\delta_{i,\lambda},\\
&=-i2\pi\sum_{\lambda}J_\lambda(E)\coth\Big(E/T_\lambda 2\Big)\delta_{i,j}\delta_{i,\lambda}.
\\
\Sigma_{E;i,j}^{R/A}&=\Sigma_{E,1}^{R/A}+\Sigma_{E,N_c}^{R/A},\\&=\sum_{\lambda=1,N_c}\int_{0}^{\infty}\dd \nu \frac{J_\lambda(\nu)}{E-\nu\pm i\eta}\delta_{i,j}\delta_{i,\lambda},
\end{aligned}
\end{equation}
With an analytic spectral density like \eqref{MME_eq:ChosenSpectralDensity} the energy integral in the retarded or advanced self-energy can be solved using the result \eqref{MMe_eq:Sokhotski}.
The retarded propagator is thus fund to be 
\begin{equation}\label{KC_eq:RetardedSelfEnergy}
\Sigma_{E,i}^{R}=-i\pi J_i(E)-\mathscr{P}\int_0^\infty \frac{J_i(\nu)}{\nu-E}. 
\end{equation} \par With the self energies evaluated we turn to the chain elements in $\mathds{G}^{-1}$.
They can be found using the form of the free propagator and the form of the interaction derived in \eqref{eq:InteractionKeldyshform} 
\begin{equation}\label{eq:InverseChainPropagators}
(G_{E;i,j}^{R/A})^{-1}=\left(E-\omega_c \pm i\eta\right)\delta_{i,j}-\epsilon\left(\delta_{i,j+1}+\delta_{i+1,j}\right).
\end{equation}
To find the inverse Keldysh element for the isolated chain one uses relation \eqref{KFT_eq:InverseKeldyshPropagatorRelation} in a similar manner as \eqref{KFT_eq:FreeInverseKeldyshProp}.
Because the hopping interaction did not affect the Keldysh blocks one again finds that $G^{-1}$ is infinitesimal.\par
The full inverse propagator for the chain is given by
\begin{equation}\label{KC_eq:fullInversePropagator}
\mathds{G}^{-1}_{i,j;E}= \pmqty{0 & (G^A_{i,j;E})^{-1}-\Sigma^A_{i,j;E}\\(G^R_{i,j;E})^{-1}-\Sigma^R_{i,j;E}&-\Sigma^K_{i;E}\delta_{i,j}},
\end{equation} 
where $\Sigma^K$ also includes the infinitesimal contribution from the isolated chain propagator.
The full retarded and advanced propagators can be found by inverting the two off-diagonal blocks.
However from the causality structure \eqref{KFT_eq:CausalityStructure} it is known that they are each others Hermitian conjugates, which makes it enough to find the retarded propagator.
The Keldysh propagator is defined by the Keldysh block of the entire propagator $\mathds{G}$.
The same element can be found more efficiently by using \eqref{KFT_eq:InverseKeldyshPropagatorRelation} and rewriting it to find the Keldysh propagator.
This means that the Keldysh propagator is given by
\begin{equation}\label{KC}
\begin{aligned}
\mathds{G}^K_{i,j;E}=\mathds{G}^R\circ \Sigma^K\circ (\mathds{G}^R)^\dagger=\sum_{l}\mathds{G}_{i,l;E}^R \Sigma^K_{l;E}\bar{\mathds{G}}_{j,l;E}^R,
\end{aligned}
\end{equation}    
where $\Sigma^K_l$ is given by \eqref{KC_eq:selfEnergies} for $l=1,N_c$ and \eqref{KFT_eq:FreeInverseKeldyshProp} for all other sites.
Due to $\Sigma^K$ being infinitesimal in the middle of the chain the expression for the full Keldysh propagator is reduced to just two terms
\begin{equation}\label{KC_eq:GeneralKeldyshChain}
\begin{aligned}
\mathds{G}^K_{i,j;E}=\mathds{G}_{i,1;E}^R \Sigma^K_{1;E}\bar{\mathds{G}}_{j,1;E}^R+\mathds{G}_{i,N_c;E}^R \Sigma^K_{N_c;E}\bar{\mathds{G}}_{j,N_c;E}^R,
\end{aligned}
\end{equation}
This last relation shows that the only element that has to be inverted is the retarded block of the full inverse propagator \eqref{KC_eq:fullInversePropagator}.
Specifically, for the Keldysh propagator, the only necessary elements are those in the first and last columns. 
\par The retarded propagator has finite imaginary parts through the environment self-energies.
With these elements there is no reason to keep the infinitesimal parts, which means the limit $\eta\to 0^+$ can be taken.
Define the free diagonal element as $R=(E-\omega_c)/\epsilon$.
The matrix representation of the inverse retarded propagator is then 
\begin{equation}\label{KC_eq:inverseRetardedProp}
(\mathds{G}^R)^{-1} = 
\epsilon\begin{pmatrix}
R-\Sigma^R/\epsilon & -1 &  & &&&\\
-1 & R & -1 && && \\
& -1 & R & -1 &  &\text{\huge{0}}& \\
&&\ddots& \ddots& \ddots &&\\
&\text{\huge{0}}& &-1 & R & -1 & \\
&&& &-1 & R & -1 \\
&& && & -1 & R-\Sigma^R/\epsilon
\end{pmatrix}.
\end{equation}
Here the retarded self-energies are the two identical terms in \eqref{KC_eq:RetardedSelfEnergy}, where the the Kronecker-deltas have been used to identify the position in the matrix.
The form of the inverse retarded propagator is known as a perturbed symmetric tridiagonal toeplitz (PSTT) matrix.
The perturbation comes from the fact that the diagonal elements are not constant.
This makes the inversion more challenging compared to the regular symmetric tridiagonal Toeplitz matrix.
There still exists a closed form expression for the elements of the inverse of a PSTT matrix which was found in \cite{Meek}.
The complication related to this closed form is that the solution depends on the value of $R$.
This is troublesome when the inverse Fourier transform has to be done, as this requires integration over all values of $E$ for which the spectral density is non-vanishing.
A way which could make this closed form useful is to transform to a rotating frame.
This makes it possible to force $R$ into a specific regime such that the closed form solution is unchanged throughout the finite support of the spectral density.\par
Getting the closed form solution to work is non-trivial but a more substantial difficulty is related to finding the inverse Fourier transform of the closed form elements, as they are described by multiple levels of hyperbolic functions.\par
As small chains are considered and the dimensionality of $(\mathds{G}^R)^{-1}$ is equal to the length of the chain a more brute force approach will be used.
If one is interested in longer chains, the propagators elements can instead be found recursively \cite{Meurant}.   
	
\section{Observables}\label{KC_sec:Observables}
As previously stated, knowing the propagators in the Keldysh basis is, due to Wicks theorem, enough to compute any observable.
This statement does not contain any information about the specific form of the observables in terms of the propagators.
This is the purpose of the generalised generating functional \eqref{KFT_eq:GeneralisedGeneratingFunctional}.
Using this generating functional the expectation values of the occupation of the two sites and current will be derived and expressed through propagators.\par
Before considering these quantities it is instructive to consider the element called the spectral function \cite{Bruus}.
This quantity, for the $i$'th site, is given by
\begin{equation}\label{KC_eq:SpectralFunction}
A_{i}(E)=-2\Im \mathds{G}_{ii}^R(E).
\end{equation}
The spectral function is a physical observable, which contains information concerning the energy eigenstructure of the system.
One interpretation is that it contains information about how well the system is described by single-particle-like excitations.
In the case of a free system \eqref{KFT_eq:FreePropagatorsFouirer} the spectral function is equal to $2\pi \delta(E-\omega_0)$.
Peaks in the spectral function, hints at the system  having particle-like excitation at these energies.
Another useful feature of the spectral density is that it satisfies a sum rule \cite{Bruus}
\begin{equation}\label{KC_eq:SpectalFuncNormalisation}
\int \frac{dE}{2\pi} A_i(E)=1.
\end{equation}
This property provides a way to test the physicality of the derived retarded propagator. \par 
As previously noted the retarded propagator has no information about the occupation of the system, so obviously there can be no information about this quantity in the spectral function either.\par
To investigate the occupation of the $i$'th site one can use the generating functional with 
\begin{equation}
\mathcal{O}=b_i^\dagger b_i.
\end{equation}
The mode matrix $\bm{\mathcal{O}}$ needed in \eqref{KFT_eq:CorrelationFunctions} is a matrix with only one non-zero element, which is unity.
This element is placed in the diagonal at the $i$'th position.
The occupation for the $i$'th site is thus given by
\begin{equation}\label{KC_eq:Occupation}
\begin{aligned}
\left<b_i^\dagger b_i\right>(t)&=\frac{i}{2}\frac{\delta}{\delta A^q(t)}\Tr\left[\bm{\mathds{G}}A^q(t)\,\mathds{1}_{q,cl}\otimes\bm{\mathcal{O}}\right]\\&=\frac{i}{2}\frac{\delta}{\delta A^q(t)}\int dt\left[\mathds{G}^K_{ii}(t,t)A^q(t)+2\mathds{G}^q_{ii}(t,t)A^q(t)\right]\\&=\frac{1}{2}\left(i\mathds{G}^K_{ii}(t,t)-1\right)=\frac{1}{2}\left(\int\frac{d E}{2\pi}i\mathds{G}^K_{ii}(E)-1\right).
\end{aligned}
\end{equation}
Here the form of the quantum propagator and $\left<\phi^{cl}\bar{\phi}^{cl}\right>$ from \eqref{KFT_eq:FreePropagatorsTimeDomain} has been used directly.
The occupation is related to the Keldysh propagator as previously noted for the free theory in section \ref{KFT_sec:ContinuumSection}.\par
The occupation is a good observable for understanding the physics of the systems but it is not the obvious observable in an experimental setting.
Here the main observables are correlation functions related to the steady-state current, $I$, through the chain.
Due to the number conserving dynamics of the chain and its one dimensional description, excitations can only be created and annihilated by the environments.
Having environments connected only to the ends of the chain, the steady-state current has to be constant across the entire chain.
The steady-state current can then be found by computing the current in any link between any two sites in the chain. Due to this simplification it is not necessary to worry about the effect of the environments as it is already contained in the reduced chain dynamics.
In the opposite case where one has some complex system and the environments are the simplest part of the system, the current can instead be described by only looking at the flow through one environment \cite{Antti}.\par
Considering the current operator describing the current through the $i$'th site, it is defined as the negative change of occupation for the $i$'th site \cite{AnttiBook}
\begin{equation}
\begin{aligned}
I_i(t) &=-\frac{d}{dt}\left(b_i^\dagger b_i\right)(t)= -i\left[H,b_i^\dagger b_i\right](t) \\&=-i\epsilon\left[(b_i^\dagger b_{i+1}+b_{i+1}^\dagger b_{i}+b_i^\dagger b_{i-1}+b_{i-1}^\dagger b_{i}),b_i^\dagger b_i\right](t).  
\end{aligned} 
\end{equation}
It is the negative change because the current has been defined to equal the number of particles that is removed from the $i$'th site.
There are two terms due to the effective current from the left ($i-1$ terms) and two terms that describes the effective current to the right ($i+1$). Due to their steady-state equivalence the calculations simplify if one defines the current in a symmetrised manner.
The particle current running from the $i$'th site to the $i+1$'th site is then found to be
\begin{equation}\label{KC_eq:Current1}
I_i(t)=\tfrac{1}{2}\left(I_L(t)+I_R(t)\right)=-i\epsilon\left( b_{i+1}^\dagger(t)b_i(t)-b_i^\dagger(t)b_{i+1}(t)\right).
\end{equation}
At this point there is a good opportunity to show the equivalence of using the generalised generating functional and a direct translation to the Keldysh basis propagators. This equivalence will be demonstrated for the expectation value of the steady-state current.\par
For the direct translation approach, half of the current operator is inserted on each leg of the time contour.
Using the coherent states, the particle current is given by
\begin{equation}\label{KC_eq:Current2}
\begin{aligned}
\left<I_i(t)\right>=&-\frac{i\epsilon}{2}\bigg(\left<\phi^+_i(t) \bar{\phi}^+_{i+1}(t)\right>+\left<\phi^-_i(t) \bar{\phi}^-_{i+1}(t)\right>-\left<\phi^+_{i+1}(t) \bar{\phi}^+_i(t)\right>-\left< \phi^-_{i+1}(t)\bar{\phi}^-_i(t)\right>\bigg).
\end{aligned}
\end{equation}      
Using that the Keldysh rotation for bosons is involutory and given by \eqref{KFT_eq:KeldyshTransformGeneral} the $+/-$ fields can be written in the $cl/q$ basis
	\begin{equation}\label{KC_eq:Current3}
	\begin{aligned}
	\left<I_i(t)\right>=&-\frac{i\epsilon}{4}\bigg(\left<\left(\phi^{cl}_i+\phi^{q}_i\right)\left(\bar{\phi}^{cl}_{i+1}+\bar{\phi}^{q}_{i+1}\right)\right>+\left<\left(\phi^{cl}_i-\phi^{q}_i\right)\left(\bar{\phi}^{cl}_{i+1}-\bar{\phi}^{q}_{i+1}\right)\right>\\&-\left<\left(\phi^{cl}_{i+1}+\phi^{q}_{i+1}\right)\left(\bar{\phi}^{cl}_i+\bar{\phi}^{q}_i\right)\right>-\left<\left(\phi^{cl}_{i+1}-\phi^{q}_{i+1}\right)\left(\bar{\phi}^{cl}_i-\bar{\phi}^{q}_i\right)\right>\bigg)(t,t),\\
	=&-\frac{i\epsilon}{2}\bigg(\left<\phi_i^{cl}\bar{\phi}_{i+1}^{cl}\right>+\left<\phi_i^{q}\bar{\phi}_{i+1}^{q}\right>-\left<\phi_{i+1}^{cl}\bar{\phi}_i^{cl}\right>-\left<\phi_{i+1}^{q}\bar{\phi}_i^{q}\right>\bigg)(t,t),
	\end{aligned}
	\end{equation}
Considering equal time correlations means that it is generally necessary to take into account the violation of the redundancy relation \eqref{KFT_eq:CausalityStructureCondition}.
As this originates from the over-counting factors in the coherent state identity relations \eqref{KFT_eq:Coherent_Identity}, this violation is completely independent of the considered Hamiltonian.
It is therefore a constant and only non-vanishing when both time and mode are equal no matter which physical system is described.
For the current, the violation is thus inconsequential as all correlations are between different modes.
This results in the quantum-quantum propagators vanishing and the classical-classical correlators equalling the Keldysh propagator which, from the causality structure \eqref{KFT_eq:CausalityStructure}, is known to be anti-Hermitian, The steady-state current then takes the form
\begin{equation}\label{KC_eq:Current4}
\begin{aligned}
\left<I_i(t)\right>&=
=\frac{\epsilon}{2}\Big(\mathds{G}^K_{i,i+1}(t,t)+ \bar{\mathds{G}}^K_{i,i+1}(t,t)\Big)
=\epsilon\text{Re}\left\{\mathds{G}^K_{i,i+1}(t,t)\right\},\\
&=\epsilon\int_{-\infty}^{\infty}\frac{dE}{2\pi}\text{Re}\left\{\mathds{G}^K_{i,i+1}(E)\right\},\\
\end{aligned}
\end{equation}
The steady-state current is thus given by the real part of a off-diagonal element in the Keldysh propagator.\par
This direct approach makes the derivation transparent but gets very inefficient if one considers higher order correlation functions.
For the two-time current correlations it will mean that \eqref{KC_eq:Current2} will consist of 8 terms, each being a product of four field operators.
Writing a single of these terms in the Keldysh basis means that one term turns into 16.
Evaluating the two time current correlations in this manner,  makes it necessary to evaluate 128 terms and then use the causality structure to identify identical terms.
Simplifying this process is exactly the purpose of the generalised generating functional.
\par With this method the steady-state current is given by      
\begin{equation}\label{KC_eq:Current5}
\begin{aligned}
\left<I_j(\tau)\right>&=\frac{i}{2}\frac{\delta}{\delta A^q(\tau)}\Tr\left[\bm{\mathds{G}}A^q\,\mathds{1}_{q,cl}\otimes\bm{\mathcal{O}}\right],
\end{aligned}
\end{equation}
where $\bm{\mathcal{O}}$ has two non-zero elements as seen in \eqref{KC_eq:Current1}.
They are $-i\epsilon$ for the $(j+1,j)$ element and $i\epsilon$ for $(j,j+1)$.
Due to the trace only the four diagonal terms are of interest for this calculation and one finds
\begin{equation}\label{KC_eq:Current6}
\begin{aligned}
\left<I_j(\tau)\right>&=\frac{\epsilon}{2}\frac{\delta}{\delta A^q(\tau)}\int dt A^q(t)\left(\mathds{G}^K_{j,j+1}(t,t)- \mathds{G}^K_{j+1,j}(t,t)+ \mathds{G}^q_{j,j+1}(t,t)- \mathds{G}^q_{j+1,j}(t,t)\right),\\
&=\epsilon \frac{1}{2}\left(\mathds{G}^K_{j,j+1}(\tau,\tau)- \mathds{G}^K_{j+1,j}(\tau,\tau)\right)=\epsilon\int_{-\infty}^{\infty}\frac{dE}{2\pi}\text{Re}\left\{\mathds{G}^K_{i,i+1}(E)\right\}.
\end{aligned}
\end{equation}  
Here the same arguments are used to discard the redundancy relation violation, as in the direct translational calculation. The two results are seen to be in perfect agreement. 
\section{Chapter summary}
In this chapter different field theoretical approaches to the chain of 2LS was considered. 
It was shown that a Jordan-Wigner transformation is not suitable for the given problem and instead a bosonic mapping was used. 
To simplify the derivations a linear bosonic mapping was chosen.
It was hypothesised that such a map should be valid in the low occupation regime if the environments spectral density was peaked closely to the energies of the system. 
With this map the theory was solved exactly and fully non-Markovian.
It was found that solving a longer chain only lead to inverting a PSTT matrix of the same dimension as the number of sites.
From this matrix inversion all the propagators of the theory could be found using matrix multiplication.
Lastly the steady-state occupation and current was derived in terms of the propagators. 
   