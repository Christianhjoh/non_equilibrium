\chapter{Introduction}
The last decades have lead to an immense advancement of experimental techniques. 
This has opened up for investigating and exploiting complex systems that lie within the quantum regime.
With these capabilities it becomes essential to find theoretical methods that can shed light on the dominating physical processes. 
The kind of complex systems considered in this report is commonly known as open systems.
These systems are characterized by having a small system of interest, the system, which are interacting with large environments. 
Open system frameworks become necessary when the environments also has to be modelled quantum mechanically.
\par This need arises in a large class of physical systems from quantum optics \cite{Carmichael} to semiconductor physics, like solid state quantum dots \cite{QD_phonon}, and even in biological systems, such as energy transfer in molecules \cite{FMOSpec1}. 
Even though these examples a physically very different, it is found that the environment interaction can be described in the language of open systems.
The mentioned realisable systems have a range of interesting applications. A few examples include engineering elements for building quantum computers \cite{LinQuOptComp,SingePhotonQD}, understanding photosynthesis \cite{FMO_ME}, improving solar cells \cite{darkStateProtec} or creating computers based on molecular structures \cite{Molecular}.
Common for all of these examples is, that they can be extended by having several copies interacting with each other.
This allows for many-body phenomena to arise and for more interesting real world applications.\par
One of the simplest approaches, which treats the environment quantum mechanically is the Markovian master equation. 
Its starting point is a microscopic model and it assumes that the environment has no memory of previous interactions between the system and the environments. 
For this to be a valid assumption the environments' and system's dynamics must be on different time scales. 
When first introduced in quantum optics this assumption was well justified, as electromagnetic fields have short relaxation times compared to atoms.
This approximation is less justified in semiconductor and molecular settings. 
In these systems the environment is not an electromagnetic environment but instead collective vibrations of ions.
Such vibrations, known as phonons, are oscillating much slower than electromagnetic fields and thus also have a slower relaxation rate.
It then proves important to take into account that the environment effectively stores information about the system's previous state. 
Effects that arise as a consequence of this are commonly referred to as non-Markovian.\par
Treating non-Markovianity is by itself a big challenge and if one is further interested in larger systems, many of the standard methods are not efficient.
This report therefore investigates alternative methods originating from condensed matter theory, where one mainly deals with large systems, to see if they are applicable to the class of open systems in question.
To understand the constraints imposed on the methods by open systems, it is necessary to start from a microscopic description of the entire system (system and environments), and see how the different concepts originate.
\section{Memory effects and non-equilibrium in open systems}           
Open quantum systems are commonly described in the setting of Hilbert spaces. 
In such a description partitioning the quantum system into subsystems means the complete Hilbert space ($\mathcal{H}$) is a tensor product of all the subsystems' Hilbert spaces
\begin{equation}
\mathcal{H}=\mathcal{H}_\mathcal{S}\otimes\mathcal{H}_\mathcal{E},
\end{equation}
where $\mathcal{H}_{\mathcal{S}(\mathcal{E})}$ is the Hilbert space of the system (environments).
Such a partitioning allows for a transparent description of the open quantum system. 
Here a notation will be used, where the system is denoted ($\mathcal{S}$) and all the environments the system is interacting with, are collectively written as ($\mathcal{E}$).
This means that the effective environment can consist of several different environments. \par 
The complete Hilbert space is huge, due to the many degrees of freedom (DOF) in $\mathcal{E}$.
In many cases this makes direct calculations intractable, as the computations require an impossible amount of data both to be stored and manipulated. 
Making these computations tractable is the main challenge in most many-body quantum formalisms. \par 
The fundamental simplification introduced in open system frameworks is focusing only on the behaviour of $\mathcal{S}$.
This simplification is used to set up an effective description of $\mathcal{S}$, which includes the effects of $\mathcal{E}$, without storing any information about the specific dynamics of $\mathcal{E}$.\par
When only describing the DOF for $\mathcal{S}$, many ambiguities arise when considering concepts that are governing the complete system.
For example even though the system and environment is in an equilibrium state, it is not generally true that the system is itself in equilibrium. 
This makes it necessary to describe the system in a manner that allows for non-equilibrium and therefore has no constraints on the final state of the system.    
Accommodating for non-equilibrium greatly reduces the available condensed matter and quantum field theory methods available. \par 
The usual starting point for an open system approach is the equation of motion (EOM), for the density matrix of the joined state of the system and environment ($\chi$) \cite{Breuer}.
A density matrix is an object which can both describe classical and quantum correlations. 
By integrating out all parts of the EOM that belongs to $\mathcal{E}$, one is left with an effective evolution for a density matrix of the system only. 
This is commonly denoted as the reduced density matrix.
With such a description $\mathcal{E}$ can effectively introduce both classical and quantum correlations in $\mathcal{S}$.\par
A consequence of integrating out $\mathcal{E}$ is that possible memory effects manifest by the effective EOM being non-local in time.
This is a result of the non-Markovian nature of the environment.
Considering a realistic quantum system, it is very often the case that interactions with some kind of environment is present. 
This makes developing new methods for open system a very active field of research. 
Many of the successful non-Markovian methods rely on a specific kind of environments like the hierarchic equations of motion \cite{HEOM} and reaction coordinate \cite{ReactionCoord} methods. 
Both methods works well for environments which mostly interact with the system, around a narrow frequency interval.
More general methods like the Nakajima-Zwanzig equation \cite{Nakajima, zwanzig} establish the non-local master equation but do not give a general way of solving it.
Other methods rely on specific physical insights, to take advantage of transformations, such that one can apply a Markovian master equation in a different basis \cite{PolaronTransform}.\par
As previously mentioned, a common feature for many of these analytical methods is that the difficulty increases significantly, if the system dimension is increased. 
This is because many of the methods use an extended space, which has a dimensionality equal to the square of the system's DOF.
Increasing the system therefore results in an exponential increase of the DOF in the extended space where the system evolution is solved.  
In this project we will investigate a small, but extendible, system where the Markovian master equation description is challenged but tractable. 
The same system will then be approached using a field theoretical method that is applicable for non-equilibrium systems.
This method will scale better for larger system, as the extended space is not necessary. 
As there will be a need for approximations in both the Markovian master equation and the field theoretical method, we are also investigating a numerical method. 
This numerical approach was originally developed for small systems but the structure of the method will be changed.
This change is done to investigate how well the method extends to larger systems.
To construct such a numerical method a mixture of methods from the non-equilibrium quantum field theory, open system and matrix product states, which is a tensor network method commonly used in condensed matter theory, will be used.

\section{Model and realisations}
The focus is on dealing with the environments, so the system considered is a simple one dimensional system consisting of $N_c$ sites.
To make the investigation of all the methods possible, within the time span of the project, we will not include any on-site interaction.
The sites are interacting with each other through a constant hopping. 
At each end of this chain there is attached a large environment. 
The temperature of this environment will give rise to a bias and therefore lead to a non-trivial steady-state.

Even though this model is simple, it is of relevance for the understanding of all the realistic system mentioned previously. 
For quantum optics realisations, one can use a linear array of coupled empty cavities \cite{CavityQED}.
In this case each site is bosonic in nature and so are the environments which are the driving electromagnetic fields at the ends. 
Due to infinite Hilbert space of the bosons, these system can be challenging in a conventional open system approach.
Often it is necessary to truncate the Hilbert space or use a mean field approximation.

The open system methods are simpler when considering sites with a finite Hilbert space.
This is the case for a large class of physical systems. 
As an example the two dimensional Hilbert spaces occurs if low energy excitonic systems are considered.
An exciton is a quasi-particle consisting of an electron and a coulomb correlated hole. 
In such a system, the ground state has the electron in the valence band such that no exciton exists. 
For low energy systems it is only necessary to include the first exciton state. 
This means that each site is effectively described with only two states, empty or occupied. 
The algebra satisfied, for the operators of a 2 level system (2LS), is similar to that of a spin-1/2 particle.
Operators on the same site anti-commute, which guarantees that only one excitation can be present at each site, while operators on different sites commute.      
This model is used for many systems for example semiconductor quantum dots \cite{QDs}. 
Due to the high precision fabrication techniques, it is possible to engineer systems with coupled QDs. 
In this case the environments can be described as the previously mentioned phonons, which couple to the exciton state \cite{QDPhonon}.\par
Here each site will be a QD and thus modelled as a 2LS while the environment consists of the normal modes of the large solid. 
The environments are then modelled as non-interacting thermal bosonic systems.\par
A similar model is applicable for molecular systems \cite{Chin}.
Here the excitaions are also of excitonic nature and hops between the different sites in the molecule. 
Molecules relevant for photosynthesis can be modelled as one dimensional chains of exciton sites where the environments are the vibrational modes of the molecule.\par
Besides investigating photosynthesis and solar cells, the model can also be used to describe a molecule which is coupled to harmonic environments.
It therefore provides a testing ground for integrating molecules with electronics.
As molecules are very stable and small, there is a great interest in using them to build computer components. 
It is hoped that due to the small size of the components, they could allow one to go beyond the semiconductor size constrictions that are emerging \cite{Molecular}. 
\subsection{Microscopic model}  
Considering this large class of systems one of the simplest microscopic Hamiltonian, which captures some of the physics for one dimensional excitonic systems coupled to two non-interacting bosonic environments is
\begin{equation}\label{Intro_eq:2LS_Hamilton}
\begin{gathered}
H=H_c+H_I+H_E,\\
H_c=\omega_c\sum_{i=1}^{N_c}\sigma^+_i \sigma^-_i+\epsilon \sum_{i=1}^{N_c-1}\left(\sigma^+_i \sigma^-_{i+1}+\sigma_{i+1}^+\sigma^-_i\right)\\ H_I=\sum_{\lambda=\{1,N_c\}; k}\left(\sigma^+_\lambda+\sigma^-_\lambda\right) g_{\lambda,k}\left(d_{\lambda,k}+d^\dagger_{\lambda,k}\right),\\
H_E=\sum_{\lambda=\{1,N_c\}; k} \nu_{\lambda,k}d^\dagger_{\lambda,k}d_{\lambda,k}.
\end{gathered}
\end{equation}
In this project we will be working in natural units such that $\hbar=c=k_B=1$. 
The Hamiltonian consists of three parts. $H_c$ describes the small excitonic chain system, which consists of $N_c$ sites that are coupled through a hopping interaction $\epsilon$. Each site is only described by two states; either empty or occupied by the first excitonic state of energy $\omega_c$. 
The level splitting and hopping strength is assumed constant throughout the chain.\par 
No disorder nor interactions beyond nearest neighbour is included in this model, as the main purpose is to investigate how the different methods handle the environment interaction. 
Without such interactions the theory remains quadratic, and can therefore often be solved exactly by an appropriate unitary transformation. 
This approach only works for quadratic theories, whereas all the methods used in this project can handle interactions either exactly or approximately.\par  
The environments interact with the system through $H_I$. 
The chosen form for this interaction is motivated by a dipole interaction. 
This is an often recurring interaction especially in quantum optics \cite{Agarwal}.
It also has the essential feature that the environments can create excitations in the system.
This means the system can be pushed out of equilibrium using different parameters for the two environments.
In the exciton-phonon case, the separation between the electron and hole gives rise to a small dipole which interacts with phonons. 
In the boson-2LS interaction case the dipole coupling is such that the position operators of the environments couple to the end sites (1 and $N_c$) through a $(\sigma^++\sigma^-)$ operator. 
The strength of the interaction is given by $g_{\lambda,k}$. This can be assumed to be real without any consequences, as it is only the absolute value that will enter the final results.
\begin{figure} 
	\centering
	\includegraphics[width=0.5\linewidth]{2site.png}
	\caption[A two-site system]{\textit{A sketch of the two-site system. It consists of two 2 level systems, coupled together by a number conserving hopping interaction. Each site is connected to an environment at a temperature of $T_1$ and $T_2$ respectively.}}
	\label{Intro_fig:2site}
\end{figure}
Finally the environments are described by two infinitely large non-interacting bosonic heat baths, with an excitation energy for each normal mode given by $\nu_{\lambda,k}$. 
The operators for these environments are bosonc and therefore satisfy the commutation relations
\begin{equation}\label{Intro_eq:BosonCommutation}
\left[d_{\alpha,k},d^\dagger_{\beta,q}\right]=\delta_{k,q}\delta_{\alpha,\beta},\;\left[d_{\alpha,k},d_{\beta,q}\right]=\left[d^\dagger_{\alpha,k},d^\dagger_{\beta,q}\right]=0.
\end{equation}
$H_c$ is build by 2LS's which means that the annihilation and creation operators satisfy the on-site anti-commutation relation
\begin{equation}\label{KC_eq:2LSAntiComm}
\left\{\sigma_i^-,\sigma_i^+\right\}=\mathds{1},\;\{\sigma_i^-,\sigma_i^-\}=\{\sigma_i^+,\sigma_i^+\}=0,
\end{equation}
while operators on different sites commute
\begin{equation}\label{KC_eq:2LSComm}
\left[\sigma_i^-,\sigma_j^+\right]=\left[\sigma_i^-,\sigma_j^-\right]=\left[\sigma_i^+,\sigma_j^+\right]=0,\;j\neq i.
\end{equation} 
\par
The main physical system that will considered in this report is  sketched on Fig. \ref{Intro_fig:2site}, and consists of only two sites, $N_c=2$.
This model has the advantage that it is tractable with all the presented methods.
It therefore serves to highlight the differences and similarities between them.
For longer systems the Markovian theory is less transparent as it must be done numerically.
When the field theory is developed, it will be done for the general case of arbitrarily long chains.

\section{Outline}
\begin{itemize}
	\item[] \textbf{Chapter 2}:\\
	In this chapter the Markovian master equation will be described. This will highlight many features and techniques used when the environments are integrated out. This chapter will also be used to show how a Markovian master equation can break down even without non-Markovian effects. 
	\item[] \textbf{Chapter 3}:\\
	Now the main theoretical background for non-equilibrium field theory will be derived in detail. 
	The non-trivial construction of the formalism  will be illustrated for a free bosonic system. 
	\item[] \textbf{Chapter 4}:\\
	Having derived the non-equilibrium field theory formalism it will be used to find a non-Markovian solution for the microscopic Hamiltonian in question. It will be shown that dealing with 2LS is challenging in a field theory language. This makes it necessary to perform an approximative mapping to solve the problem.
	\item[] \textbf{Chapter 5}:\\
	The previous methods were of analytic nature, so in this chapter a numerical tensor network method is derived for comparison.
	This is based on a method constructed for small systems which this chapter derives an extension of which makes a specific class of larger systems tractable.
	\item[] \textbf{Chapter 6}:\\
	In this chapter all three methods are applied to the small two-site system and their results are compared and discussed.      
\end{itemize}

