\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Memory effects and non-equilibrium in open systems}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Model and realisations}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Microscopic model}{4}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Outline}{5}{section.1.3}
\contentsline {chapter}{\numberline {2}The Markovian master equation}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Born-Markov master equation}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Lindblad form}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}A two-site system}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Completely positive master equations}{15}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Chapter summary}{16}{section.2.4}
\contentsline {chapter}{\numberline {3}Non-equilibrium quantum field theory}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Evolving a non-equilibrium system}{17}{section.3.1}
\contentsline {section}{\numberline {3.2}The generating functional}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Time slicing the closed time contour}{19}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Free boson partition function}{21}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}The free boson generating functional}{23}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Propagators for the Free Boson}{24}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}A continuum theory}{27}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}The generalised generating functional}{29}{subsection.3.2.6}
\contentsline {section}{\numberline {3.3}Chapter summary}{32}{section.3.3}
\contentsline {chapter}{\numberline {4}The linear bosonic chain model}{33}{chapter.4}
\contentsline {section}{\numberline {4.1}Fermion mapping}{33}{section.4.1}
\contentsline {section}{\numberline {4.2}Bosonic mapping}{34}{section.4.2}
\contentsline {section}{\numberline {4.3}Effective partition function}{35}{section.4.3}
\contentsline {section}{\numberline {4.4}Observables}{39}{section.4.4}
\contentsline {section}{\numberline {4.5}Chapter summary}{42}{section.4.5}
\contentsline {chapter}{\numberline {5}A numerical tensor network method}{43}{chapter.5}
\contentsline {section}{\numberline {5.1}Reduced system evolution}{43}{section.5.1}
\contentsline {section}{\numberline {5.2}Influence functional}{45}{section.5.2}
\contentsline {section}{\numberline {5.3}Numerical evaluation scheme}{47}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Matrix product states}{47}{subsection.5.3.1}
\contentsline {subsubsection}{Matrix product operators}{49}{section*.11}
\contentsline {subsection}{\numberline {5.3.2}Time evolution using matrix product operators}{49}{subsection.5.3.2}
\contentsline {subsubsection}{Time-local evolution}{52}{section*.12}
\contentsline {subsubsection}{Augmented density tensor}{53}{section*.14}
\contentsline {subsubsection}{Finite memory time approximation and singular value decompositions}{55}{section*.17}
\contentsline {subsubsection}{Numerical parameters and convergence}{55}{section*.19}
\contentsline {section}{\numberline {5.4}Chapter summary}{58}{section.5.4}
\contentsline {chapter}{\numberline {6}Two-site system: A comparison}{59}{chapter.6}
\contentsline {section}{\numberline {6.1}NEQFT observables}{59}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Spectral function}{59}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Occupation and current}{61}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Comparing NEQFT with 2LS models}{64}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}NTNM}{66}{subsection.6.2.1}
\contentsline {section}{\numberline {6.3}Chapter summary}{68}{section.6.3}
\contentsline {chapter}{\numberline {7}Conclusion}{69}{chapter.7}
\contentsline {section}{\numberline {7.1}Outlook}{70}{section.7.1}
\contentsline {chapter}{\numberline {A}Markovian master equation derivations}{71}{appendix.A}
\contentsline {section}{\numberline {A.1}Markovian Rates}{71}{section.A.1}
\contentsline {chapter}{\numberline {B}Tensor network derivations}{72}{appendix.B}
\contentsline {section}{\numberline {B.1}Influence functional derivation}{72}{section.B.1}
\contentsline {section}{\numberline {B.2}Rewriting the overlap}{73}{section.B.2}
\contentsline {chapter}{\numberline {C}Project plan}{76}{appendix.C}
\contentsline {section}{\numberline {C.1}Original project plan}{76}{section.C.1}
\contentsline {section}{\numberline {C.2}Revised project plan}{78}{section.C.2}
\contentsline {chapter}{Bibliography}{79}{section*.32}
