import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('NegEV_A.h5','r')

print(list(data.keys()))
Temp = data.get('Temp').value
A = data.get('A').value
EVneg = data.get('negEV').value
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 16})
plt.ylabel(r'$A\; [\omega_c]$')
plt.xlabel(r'$T_1\; [\omega_c]$')
cnt = plt.contourf(Temp,A,EVneg,100, cmap='Greys')
for c in cnt.collections:
    c.set_edgecolor("face")
plt.colorbar()
plt.savefig('Kossakowski_A.svg')