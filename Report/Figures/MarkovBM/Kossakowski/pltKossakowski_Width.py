import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('NegEV_width.h5','r')

print(list(data.keys()))
width = data.get('width').value
A = data.get('A').value[:20]
EVneg = data.get('negEV').value[:20][:]
print(EVneg.shape)
print(width.shape)
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})
plt.ylabel(r'$g\; [\omega_c]$')
plt.xlabel(r'$\sigma\; [\omega_c]$')
cnt = plt.contourf(width,A,EVneg,100, cmap='Greys_r')
for c in cnt.collections:
    c.set_edgecolor("face")
plt.colorbar()
plt.savefig('Kossakowski_Width.svg')