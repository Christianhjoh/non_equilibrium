import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('NegEV_hop.h5','r')

print(list(data.keys()))
Temp = data.get('Temp').value
Hop = data.get('hop').value
EVneg = data.get('negEV').value
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})
plt.ylabel(r'$\epsilon\; [\omega_c]$')
plt.xlabel(r'$T\; [\omega_c]$')
cnt=plt.contourf(Temp,Hop,EVneg,100, cmap='Greys_r')
for c in cnt.collections:
    c.set_edgecolor("face")
plt.colorbar()
plt.savefig('Kossakowski_hop.svg')
 