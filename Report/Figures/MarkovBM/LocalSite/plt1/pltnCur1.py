import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('pltMECur1.h5','r')

print(list(data.keys()))
Temp = (data.get('Temperature').value)
Cur1 = (data.get('hop=0.05').value)
Cur2 = (data.get('hop=0.5').value)
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 16})
plt.ylabel(r'Particle Current $[\omega_c]$')
plt.xlabel(r'Temp. Env. 1 $[\omega_c]$')
plt.plot(Temp, Cur1, 'C1',label = '$\\epsilon$ = {0:.2f}$\omega_c$'.format(0.05))
plt.plot(Temp, Cur2, 'C2',label = '$\\epsilon$ = {0:.2f}$\omega_c$'.format(0.5))
plt.legend(bbox_to_anchor=(0.3, 1), loc=2, borderaxespad=0.5)
plt.savefig('Cur1.svg')