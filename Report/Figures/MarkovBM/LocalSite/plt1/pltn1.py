import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('pltMEn1.h5','r')

print(list(data.keys()))
Temp = (data.get('Temperature').value)
Occ1 = data.get('hop=0.05').value
Occ2 = data.get('hop=0.5').value

#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 16})
plt.ylabel(r'Occupation Diff.')
plt.xlabel(r'Temp. Env. 1 $[\omega_c]$')
plt.plot(Temp, Occ1, 'C1',label = '$\\epsilon$ = {0:.2f}$\omega_c$'.format(0.05))
plt.plot(Temp, Occ2, 'C2',label = '$\\epsilon$ = {0:.2f}$\omega_c$'.format(0.5))
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=2, loc=2, borderaxespad=0.5)
plt.show()
#plt.savefig('MEn1.svg')
