import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('pltMECur2.h5','r')

print(list(data.keys()))
Temp = 1/1.4*(data.get('Temperature').value)
Cur1 = 1/1.4*(data.get('A=0.02').value)
Cur2 = 1/1.4*(data.get('A=0.1').value)
Cur3 = 1/1.4*(data.get('A=0.2').value)
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 16})
plt.ylabel(r'Particle Current $[\omega_c]$')
plt.xlabel(r'Temp. Env. 1 $[\omega_c]$')
plt.plot(Temp, Cur1, 'C1',label = 'A = {0:.2f}$\omega_c$'.format(0.02/1.4))
plt.plot(Temp, Cur2, 'C2',label = 'A = {0:.2f}$\omega_c$'.format(0.1/1.4))
plt.plot(Temp, Cur3, 'C3',label = 'A = {0:.2f}$\omega_c$'.format(0.2/1.4))
plt.legend(bbox_to_anchor=(0.3, 1), loc=2, borderaxespad=0.5)
plt.savefig('MECur2.svg')