import h5py
from numpy import array
import matplotlib.pyplot as plt

dataK= h5py.File('plt1_data.h5','r')
dataMME = h5py.File('plt1MME.h5','r')
print(list(dataK.keys()))
print(list(dataMME.keys()))
ODl = dataMME.get('OcD005').value
ODh = dataMME.get('OcD05').value
Curh = dataMME.get('Cur05').value
Curl = dataMME.get('Cur005').value
hop = dataK.get('hop').value
Temp = dataK.get('Temp').value
Occh1 = dataK.get('Occh1').value
Occh2 = dataK.get('Occh2').value
Curh1 = dataK.get('Curh1').value
Curh2 = dataK.get('Curh2').value
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})
cutOffOCC = 60
cutOffCur = 100
plt.figure(1)
plt.ylabel(r'$n_2-n_1$')
plt.xlabel(r'$T_1 \;[\omega_c]$')
plt.plot(Temp[:cutOffOCC], Occh1[:cutOffOCC], 'C1',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
plt.plot(Temp[:cutOffOCC], Occh2[:cutOffOCC], 'C0',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[1]))
plt.plot(Temp[:cutOffOCC], ODl[:cutOffOCC], 'C1--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
plt.plot(Temp[:cutOffOCC], ODh[:cutOffOCC], 'C0--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[1]))
lgd=plt.legend(bbox_to_anchor=(-0.4, 0.9),ncol=4, loc=2, borderaxespad=0.5)
plt.savefig('n_dif.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.figure(2)
plt.ylabel(r'$I\; [\omega_c]$')
plt.xlabel(r'$T_1\; [\omega_c]$')
plt.plot(Temp[:cutOffCur
], Curh1[:cutOffCur], 'C1',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
plt.plot(Temp[:cutOffCur
], Curh2[:cutOffCur], 'C0',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[1]))
plt.plot(Temp[:cutOffCur
], Curl[:cutOffCur], 'C1--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
plt.plot(Temp[:cutOffCur
], Curh[:cutOffCur], 'C0--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[1]))

lgd=plt.legend(bbox_to_anchor=(-0.4, 0.9),ncol=4, loc=2, borderaxespad=0.5)
plt.savefig('Cur.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.show()