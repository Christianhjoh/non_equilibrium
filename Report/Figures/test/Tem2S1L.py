import os
from csv import reader
#OMP_NUM_THREADS = os.environ['PBS_NUM_PPN']
from sysclass_TEMPO3 import temposys
import pickle
from lineshapes import eta_all
from numpy import (array, exp, trace, dot, eye, kron, zeros, 
	real, linspace)
from numpy.linalg import eig
from math import pi
from qutip import sigmap, sigmaz, sigmax
from scipy.integrate import quad
from time import time
#Var = int(os.environ['PBS_ARRAYID'])-1

Var1 =1
Var2 = 1
hop=linspace(0.8,1.2,3)
T = linspace(0.01,0.5,50)
start = time()
#name = '2S2E_S'+str(T1[Var]).replace('.','')
name ="test"
wc = 1.4
h=hop[Var1]
T1 = T[Var2]
T2 = 0.01
A = 0.02
width = 1.5

#Lindblad dissipator initialzation 
def Bose(v,T):
	return 1/(exp(v/T)-1)
def J(v,A,width):
	return A*v**2 *exp(-(v/width)**2)
gamma1 = 2*pi *J(wc,A,width)*(Bose(wc,0.01)+1)
gamma2 = 2*pi *J(wc,A,width)*Bose(wc,0.01)
S = quad(J,0,50, args=(A, width),weight = 'cauchy', wvar = wc ,epsabs = 1e-08)[0] 

#Function creating matrix representaion of relevant operators 
iD = eye(2)
sp = sigmap().full()
sp2 = kron(iD,sp)
sz = sigmaz().full()
sx = sigmax().full()
Ex = dot(sp,sp.T)
ET = kron(eig(sx)[1],eig(eye(2))[1])+ 1j*zeros([4,4])
print('----Eigenbasis transformation-----\n')
print(ET)
h0 = kron(wc*Ex,iD)+kron(iD,wc*Ex)+kron(iD,S*Ex)+h*(kron(sp,sp.T)+kron(sp.T,sp))
ET1 = array([[ 0.5 ,  0.5,  0.5,  0.5],
 [ 0.5, -0.5,  0.5, -0.5],
 [ 0.5,  0.5, -0.5, -0.5],
 [ 0.5, -0.5, -0.5,  0.5]])
rhoi = zeros([4,4])
rhoi[0][0] = 1
rhoi = dot(dot(ET1,rhoi),ET1.T)
rhoi = dot(dot(ET,rhoi),ET.T)
print('------- evolution steps-----')	
#Transform to interaction operator basis for connected subspace

H0 = dot(dot(ET,h0),ET.T)

IntOp = real(dot(dot(ET,kron(sx,iD)),ET.T))
print(IntOp)
def eta1(t):
    return eta_all(t,T1,2,width,0,A)

Tem = temposys(4)
#Initial state
Tem.set_hamiltonian(H0)
Tem.add_bath([[IntOp, eta1]])
Tem.add_dissipation([[gamma1,sp2.T],[gamma2,sp2]])
dt = 0.1
dkmax = 20
prec = 60
Tem.set_filename(name)
Tem.set_state(rhoi)
Tem.convergence_params(dt,dkmax,prec)

Tem.prep()
Tem.prop(10)
# for i in range(100):
# 	Tem.prop(20)
# 	Tem.savesys()
print('Total time used: {0:.2f} sec'.format(time()-start))
