from sysclass_ParIS import temposys
import pickle 
from XYChainBuilder import XYCB
from numpy import (trace, dot, array,zeros,
									 real, append, arange, identity, kron)
from numpy.linalg import eig
import matplotlib.pyplot as plt
from qutip import sigmap, qeye, sigmax, sigmaz, sigmay, tensor
from mpmath import *
mp.pretty = True


### ParIS ####

Occ = []

rho = pickle.load(open('2S1L_1_statedat_dkm20prec40.pickle','rb'))


N = 2
Nc = 1
name = str(N)+'S'+str(Nc)+'L'
Chain = XYCB(N,Nc)
Chain.buildOps()
TimeSteps = len(rho[0])
for l in range(1):
	Tem = temposys(Chain.dimC,Chain.dimI,Chain.dl)
	Tem.eig_vec  = rho[2][2]
	for j in range(TimeSteps):
			Iso = []
			for i in range(Chain.Ni):
				Iso.append(real(trace(dot(Tem.HilbertMat(rho[1][j]),kron(identity(Chain.dimC),Chain.EXI[i].full())))))

			Con = []
			for i in range(Chain.Nc):
				Con.append(real(trace(dot(Tem.HilbertMat(rho[1][j]),kron(Chain.EXC[i].full(),identity(Chain.dimI))))))
			Occ.append(Con+Iso)
	Occ = array(Occ,dtype =float)


#### Plot both  ####
plt.style.use('ggplot')
plt.ylabel('occupation of sites')
plt.xlabel('time')
Color = ['C1','C2','C3']
for j in range(1):
	for i in range(Chain.N):
		plt.plot(rho[0][:TimeSteps], Occ[:,i],Color[i], label =" site "+str(i+1))
lgd = plt.legend(bbox_to_anchor=(1.05, 0.95), loc=2, borderaxespad=0.)
plt.title(name)
plt.savefig(name + '.png',dpi=200,bbox_extra_artists=(lgd,), bbox_inches='tight')
