import os
import sys
from sysclass_ParIS import temposys
from XYChainBuilder import XYCB
from math import pi
import pickle
from lineshapes import eta_all
from numpy import array, exp, trace, dot, eye, kron, zeros, linspace
from time import time
from scipy.integrate import quad

start = time()
name = '2S1L_1'
def Bose(v,T):
	return 1/(exp(v/T)-1)
def J(v,A,width):
	return A*v**2 *exp(-(v/width)**2)

N = 2
Nc = 1
Chain = XYCB(N,Nc)

Var1 =0
Var2 = 0
hop=linspace(0.8,1.2,3)
T1 = linspace(0.01,0.5,50)

w = 1.4*array([1,1])
h = hop[Var1]
T= T1[Var2]
A = 0.02
width = 1.5


sx = array([[0,1],[1,0]])
EnvOp = [sx]

#Markovian rates and lamb-shift
gamma1 = 2*pi *J(w[1],A,width)*(Bose(w[1],0.01)+1)
gamma2 = 2*pi *J(w[1],A,width)*Bose(w[1],0.01)
S = quad(J,0,50, args=(A, width),weight = 'cauchy', wvar = w[1] ,epsabs = 1e-08)[0] 
Hls = S*dot(Chain.SPI[Chain.Ni-1],Chain.SMI[Chain.Ni-1])

#initial state
ET = array([[ 0.5 ,  0.5,  0.5,  0.5],
 [ 0.5, -0.5,  0.5, -0.5],
 [ 0.5,  0.5, -0.5, -0.5],
 [ 0.5, -0.5, -0.5,  0.5]])
rhoi = zeros([Chain.dim,Chain.dim])
rhoi[0][0] = 1
rhoi = dot(dot(ET,rhoi),ET.T)


#Hot environment autocorrelation function
def eta(t,T):
    return eta_all(t,T,2,width,0,A)

#print parameters
VarValStr = 'Chain parameters (N, [w], h) = ({}, {}, {}),\nbath coupling Op= {},\nbath parameters ([T], A, wc, s) =  ({}, {}, {}, {})\nDissiaption Strength (emission, pump, lamb-shift) = ({},{},{})'
print(VarValStr.format(N, w, h,'sigx_1 and Lind(sm_3)',T ,A,width,2, gamma1,gamma2,S))

#find chain operators
Chain.set_energy(w)
Chain.set_hop(h)
Chain.buildOps()
Chain.find_Ham()

#for several baths define the different autocorrelation functions
BathOp = []
for i in range(Nc): 
	def ETA(t,j = i):
		return eta(t,T)
	BathOp.append([Chain.SXC[i].full(),ETA])

#Lindblad dissipator
Dissipator = ([[1,[eye(Chain.dimC),Chain.SMI[Chain.Ni-1].full()]]])
#Dissipator = ([[gamma1,[eye(Chain.dimC),Chain.SMI[Chain.Ni-1].full()]],
#	[gamma2,[eye(Chain.dimC),Chain.SPI[Chain.Ni-1].full()]]])

#Set up Paris/Tempo class
Tem = temposys(Chain.dimC,Chain.dimI,Chain.dl)
Tem.set_basis(EnvOp)
Tem.set_hamiltonian(Chain.H0+[[eye(Chain.dimC),S*Chain.EXI[Chain.Ni-1].full()]])
Tem.set_filename(name)
Tem.set_istate(rhoi)
Tem.add_bath(BathOp)
#Tem.add_dissipation(Dissipator)

delta = 0.1
dkmax = 20
prec = 40
Tem.convergence_params(delta,dkmax,prec)
Tem.prep()
Tem.prop(600)
print('time used in total: {0:.2f} sec.'.format(time()-start))
