from sysclass_ParIS import temposys
import pickle 
from XYChainBuilder import XYCB
from numpy import (trace, dot, array,zeros,
									 real, append, arange, identity, kron)
from numpy.linalg import eig
import matplotlib.pyplot as plt
from qutip import sigmap, qeye, sigmax, sigmaz, sigmay, tensor
from mpmath import *
mp.pretty = True

name = 'S001'
### TEMPO ####
rhoT=pickle.load(open('test_state_dkm20prec60.pickle','rb'))
Ex = array([[1,0],[0,0]])
iD = array([[1,0],[0,1]])
sp = array([[0,1],[0,0]])
Ex1 = kron(Ex,iD)
Ex2 = kron(iD,Ex)
sp1 = kron(sp,iD)
sp2 = kron(iD,sp)
ET = array([[ 0.70710678+1j*0, 0.0+1j*0,  0.70710678+1j*0,  0.0+1j*0],
 [ 0.0+1j*0,  0.70710678+1j*0,  0.0+1j*0,  0.70710678+1j*0],
 [ 0.70710678+1j*0,  0.0+1j*0, -0.70710678+1j*0,  0.0+1j*0],
 [ 0.0+1j*0,  0.70710678+1j*0,  0.0+1j*0, -0.70710678+1j*0]])
I = - 1j * (dot(sp1.T,sp2)-dot(sp2.T,sp1))


def Tra(rho,ET):
		r = dot(dot(ET.T,rho.reshape([4,4])),ET)
		flag = abs(r) < 1e-15
		r[flag] = 0 + 1j*0
		return r
OccT=[]
for j in range(len(rhoT[0])):
		OccT.append([real(trace(dot(Tra(rhoT[1][j],ET),Ex1))),real(trace(dot(Tra(rhoT[1][j],ET),Ex2)))])
OccT = array(OccT,dtype=float)

## ParIS ####

Occ = []

rho = pickle.load(open('2S1L_1_statedat_dkm20prec60.pickle','rb'))


N = 2
Nc = 1
name = str(N)+'S'+str(Nc)+'L'
Chain = XYCB(N,Nc)
Chain.buildOps()
TimeSteps = len(rho[0])
for l in range(1):
	Tem = temposys(Chain.dimC,Chain.dimI,Chain.dl)
	Tem.eig_vec  = rho[2][2]
	for j in range(TimeSteps):
			Iso = []
			for i in range(Chain.Ni):
				Iso.append(real(trace(dot(Tem.HilbertMat(rho[1][j]),kron(identity(Chain.dimC),Chain.EXI[i].full())))))

			Con = []
			for i in range(Chain.Nc):
				Con.append(real(trace(dot(Tem.HilbertMat(rho[1][j]),kron(Chain.EXC[i].full(),identity(Chain.dimI))))))
			Occ.append(Con+Iso)
	Occ = array(Occ,dtype =float)

#### Plot  ####
plt.style.use('ggplot')
plt.figure(1)
plt.ylabel('occupation of sites')
plt.xlabel('time [$\\epsilon^{-1}$]')
colTem=['C1','C2']
colPar=['C4','C6']
for i in range(2):
	plt.plot(rhoT[0], OccT[:,i],colTem[i] ,label = 'Tempo S '+str(i+1))

for i in range(Chain.N):
	plt.plot(rho[0], Occ[:,i],colPar[i],linestyle =":", label =" Paris S "+str(i+1))

plt.legend(bbox_to_anchor=(0.10, 0.15), loc=2, borderaxespad=0.)
plt.title(name+ 'occupation')
plt.savefig('Occupation_'+name+'.png')