import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('pltn3.h5','r')

print(list(data.keys()))
Temp = 1/1.4*(data.get('Temperature').value)
Occ1 = data.get('hop=1.0').value
Occ2 = data.get('hop=1.1').value
Occ3 = data.get('hop=1.2').value
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 16})
plt.ylabel(r'Occupation Diff.')
plt.xlabel(r'Temp. Env. 1 $[\omega_c]$')
plt.plot(Temp, Occ1, 'C1',label = '$\epsilon$ = {:.2f}$\omega_c$'.format(1/1.4))
plt.plot(Temp, Occ2, 'C2',label = '$\epsilon$ = {:.2f}$\omega_c$'.format(1.1/1.4))
plt.plot(Temp, Occ3, 'C3',label = '$\epsilon$ = {:.2f}$\omega_c$'.format(1.2/1.4))
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=3, loc=2, borderaxespad=0.5)
plt.savefig('n3.svg')