import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('Spectral_funcs.h5','r')

print(list(data.keys()))
Estruc = data.get('Energy structured [wc]').value
Astruc = data.get('A structured').value
Jstruc = data.get('scaled 10^4 J structured').value
Eflat = data.get('Energy flat [wc]').value
Aflat = data.get('A flat').value
Jflat = data.get('scaled J flat').value
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})
plt.figure(1)
plt.ylabel(r'A(E)')
plt.xlabel(r'E $[\omega_c]$')
plt.plot(Estruc, Astruc, 'C1',label = '$A_1$')
plt.plot(Estruc, Jstruc, 'C0--',label = '$10^{4} J(E)$')
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=3, loc=2, borderaxespad=0.5)
plt.savefig('A_Structured.svg')
plt.figure(2)
plt.ylabel(r'A(E)')
plt.xlabel(r'E $[\omega_c]$')
plt.plot(Eflat, Aflat, 'C1',label = '$A_1$')
plt.plot(Eflat, Jflat, 'C0--',label = '$10^{4} J(E)$')
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=3, loc=2, borderaxespad=0.5)
plt.savefig('A_flat.svg')
plt.show()