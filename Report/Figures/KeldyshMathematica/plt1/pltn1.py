import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('plt1_data.h5','r')

print(list(data.keys()))
hop = data.get('hop').value
Temp = data.get('Temp').value
Occh1 = data.get('Occh1').value
Occh2 = data.get('Occh2').value
Curh1 = data.get('Curh1').value
Curh2 = data.get('Curh2').value
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})
plt.figure(1)
plt.ylabel(r'$n_2-n_1$')
plt.xlabel(r'$T_1 \;[\omega_c]$')
plt.plot(Temp[:40], Occh1[:40], 'C1',label = '$\\epsilon$ = {}$\omega_c$'.format(hop[0]))
plt.plot(Temp[:40], Occh2[:40], 'C0--',label = '$\\epsilon$ = {}$\omega_c$'.format(hop[1]))
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=3, loc=2, borderaxespad=0.5)
plt.savefig('n_dif.svg')
plt.figure(2)
plt.ylabel(r'$I\; [\omega_c]$')
plt.xlabel(r'$T_1\; [\omega_c]$')
plt.plot(Temp, Curh1, 'C1',label = '$\\epsilon$ = {}$\omega_c$'.format(hop[0]))
plt.plot(Temp, Curh2, 'C0--',label = '$\\epsilon$ = {}$\omega_c$'.format(hop[1]))
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=3, loc=2, borderaxespad=0.5)
plt.savefig('Cur.svg')
plt.show()