import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('pltCur1.h5','r')

print(list(data.keys()))
Temp =1/1.4*(data.get('Temperature').value)
Cur1 = 1/1.4*(data.get('hop=0.8').value)
Cur2 = 1/1.4*(data.get('hop=1.0').value)
Cur3 = 1/1.4*(data.get('hop=1.2').value)
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 16})
plt.ylabel(r'Particle Current $[\omega_c]$')
plt.xlabel(r'Temp. Env. 1 $[\omega_c]$')
plt.plot(Temp, Cur1, 'C1',label = '$\\epsilon$ = {0:.2f}$\omega_c$'.format(0.8/1.4))
plt.plot(Temp, Cur2, 'C2',label = '$\\epsilon$ = {0:.2f}$\omega_c$'.format(1/1.4))
plt.plot(Temp, Cur3, 'C3',label = '$\\epsilon$ = {0:.2f}$\omega_c$'.format(1.2/1.4))
plt.legend(bbox_to_anchor=(0.3, 1), loc=2, borderaxespad=0.5)
plt.savefig('Cur1.svg')