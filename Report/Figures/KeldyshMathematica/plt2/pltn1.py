import h5py
from numpy import array
import matplotlib.pyplot as plt

data= h5py.File('plt2_data.h5','r')

print(list(data.keys()))
hop = data.get('hop').value
g = data.get('g').value
Temp = data.get('Temp').value
Oc1_ll = data.get('Oc1_ll').value
Oc2_ll = data.get('Oc2_ll').value
Oc1_lh = data.get('Oc1_lh').value
Oc2_lh = data.get('Oc2_lh').value
Oc1_hl = data.get('Oc1_hl').value
Oc2_hl = data.get('Oc2_hl').value
Oc1_hh = data.get('Oc1_hh').value
Oc2_hh = data.get('Oc2_hh').value
#### Plot  ####
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})
plt.figure(1)
plt.ylabel(r'$\left<n_i\right>$')
plt.xlabel(r'$T_1 \;[\omega_c]$')
plt.plot(Temp, Oc1_ll, 'C1',label = 'site 1: g = {}$\omega_c$'.format(g[0]))
plt.plot(Temp, Oc2_ll, 'C0',label = 'site 2: g= {}$\omega_c$'.format(g[0]))
plt.plot(Temp, Oc1_lh, 'C1--',label = 'site 1: g = {}$\omega_c$'.format(g[1]))
plt.plot(Temp, Oc2_lh, 'C0--',label = 'site 2: g = {}$\omega_c$'.format(g[1]))
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=2, loc=2, borderaxespad=0.5)
plt.savefig('005Hop.svg')
plt.figure(2)
plt.ylabel(r'$\left<n_i\right>$')
plt.xlabel(r'$T_1 \;[\omega_c]$')
plt.plot(Temp, Oc1_hl, 'C1',label = 'site 1: g = {}$\omega_c$'.format(g[0]))
plt.plot(Temp, Oc2_hl, 'C0',label = 'site 2: g= {}$\omega_c$'.format(g[0]))
plt.plot(Temp, Oc1_hh, 'C1--',label = 'site 1: g = {}$\omega_c$'.format(g[1]))
plt.plot(Temp, Oc2_hh, 'C0--',label = 'site 2: g = {}$\omega_c$'.format(g[1]))
plt.legend(bbox_to_anchor=(-0.1, 0.9),ncol=2, loc=2, borderaxespad=0.5)
plt.savefig('05Hop.svg')
plt.show()