(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[         0,          0]
NotebookDataLength[     27453,        696]
NotebookOptionsPosition[     26463,        656]
NotebookOutlinePosition[     26833,        672]
CellTagsIndexPosition[     26790,        669]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Keldysh Functions", "Section",
 CellChangeTimes->{{3.735036171199449*^9, 3.735036176584875*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"J", "[", 
   RowBox[{"\[Epsilon]_", ",", "A_", ",", "\[Sigma]_"}], "]"}], ":=", 
  RowBox[{"A", " ", 
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   RowBox[{"Exp", "[", 
    RowBox[{"-", 
     SuperscriptBox[
      RowBox[{"(", 
       FractionBox["\[Epsilon]", "\[Sigma]"], ")"}], "2"]}], 
    "]"}]}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[CapitalSigma]r", "[", 
   RowBox[{"\[Epsilon]_", ",", "A_", ",", "\[Sigma]_"}], "]"}], ":=", 
  RowBox[{"Re", "[", 
   RowBox[{
    FractionBox["1", "4"], " ", "A", " ", 
    SuperscriptBox["\[ExponentialE]", 
     RowBox[{"-", 
      FractionBox[
       SuperscriptBox["\[Epsilon]", "2"], 
       SuperscriptBox["\[Sigma]", "2"]]}]], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         SuperscriptBox["\[Epsilon]", "2"], 
         SuperscriptBox["\[Sigma]", "2"]]], " ", 
       SqrtBox["\[Pi]"], " ", "\[Epsilon]", " ", "\[Sigma]"}], "+", 
      RowBox[{"2", " ", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         SuperscriptBox["\[Epsilon]", "2"], 
         SuperscriptBox["\[Sigma]", "2"]]], " ", 
       SuperscriptBox["\[Sigma]", "2"]}], "-", 
      RowBox[{"2", " ", "\[Pi]", " ", 
       SuperscriptBox["\[Epsilon]", "2"], " ", 
       RowBox[{"Erfi", "[", 
        FractionBox["\[Epsilon]", "\[Sigma]"], "]"}]}], "-", 
      RowBox[{"2", " ", 
       SuperscriptBox["\[Epsilon]", "2"], " ", 
       RowBox[{"ExpIntegralEi", "[", 
        FractionBox[
         SuperscriptBox["\[Epsilon]", "2"], 
         SuperscriptBox["\[Sigma]", "2"]], "]"}]}], "-", 
      RowBox[{"4", " ", 
       SuperscriptBox["\[Epsilon]", "2"], " ", 
       RowBox[{"Log", "[", 
        RowBox[{"-", "\[Epsilon]"}], "]"}]}], "+", 
      RowBox[{
       SuperscriptBox["\[Epsilon]", "2"], " ", 
       RowBox[{"Log", "[", 
        FractionBox[
         SuperscriptBox["\[Epsilon]", "2"], 
         SuperscriptBox["\[Sigma]", "2"]], "]"}]}], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox["\[Epsilon]", "2"], " ", 
       RowBox[{"Log", "[", "\[Sigma]", "]"}]}], "-", 
      RowBox[{
       SuperscriptBox["\[Epsilon]", "2"], " ", 
       RowBox[{"Log", "[", 
        FractionBox[
         SuperscriptBox["\[Sigma]", "2"], 
         SuperscriptBox["\[Epsilon]", "2"]], "]"}]}]}], ")"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Gk1", "[", 
   RowBox[{
   "\[Epsilon]_", ",", "\[Omega]c_", ",", "h_", ",", "T1_", ",", "T2_", ",", 
    "A_", ",", "\[Sigma]_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"-", "\[ImaginaryI]"}], " ", "2", "\[Pi]", " ", 
   RowBox[{"J", "[", 
    RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{
           SuperscriptBox["\[Pi]", "2"], 
           SuperscriptBox[
            RowBox[{"J", "[", 
             RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], "2"]}],
           "+", 
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
             RowBox[{"\[CapitalSigma]r", "[", 
              RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], 
            ")"}], "2"]}], ")"}], 
        RowBox[{"Coth", "[", 
         FractionBox["\[Epsilon]", 
          RowBox[{"2", " ", "T1"}]], "]"}]}], "+", 
       RowBox[{
        SuperscriptBox["h", "2"], 
        RowBox[{"Coth", "[", 
         FractionBox["\[Epsilon]", 
          RowBox[{"2", " ", "T2"}]], "]"}]}]}], ")"}], "/", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
             RowBox[{"\[CapitalSigma]r", "[", 
              RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], 
            ")"}], "2"], "-", 
          RowBox[{
           SuperscriptBox["\[Pi]", "2"], 
           SuperscriptBox[
            RowBox[{"J", "[", 
             RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], "2"]}],
           "-", 
          SuperscriptBox["h", "2"]}], ")"}], "2"], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox["\[Pi]", "2"], 
        SuperscriptBox[
         RowBox[{"J", "[", 
          RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], "2"], 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
           RowBox[{"\[CapitalSigma]r", "[", 
            RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], ")"}],
          "2"]}]}], ")"}]}], ")"}]}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"ReGk12", "[", 
    RowBox[{
    "\[Epsilon]_", ",", "\[Omega]c_", ",", "h_", ",", "T1_", ",", "T2_", ",", 
     "A_", ",", "\[Sigma]_"}], "]"}], ":=", 
   RowBox[{"2", 
    SuperscriptBox["\[Pi]", "2"], " ", "h", " ", 
    SuperscriptBox[
     RowBox[{"J", "[", 
      RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], "2"], 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Coth", "[", 
         FractionBox["\[Epsilon]", 
          RowBox[{"2", " ", "T1"}]], "]"}], "-", 
        RowBox[{"Coth", "[", 
         FractionBox["\[Epsilon]", 
          RowBox[{"2", " ", "T2"}]], "]"}]}], ")"}], "/", 
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
              RowBox[{"\[CapitalSigma]r", "[", 
               RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], 
             ")"}], "2"], "-", 
           RowBox[{
            SuperscriptBox["\[Pi]", "2"], 
            SuperscriptBox[
             RowBox[{"J", "[", 
              RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], 
             "2"]}], "-", 
           SuperscriptBox["h", "2"]}], ")"}], "2"], "+", 
        RowBox[{"4", " ", 
         SuperscriptBox["\[Pi]", "2"], 
         SuperscriptBox[
          RowBox[{"J", "[", 
           RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], "2"], 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
            RowBox[{"\[CapitalSigma]r", "[", 
             RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], 
           ")"}], "2"]}]}], ")"}]}], ")"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Spec", "[", 
   RowBox[{
   "\[Epsilon]_", ",", "\[Omega]c_", ",", "h_", ",", "A_", ",", "\[Sigma]_"}],
    "]"}], ":=", " ", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"2", "\[Pi]", " ", 
     RowBox[{"J", "[", 
      RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}], 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
          RowBox[{"\[CapitalSigma]r", "[", 
           RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], ")"}], 
        "2"], "+", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"\[Pi]", " ", 
          RowBox[{"J", "[", 
           RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], ")"}], 
        "2"], "+", 
       SuperscriptBox["h", "2"]}], ")"}]}], ")"}], "/", 
   RowBox[{"(", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
           RowBox[{"\[CapitalSigma]r", "[", 
            RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], ")"}],
          "2"], "-", " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"\[Pi]", " ", 
           RowBox[{"J", "[", 
            RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], ")"}],
          "2"], "-", 
        SuperscriptBox["h", "2"]}], ")"}], "2"], "+", 
     RowBox[{"4", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"\[Epsilon]", "-", "\[Omega]c", "-", 
         RowBox[{"\[CapitalSigma]r", "[", 
          RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], ")"}], 
       "2"], 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"\[Pi]", " ", 
         RowBox[{"J", "[", 
          RowBox[{"\[Epsilon]", ",", "A", ",", "\[Sigma]"}], "]"}]}], ")"}], 
       "2"]}]}], ")"}]}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.732337401076427*^9, 3.732337430157209*^9}, {
   3.732337613324576*^9, 3.732337679343315*^9}, {3.7323378146109743`*^9, 
   3.732337920884007*^9}, {3.732337960474523*^9, 3.732338204048753*^9}, {
   3.732339072866*^9, 3.732339073735445*^9}, {3.7323416247904663`*^9, 
   3.732341731449234*^9}, {3.732341771932921*^9, 3.732341774996354*^9}, {
   3.7323418863114357`*^9, 3.73234190203071*^9}, {3.732342012393815*^9, 
   3.732342037206809*^9}, {3.732342152153812*^9, 3.732342165871603*^9}, {
   3.732342341978899*^9, 3.732342348834197*^9}, {3.732342448876116*^9, 
   3.732342467513672*^9}, {3.732342507731937*^9, 3.7323425127684193`*^9}, {
   3.7323450883790817`*^9, 3.7323451312056637`*^9}, {3.732345253324106*^9, 
   3.732345262947921*^9}, {3.732348711188696*^9, 3.73234872115551*^9}, {
   3.7323492576323633`*^9, 3.732349272805917*^9}, {3.732349509610379*^9, 
   3.732349517063483*^9}, {3.73234959606387*^9, 3.732349643719878*^9}, {
   3.732350024131571*^9, 3.732350039594955*^9}, {3.732351097147276*^9, 
   3.732351134429125*^9}, {3.732351182855928*^9, 3.732351211295661*^9}, {
   3.732351323767579*^9, 3.7323513629504833`*^9}, {3.73235150697134*^9, 
   3.732351507243414*^9}, {3.732352308760911*^9, 3.732352341372141*^9}, {
   3.7323560039935913`*^9, 3.7323560435133343`*^9}, {3.7323572129322577`*^9, 
   3.7323574142034893`*^9}, {3.732357451947927*^9, 3.732357508419496*^9}, {
   3.732357558359641*^9, 3.732357566724737*^9}, {3.7323576433776693`*^9, 
   3.732357729180172*^9}, {3.732358317985224*^9, 3.73235832147674*^9}, {
   3.7323616928697557`*^9, 3.732361698544908*^9}, 3.7323617394388742`*^9, {
   3.732361781141039*^9, 3.732361814014285*^9}, {3.732361866364159*^9, 
   3.7323619584215527`*^9}, {3.7323620051881313`*^9, 
   3.7323620151084642`*^9}, {3.732362068233089*^9, 3.732362069228312*^9}, {
   3.732362278819213*^9, 3.7323623135609426`*^9}, 3.7324259638224792`*^9, {
   3.73242600410389*^9, 3.732426006478608*^9}, {3.732429743280292*^9, 
   3.732429743608285*^9}, {3.73244506862609*^9, 3.7324450741374817`*^9}, {
   3.732445546083568*^9, 3.7324455970687323`*^9}, {3.732445643213682*^9, 
   3.732445648533306*^9}, {3.7326016262587852`*^9, 3.732601627131521*^9}, {
   3.732691153648416*^9, 3.732691199827559*^9}, 3.73288337260863*^9, {
   3.7328915537882853`*^9, 3.732891554636373*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Occupation - T1", "Section",
 CellChangeTimes->{{3.7350163021170607`*^9, 3.735016317724642*^9}}],

Cell[CellGroupData[{

Cell["A=0.02", "Subsubsection",
 CellChangeTimes->{{3.735018873676077*^9, 3.7350188762521763`*^9}, {
  3.735054959471593*^9, 3.7350549624447317`*^9}}],

Cell[BoxData[{
 RowBox[{"Module", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "magnitude", ",", "width", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T1",
      ",", "T2", ",", "ZeroTcorrection"}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{
    RowBox[{"magnitude", " ", "=", "0.02"}], ";", "\[IndentingNewLine]", 
    RowBox[{"width", "=", "1.5"}], ";", "\[IndentingNewLine]", 
    RowBox[{"\[Omega]c", "=", "1.4"}], ";", "\[IndentingNewLine]", 
    RowBox[{"\[Epsilon]", "=", "1"}], ";", "\[IndentingNewLine]", 
    RowBox[{"T2", "=", "0.01"}], ";", "\[IndentingNewLine]", 
    RowBox[{"ZeroTcorrection", "=", 
     RowBox[{"NIntegrate", "[", 
      RowBox[{
       RowBox[{"\[ImaginaryI]", " ", 
        RowBox[{
         FractionBox["1", 
          RowBox[{"4", "\[Pi]"}]], 
         RowBox[{"Gk1", "[", 
          RowBox[{
          "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "0.0000001", ",", 
           "0.0000001", ",", "magnitude", ",", "width"}], "]"}]}]}], ",", 
       RowBox[{"{", 
        RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"SpecOcc1", "=", 
     RowBox[{"ParallelTable", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"T1", ",", 
         RowBox[{"NIntegrate", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"Spec", "[", 
             RowBox[{
             "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "magnitude", ",",
               "width"}], "]"}], "/", 
            RowBox[{"(", 
             RowBox[{"2", "\[Pi]"}], ")"}]}], ",", 
           RowBox[{"{", 
            RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}], ",", 
           RowBox[{"WorkingPrecision", "\[Rule]", "10"}], ",", 
           RowBox[{"PrecisionGoal", "\[Rule]", "10"}], ",", 
           RowBox[{"MinRecursion", " ", "\[Rule]", " ", "42"}], ",", " ", 
           RowBox[{"MaxRecursion", "\[Rule]", "50"}]}], "]"}], ",", 
         RowBox[{
          RowBox[{"NIntegrate", "[", 
           RowBox[{
            RowBox[{"\[ImaginaryI]", " ", 
             FractionBox[
              RowBox[{"Gk1", "[", 
               RowBox[{
               "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T1", ",", 
                "T2", ",", "magnitude", ",", "width"}], "]"}], 
              RowBox[{"4", "\[Pi]"}]]}], ",", 
            RowBox[{"{", 
             RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], 
          "-", "ZeroTcorrection"}], ",", 
         RowBox[{
          RowBox[{"NIntegrate", "[", 
           RowBox[{
            RowBox[{"\[ImaginaryI]", " ", 
             FractionBox[
              RowBox[{"Gk1", "[", 
               RowBox[{
               "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T2", ",", 
                "T1", ",", "magnitude", ",", "width"}], "]"}], 
              RowBox[{"4", "\[Pi]"}]]}], ",", 
            RowBox[{"{", 
             RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], 
          "-", "ZeroTcorrection"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"T1", ",", 
         RowBox[{"Range", "[", 
          RowBox[{"0.01", ",", "0.5", ",", "0.01"}], "]"}]}], "}"}]}], 
      "]"}]}], ";"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"OcDiff1", "=", 
   RowBox[{"Chop", "[", 
    RowBox[{"ParallelTable", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"SpecOcc1", "\[LeftDoubleBracket]", 
        RowBox[{"i", ",", "4"}], "\[RightDoubleBracket]"}], "-", 
       RowBox[{"SpecOcc1", "\[LeftDoubleBracket]", 
        RowBox[{"i", ",", "3"}], "\[RightDoubleBracket]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", 
         RowBox[{"SpecOcc1", "\[LeftDoubleBracket]", 
          RowBox[{"All", ",", "1"}], "\[RightDoubleBracket]"}], "]"}]}], 
       "}"}]}], "]"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.735016828422386*^9, 3.735016847175976*^9}, {
   3.735017033947565*^9, 3.735017078526725*^9}, {3.735017131942608*^9, 
   3.735017212485376*^9}, {3.735017272897443*^9, 3.7350172743070507`*^9}, {
   3.735017342468535*^9, 3.735017350567811*^9}, {3.735017973518669*^9, 
   3.7350180183784733`*^9}, {3.735018523062014*^9, 3.735018531749135*^9}, 
   3.7350188861260157`*^9, {3.735019037697357*^9, 3.735019038087582*^9}, {
   3.735019253957533*^9, 3.735019291597879*^9}, {3.7350193509112263`*^9, 
   3.735019383833212*^9}, {3.73501973409549*^9, 3.7350197343543177`*^9}, {
   3.735043386748475*^9, 3.735043388084756*^9}, {3.7350463844114647`*^9, 
   3.7350463848608027`*^9}, {3.735047238969285*^9, 3.735047363036962*^9}, 
   3.735047395716893*^9, {3.735047687050262*^9, 3.7350476914279013`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["A=0.1", "Subsubsection",
 CellChangeTimes->{{3.735019054381297*^9, 3.735019058581575*^9}, {
  3.735055147812196*^9, 3.73505515419598*^9}}],

Cell[BoxData[{
 RowBox[{"Module", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "magnitude", ",", "width", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T1",
      ",", "T2", ",", "ZeroTcorrection"}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{
    RowBox[{"magnitude", " ", "=", "0.1"}], ";", "\[IndentingNewLine]", 
    RowBox[{"width", "=", "1.5"}], ";", "\[IndentingNewLine]", 
    RowBox[{"\[Omega]c", "=", "1.4"}], ";", "\[IndentingNewLine]", 
    RowBox[{"\[Epsilon]", "=", "1"}], ";", "\[IndentingNewLine]", 
    RowBox[{"T2", "=", "0.01"}], ";", "\[IndentingNewLine]", 
    RowBox[{"ZeroTcorrection", "=", 
     RowBox[{"NIntegrate", "[", 
      RowBox[{
       RowBox[{"\[ImaginaryI]", " ", 
        RowBox[{
         FractionBox["1", 
          RowBox[{"4", "\[Pi]"}]], 
         RowBox[{"Gk1", "[", 
          RowBox[{
          "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "0.0000001", ",", 
           "0.0000001", ",", "magnitude", ",", "width"}], "]"}]}]}], ",", 
       RowBox[{"{", 
        RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"SpecOcc2", "=", 
     RowBox[{"ParallelTable", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"T1", ",", 
         RowBox[{"NIntegrate", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"Spec", "[", 
             RowBox[{
             "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "magnitude", ",",
               "width"}], "]"}], "/", 
            RowBox[{"(", 
             RowBox[{"2", "\[Pi]"}], ")"}]}], ",", 
           RowBox[{"{", 
            RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}], ",", 
           RowBox[{"WorkingPrecision", "\[Rule]", "10"}], ",", 
           RowBox[{"PrecisionGoal", "\[Rule]", "10"}], ",", 
           RowBox[{"MinRecursion", " ", "\[Rule]", " ", "42"}], ",", " ", 
           RowBox[{"MaxRecursion", "\[Rule]", "50"}]}], "]"}], ",", 
         RowBox[{
          RowBox[{"NIntegrate", "[", 
           RowBox[{
            RowBox[{"\[ImaginaryI]", " ", 
             FractionBox[
              RowBox[{"Gk1", "[", 
               RowBox[{
               "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T1", ",", 
                "T2", ",", "magnitude", ",", "width"}], "]"}], 
              RowBox[{"4", "\[Pi]"}]]}], ",", 
            RowBox[{"{", 
             RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], 
          "-", "ZeroTcorrection"}], ",", 
         RowBox[{
          RowBox[{"NIntegrate", "[", 
           RowBox[{
            RowBox[{"\[ImaginaryI]", " ", 
             FractionBox[
              RowBox[{"Gk1", "[", 
               RowBox[{
               "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T2", ",", 
                "T1", ",", "magnitude", ",", "width"}], "]"}], 
              RowBox[{"4", "\[Pi]"}]]}], ",", 
            RowBox[{"{", 
             RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], 
          "-", "ZeroTcorrection"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"T1", ",", 
         RowBox[{"Range", "[", 
          RowBox[{"0.01", ",", "0.5", ",", "0.01"}], "]"}]}], "}"}]}], 
      "]"}]}], ";"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"OcDiff2", "=", 
   RowBox[{"Chop", "[", 
    RowBox[{"ParallelTable", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"SpecOcc2", "\[LeftDoubleBracket]", 
        RowBox[{"i", ",", "4"}], "\[RightDoubleBracket]"}], "-", 
       RowBox[{"SpecOcc2", "\[LeftDoubleBracket]", 
        RowBox[{"i", ",", "3"}], "\[RightDoubleBracket]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", 
         RowBox[{"SpecOcc2", "\[LeftDoubleBracket]", 
          RowBox[{"All", ",", "1"}], "\[RightDoubleBracket]"}], "]"}]}], 
       "}"}]}], "]"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7350186307416277`*^9, 3.735018642797967*^9}, {
  3.7350190875936947`*^9, 3.735019133597011*^9}, {3.7350193903126993`*^9, 
  3.7350193996001062`*^9}, {3.7350197182821217`*^9, 3.7350197303349657`*^9}, {
  3.735043382331859*^9, 3.735043383122944*^9}, {3.7350463782374563`*^9, 
  3.735046379413225*^9}, {3.7350473820135183`*^9, 3.735047398901497*^9}, {
  3.735047677971531*^9, 3.7350476818269653`*^9}, {3.735055159334422*^9, 
  3.735055161781391*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["A=0.2", "Subsubsection",
 CellChangeTimes->{{3.735019149486361*^9, 3.735019152431121*^9}, {
  3.735055179668108*^9, 3.735055181748394*^9}}],

Cell[BoxData[{
 RowBox[{"Module", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "magnitude", ",", "width", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T1",
      ",", "T2", ",", "ZeroTcorrection"}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{
    RowBox[{"magnitude", " ", "=", "0.2"}], ";", "\[IndentingNewLine]", 
    RowBox[{"width", "=", "1.5"}], ";", "\[IndentingNewLine]", 
    RowBox[{"\[Omega]c", "=", "1.4"}], ";", "\[IndentingNewLine]", 
    RowBox[{"\[Epsilon]", "=", "1"}], ";", "\[IndentingNewLine]", 
    RowBox[{"T2", "=", "0.01"}], ";", "\[IndentingNewLine]", 
    RowBox[{"ZeroTcorrection", "=", 
     RowBox[{"NIntegrate", "[", 
      RowBox[{
       RowBox[{"\[ImaginaryI]", " ", 
        RowBox[{
         FractionBox["1", 
          RowBox[{"4", "\[Pi]"}]], 
         RowBox[{"Gk1", "[", 
          RowBox[{
          "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "0.0000001", ",", 
           "0.0000001", ",", "magnitude", ",", "width"}], "]"}]}]}], ",", 
       RowBox[{"{", 
        RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"SpecOcc3", "=", 
     RowBox[{"ParallelTable", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"T1", ",", 
         RowBox[{"NIntegrate", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"Spec", "[", 
             RowBox[{
             "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "magnitude", ",",
               "width"}], "]"}], "/", 
            RowBox[{"(", 
             RowBox[{"2", "\[Pi]"}], ")"}]}], ",", 
           RowBox[{"{", 
            RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}], ",", 
           RowBox[{"WorkingPrecision", "\[Rule]", "10"}], ",", 
           RowBox[{"PrecisionGoal", "\[Rule]", "10"}], ",", 
           RowBox[{"MinRecursion", " ", "\[Rule]", " ", "42"}], ",", " ", 
           RowBox[{"MaxRecursion", "\[Rule]", "50"}]}], "]"}], ",", 
         RowBox[{
          RowBox[{"NIntegrate", "[", 
           RowBox[{
            RowBox[{"\[ImaginaryI]", " ", 
             FractionBox[
              RowBox[{"Gk1", "[", 
               RowBox[{
               "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T1", ",", 
                "T2", ",", "magnitude", ",", "width"}], "]"}], 
              RowBox[{"4", "\[Pi]"}]]}], ",", 
            RowBox[{"{", 
             RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], 
          "-", "ZeroTcorrection"}], ",", 
         RowBox[{
          RowBox[{"NIntegrate", "[", 
           RowBox[{
            RowBox[{"\[ImaginaryI]", " ", 
             FractionBox[
              RowBox[{"Gk1", "[", 
               RowBox[{
               "En", ",", "\[Omega]c", ",", "\[Epsilon]", ",", "T2", ",", 
                "T1", ",", "magnitude", ",", "width"}], "]"}], 
              RowBox[{"4", "\[Pi]"}]]}], ",", 
            RowBox[{"{", 
             RowBox[{"En", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}], 
          "-", "ZeroTcorrection"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"T1", ",", 
         RowBox[{"Range", "[", 
          RowBox[{"0.01", ",", "0.5", ",", "0.01"}], "]"}]}], "}"}]}], 
      "]"}]}], ";"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"OcDiff3", "=", 
   RowBox[{"Chop", "[", 
    RowBox[{"ParallelTable", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"SpecOcc3", "\[LeftDoubleBracket]", 
        RowBox[{"i", ",", "4"}], "\[RightDoubleBracket]"}], "-", 
       RowBox[{"SpecOcc3", "\[LeftDoubleBracket]", 
        RowBox[{"i", ",", "3"}], "\[RightDoubleBracket]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", 
         RowBox[{"SpecOcc3", "\[LeftDoubleBracket]", 
          RowBox[{"All", ",", "1"}], "\[RightDoubleBracket]"}], "]"}]}], 
       "}"}]}], "]"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7350186307416277`*^9, 3.735018642797967*^9}, {
   3.7350187546795177`*^9, 3.735018763816012*^9}, {3.735019176331358*^9, 
   3.735019216298707*^9}, {3.735019400959073*^9, 3.7350194108256083`*^9}, {
   3.735019749481572*^9, 3.735019749866126*^9}, 3.7350433774389687`*^9, {
   3.735046963738089*^9, 3.735047214567726*^9}, {3.735047411487616*^9, 
   3.7350474812002783`*^9}, {3.73504766560526*^9, 3.735047671057755*^9}, {
   3.735055184261628*^9, 3.735055188213992*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Export", "[", 
   RowBox[{
   "\"\</home/christian/repos/non_equilibrium/Report/Figures/\
KeldyshMathematica/plt2/pltn2.h5\>\"", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"Chop", "[", "SpecOcc3", "]"}], "\[LeftDoubleBracket]", 
       RowBox[{"All", ",", "1"}], "\[RightDoubleBracket]"}], ",", "OcDiff3", 
      ",", "OcDiff1", ",", "OcDiff2"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\"\<Datasets\>\"", ",", 
      RowBox[{"{", 
       RowBox[{
       "\"\<Temperature\>\"", ",", "\"\<A=0.02\>\"", ",", "\"\<A=0.1\>\"", 
        ",", "\"\<A=0.2\>\""}], "}"}]}], "}"}]}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.7350477467675743`*^9, 3.735047799043202*^9}, {
   3.735049778193306*^9, 3.73504978496074*^9}, {3.735050379989831*^9, 
   3.735050380124467*^9}, {3.735050754045579*^9, 3.7350507612960463`*^9}, {
   3.735050970722496*^9, 3.735051054194828*^9}, {3.735055217189096*^9, 
   3.735055249736154*^9}, {3.7350554876743393`*^9, 3.735055491162747*^9}, 
   3.735202021742463*^9}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1596, 1615},
WindowMargins->{{Automatic, 2}, {-52, Automatic}},
Magnification:>2. Inherited,
FrontEndVersion->"11.0 for Linux x86 (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[422, 15, 102, 1, 132, "Section"],
Cell[527, 18, 10828, 270, 1303, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11392, 293, 102, 1, 132, "Section"],
Cell[CellGroupData[{
Cell[11519, 298, 150, 2, 72, "Subsubsection"],
Cell[11672, 302, 4667, 105, 1147, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16376, 412, 144, 2, 72, "Subsubsection"],
Cell[16523, 416, 4327, 101, 1147, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20887, 522, 145, 2, 72, "Subsubsection"],
Cell[21035, 526, 4346, 101, 1147, "Input"],
Cell[25384, 629, 1051, 23, 202, "Input"]
}, Open  ]]
}, Open  ]]
}
]
*)

