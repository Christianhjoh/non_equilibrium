import os
import pickle
from numpy import (array, exp, trace, dot, eye, kron, zeros, 
	real, linspace,std, append, all, shape, mean, std)
from numpy.linalg import eig
from qutip import sigmap, sigmaz, sigmax
from time import time
import matplotlib.pyplot as plt
import mpmath as mp
mp.pretty = True
Temp = [0.4,0.3,0.2,0.1]
N = len(Temp)
data = []
val = []
for ii in range(N):
	NAME = '2S2E_weak_R2_T'+str(Temp[ii]).replace('.','')
	data.append(pickle.load(open('Tempo_'+NAME+'_statedat_dkm32prec55.pickle','rb')))
wc = 1
h= 0.5
#Function creating matrix representaion of relevant operators 
iD = eye(2)
sp = sigmap().full()
sp2 = kron(iD,sp)
sp1 = kron(sp,iD)
sz = sigmaz().full()
sx = sigmax().full()
Ex = dot(sp,sp.T)
ET = kron(eig(sx)[1],eig(sx)[1])+ 1j*zeros([4,4])

Ex1 = dot(dot(ET,kron(Ex,iD)),ET.T)
Ex2 = dot(dot(ET,kron(iD,Ex)),ET.T)
ODif = Ex2-Ex1
h0 = dot(dot(ET,(kron(wc*Ex,iD)+kron(iD,wc*Ex)
	+h*(kron(sp,sp.T)+kron(sp.T,sp)))),ET.T)
Current= -1j *h* dot(dot(ET,(dot(sp1.T,sp2)-dot(sp2.T,sp1))),ET.T)

OcD = []
Oc1 = []
Oc2 = []
Cur = []
TIME = []
for i in range(N):
	OcD.append([])
	Oc1.append([])
	Oc2.append([])
	Cur.append([])
	TIME.append([])

for i in range(N):
	tempD = []
	tempC = []
	temp1 = []
	temp2 = []
	tempt = []
	for j in range(len(data[i][0])):
		temp1.append(real(trace(dot(Ex1,data[i][1][j].reshape([4,4])))))
		temp2.append(real(trace(dot(Ex2,data[i][1][j].reshape([4,4])))))
		tempD.append(real(trace(dot(ODif,data[i][1][j].reshape([4,4])))))
		tempC.append(real(trace(dot(Current,data[i][1][j].reshape([4,4])))))
		tempt.append(data[i][0][j])
	OcD[i].append(array(tempD,dtype=float))
	Oc1[i].append(array(temp1,dtype=float))
	Oc2[i].append(array(temp2,dtype=float))
	Cur[i].append(array(tempC,dtype=float))
	TIME[i].append(tempt)

#average values
# avgOcDif= []
# avgCur = []
# avgOc1 = []
# avgOc2 = []
# cutOff = 400
# for i in range(N):
# 	avgOcDif.append([mean(OcD[i][0][cutOff:]),std(OcD[i][0][cutOff:])])
# 	avgCur.append([mean(Cur[i][0][cutOff:]),std(Cur[i][0][cutOff:])])
# 	avgOc1.append([mean(Oc1[i][0][cutOff:]),std(Oc1[i][0][cutOff:])])
# 	avgOc2.append([mean(Oc2[i][0][cutOff:]),std(Oc2[i][0][cutOff:])])
# print('Oc dif, warm to cold: {}'.format(avgOcDif))
# print('current, warm to cold: {}'.format(avgCur))
# print('Oc 1, warm to cold: {}'.format(avgOc1))
# print('Oc 2, warm to cold: {}'.format(avgOc2))


plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 16})
plt.figure(1)
plt.ylabel('$n_2-n_1$')
plt.xlabel('$Time [\\omega_c^{-1}]$')
for i in range(N):
	plt.plot(TIME[i][0], OcD[i][0], 'C{}'.format(i+1),label = 'OcDif;Temp = {:.2f}'.format(Temp[i]))
plt.legend(bbox_to_anchor=(0.55, 0.75), loc=2, borderaxespad=0.)
plt.savefig('Ocif.png')
plt.figure(3)
plt.ylabel('$n_i$')
plt.xlabel('$Time [\\omega_c^{-1}]$')
for i in range(N):
	plt.plot(TIME[i][0], Oc1[i][0], 'C{}'.format(i+1),label = 'Oc1;Temp = {:.2f}'.format(Temp[i]))
	plt.plot(TIME[i][0], Oc2[i][0], 'C{}'.format(i+3),label = 'Oc2;Temp = {:.2f}'.format(Temp[i]))
plt.legend(bbox_to_anchor=(0.55, 0.75), loc=2, borderaxespad=0.)
plt.savefig('Oc.png')
plt.figure(2)
plt.ylabel('I')
plt.xlabel('$Time [\\omega_c^{-1}]$')
for i in range(N):
	plt.plot(TIME[i][0], Cur[i][0], 'C{}'.format(i+1),label = 'Cur;Temp = {:.2f}'.format(Temp[i]))
plt.legend(bbox_to_anchor=(0.55, 0.75), loc=2, borderaxespad=0.)
plt.savefig('Cur.png')
plt.show()
