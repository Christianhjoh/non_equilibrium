import pickle
from numpy import (array, exp, trace, dot, eye, kron, zeros, 
	real, linspace,std, append, all, shape, mean, std)
from numpy.linalg import eig
from qutip import sigmap, sigmaz, sigmax
from time import time
import matplotlib.pyplot as plt
import mpmath as mp
mp.pretty = True
Temp = [0.4,0.3,0.2]
N = len(Temp)
data = []
val = []
for ii in range(N):
	NAME = '2S2E_weak_R2_T'+str(Temp[ii]).replace('.','')
	data.append(pickle.load(open('Tempo_'+NAME+'_statedat_dkm32prec55.pickle','rb')))
wc = 1
h= 0.5
#Function creating matrix representaion of relevant operators 
iD = eye(2)
sp = sigmap().full()
sp2 = kron(iD,sp)
sp1 = kron(sp,iD)
sz = sigmaz().full()
sx = sigmax().full()
Ex = dot(sp,sp.T)
ET = kron(eig(sx)[1],eig(sx)[1])+ 1j*zeros([4,4])

Ex1 = dot(dot(ET,kron(Ex,iD)),ET.T)
Ex2 = dot(dot(ET,kron(iD,Ex)),ET.T)
ODif = Ex2-Ex1
h0 = dot(dot(ET,(kron(wc*Ex,iD)+kron(iD,wc*Ex)
	+h*(kron(sp,sp.T)+kron(sp.T,sp)))),ET.T)
Current= -1j *h* dot(dot(ET,(dot(sp1.T,sp2)-dot(sp2.T,sp1))),ET.T)

OcD = []
Oc1 = []
Oc2 = []
Cur = []
TIME = []
for i in range(N):
	OcD.append([])
	Oc1.append([])
	Oc2.append([])
	Cur.append([])
	TIME.append([])

for i in range(N):
	tempD = []
	tempC = []
	temp1 = []
	temp2 = []
	tempt = []
	for j in range(len(data[i][0])):
		temp1.append(real(trace(dot(Ex1,data[i][1][j].reshape([4,4])))))
		temp2.append(real(trace(dot(Ex2,data[i][1][j].reshape([4,4])))))
		tempD.append(real(trace(dot(ODif,data[i][1][j].reshape([4,4])))))
		tempC.append(real(trace(dot(Current,data[i][1][j].reshape([4,4])))))
		tempt.append(data[i][0][j])
	OcD[i].append(array(tempD,dtype=float))
	Oc1[i].append(array(temp1,dtype=float))
	Oc2[i].append(array(temp2,dtype=float))
	Cur[i].append(array(tempC,dtype=float))
	TIME[i].append(tempt)

#average values
avgOcDif= []
avgCur = []
avgOc1 = []
avgOc2 = []
cutOff = 50
for i in range(N):
	avgOcDif.append([mean(OcD[i][0][:-cutOff]),std(OcD[i][0][:-cutOff])])
	avgCur.append([mean(Cur[i][0][:-cutOff]),std(Cur[i][0][:-cutOff])])
	avgOc1.append([mean(Oc1[i][0][:-cutOff]),std(Oc1[i][0][:-cutOff])])
	avgOc2.append([mean(Oc2[i][0][:-cutOff]),std(Oc2[i][0][:-cutOff])])
# print('Oc dif, warm to cold: {}'.format(avgOcDif))
# print('current, warm to cold: {}'.format(avgCur))
# print('Oc 1, warm to cold: {}'.format(avgOc1))
# print('Oc 2, warm to cold: {}'.format(avgOc2))


plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 40})
col=['C0--','C1','C3']
# plt.figure(1)
# plt.ylabel('$n_2-n_1$')
# plt.xlabel('Time $[\\omega_c^{-1}]$')
# for i in range(N):
# 	plt.plot(TIME[i][0], OcD[i][0], col[i],label = '$T_1$ = {:.2f} $\\omega_c$'.format(Temp[i]))
# plt.savefig('OcDif_Tempo2E.svg')
# plt.figure(2)
# plt.ylabel('I $[\\omega_c]$')
# plt.xlabel('Time $[\\omega_c^{-1}]$')
# for i in range(N):
# 	plt.plot(TIME[i][0], Cur[i][0], col[i],label = '$T_1$ = {:.2f} $\\omega_c$'.format(Temp[i]))
# lgd=plt.legend(bbox_to_anchor=(-0.4, 0.9),ncol=3, loc=2, borderaxespad=0.5)
# plt.savefig('Cur.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
a=350
b=570
plt.figure(3)
plt.plot(TIME[1][0][a:b], OcD[1][0][a:b], col[2])
plt.savefig('OcDif_Inset_Tempo2E.svg')

# plt.ylabel(r'$n_2-n_1$')
# plt.xlabel(r'$T_1 \;[\omega_c]$')
# plt.plot(Temp[:cutOffOCC], Occh1[:cutOffOCC], 'C1',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
# plt.plot(Temp[:cutOffOCC], Occh2[:cutOffOCC], 'C0',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[1]))
# plt.plot(Temp[:cutOffOCC], ODl[:cutOffOCC], 'C1--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
# plt.plot(Temp[:cutOffOCC], ODh[:cutOffOCC], 'C0--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[1]))
# lgd=plt.legend(bbox_to_anchor=(-0.4, 0.9),ncol=4, loc=2, borderaxespad=0.5)
# plt.savefig('n_dif.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
# plt.figure(2)
# plt.ylabel(r'$I\; [\omega_c]$')
# plt.xlabel(r'$T_1\; [\omega_c]$')
# plt.plot(Temp[:cutOffCur
# ], Curh1[:cutOffCur], 'C1',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
# plt.plot(Temp[:cutOffCur
# ], Curh2[:cutOffCur], 'C0',label = '$NEQFT: \\epsilon$ = {}$\omega_c$'.format(hop[1]))
# plt.plot(Temp[:cutOffCur
# ], Curl[:cutOffCur], 'C1--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[0]))
# plt.plot(Temp[:cutOffCur
# ], Curh[:cutOffCur], 'C0--',label = '$MME: \\epsilon$ = {}$\omega_c$'.format(hop[1]))

# lgd=plt.legend(bbox_to_anchor=(-0.4, 0.9),ncol=4, loc=2, borderaxespad=0.5)
# plt.savefig('Cur.svg',bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.show()