import pickle 
from numpy import (trace, dot, array,zeros,
	real,imag, append, arange, identity, kron, linspace,shape)
import matplotlib.pyplot as plt
import lineshapes as lp
import scipy
from mpmath import *
from mpmath import coth,cos,sin
mp.pretty = True

t = linspace(0,6,600)
g = 0.02
width = 2
T=0.5

def J(E):
	return g* E**3*exp(-(E/width)**2)
def eta1(t):
	return lp.numint(t,T,J)
def fo(w,t):
	return J(w)*(coth(w/(2*T))*cos(w*t)-1j*sin(w*t))
def A(t):
	numir = scipy.integrate.quad(lambda w: fo(w,t).real,0,inf)
	numii = scipy.integrate.quad(lambda w: fo(w,t).imag,0,inf)
	return numir[0]+1j*numii[0]

AF = []
LS = []
for i in range(len(t)):
	LS.append(eta1(t[i]))
	AF.append(A(t[i]))
LS = array(LS,dtype=complex)
AF = array(AF,dtype=complex)
plt.style.use('ggplot')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 20})

plt.figure(1)
plt.ylabel(r'Lineshape')
plt.xlabel(r'Time $[\omega_c^{-1}]$')
plt.plot(t,real(LS),'C1',label='Real part')
plt.plot(t,imag(LS),'C0--',label='Imaginary part')
plt.legend(bbox_to_anchor=(0.55, 0.75), loc=2, borderaxespad=0.)
#plt.savefig('Lineshape.svg')

plt.figure(2)
plt.ylabel(r'AutoCorrelation')
plt.xlabel(r'Time $[\omega_c^{-1}]$')
plt.plot(t,real(AF),'C1',label='Real part')
plt.plot(t,imag(AF),'C0--',label='Imaginary part')
plt.legend(bbox_to_anchor=(0.55, 0.75),ncol=2, loc=2, borderaxespad=0.)
#plt.savefig('AutoCorrelation.svg')
plt.show()