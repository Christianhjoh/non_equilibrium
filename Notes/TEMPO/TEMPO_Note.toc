\contentsline {section}{\numberline {1}General non-Markovian effects in open systems and model}{1}
\contentsline {subsection}{\numberline {1.1}Theoretical model}{2}
\contentsline {section}{\numberline {2}Effective evolution}{2}
\contentsline {section}{\numberline {3}Influence functional}{3}
\contentsline {section}{\numberline {4}Numerical evaluation scheme}{8}
\contentsline {subsection}{\numberline {4.1}Matrix Product States}{8}
\contentsline {subsubsection}{\numberline {4.1.1}Matrix Product Operators}{9}
\contentsline {subsection}{\numberline {4.2}Reduced system evolution using Matrix Product Operators}{10}
\contentsline {subsubsection}{\numberline {4.2.1}Augmented Density Tensor}{11}
\contentsline {subsubsection}{\numberline {4.2.2}Finite memory}{12}
\contentsline {section}{\numberline {5}Example computation}{12}
\contentsline {subsection}{\numberline {5.1}Computation parameter optimisation}{13}
\contentsline {subsection}{\numberline {5.2}Dynamics}{13}
\contentsline {section}{\numberline {6}Conclusion}{14}
