\section{Effective evolution}
To have a true non-equilibrium description of the system one needs the density matrix, which encodes all information about the systems state at that specific time. Given the Hamiltonian and the initial state, finding this element for a specific time ($T$) is the goal of the computation. The class of initial state which will be considered is of a form which factorises and in which the environment is initially thermalised at the inverse temperature $\beta$.\par
As the Hamiltonian is time independent the time evolution operator is a simple exponential operator
\begin{equation}
U(T)=e^{-i H T},
\end{equation}     
where the units have been chosen such that $\hbar=1$ throughout the project. The density matrix of the entire system for the an arbitrary later time is then  
\begin{equation}
\chi (T) = U(T)\chi_0 U^{\dagger}(T),
\end{equation}
where it is assumed that the initial time is $t=0$. The reduced density matrix is obtained by tracing over the environment Hilbert space
\begin{equation}
\rho(T)= \Tr_E \chi (T) = \Tr_E U(T)(\rho_0\otimes \mu_\beta) U^{\dagger}(T).
\end{equation}
Here $\rho_0$ is the initial state of the system and $\mu_\beta$ is the initial thermal state at temperature $1/\beta$ for the harmonic environment.
As the evolution operator satisfies the semigroup property (composition condition) \cite{Breuer}
\begin{equation}
U(t+s)=U(t)U(s),
\end{equation} 
it can be split into $N_1$ equal pieces which corresponds to dividing the time range: $T/N_1=\Delta$. The evolution operator then takes the form 
\begin{equation}
\begin{aligned}
U(T)&=\left(e^{-iH \Delta}\right)^{N_1}=\left(e^{-iH_0 \Delta}e^{-iH_E \Delta}+\mathcal{O}(\left[H_0 \Delta,H_E\Delta\right])\right)^{N_1}=\left(e^{-iH_0 \Delta}e^{-iH_E \Delta}+\mathcal{O}(\Delta^2)\right)^{N_1}\\
&\approx \left(e^{-iH_0 \Delta}e^{-iH_E \Delta}\right)^{N_1}=\left(U_s U_E\right)^{N_1}.
\end{aligned}
\end{equation}
By considering small enough time steps the error related to the fact that $H_0$ an $H_E$ do not commute can be neglected. This is called a Trotter expansion. Under the constraint that $N_1$ is sufficiently high the two exponentials ordering can be exchanged as needed.\par
The next realisation is that as $A_s$ is Hermitian, its eigenvectors is a complete orthonormal basis for $\mathcal{H}_s$. Denote the eigenvectors and eigenvalues as 
\begin{equation}\label{eq:A_Id_Eigensystem}
A_s \ket{s}=s\ket{s}, \quad \mathds{1}=\sum_{s}\ket{s}\bra{s},
\end{equation}   \par
Now a functional integration giving an element of the reduced density matrix ($\rho_{nm}(T)$) is constructed using the eigenstates of $A_s$
\begin{equation}
\rho_{nm}(T)=\Tr_E\bra{s_n}U(T)(\rho_0\otimes \mu_\beta) U^{\dagger}(T)\ket{s_m}.
\end{equation}
Trotterising the evolution and inserting system identity resolutions between the evolution operators
\begin{equation}
\begin{aligned}
\rho_{nm}(T)&=\Tr_E\sum_{\mathclap{\substack{s_0^+...s_{N_1-1}^+\\ s_0^-...s_{N_1-1}^-}}}\bra{s_n}U_s U_E\ket{s_{N_1-1}^+}\bra{s_{N_1-1}^+}U_sU_E\ket{s_{N_1-2}^+}\bra{s_{N_1-2}^+}U_sU_E\ket{s_{N_1-3}^+}...\bra{s_{1}^+}U_sU_E\ket{s_{0}^+}\\
&\times \bra{s_{0}^+}(\rho_0\otimes \mu_\beta)\ket{s_{0}^-}\bra{s_{0}^-}U_E^\dagger U_s^\dagger\ket{s_{1}^-}...\bra{s_{N_1-1}^-}U_E^\dagger U_s^\dagger\ket{s_m}.
\end{aligned}
\end{equation} 
The plus index have been used to signify the identity is being used to expand $U$ while the minus index is used for $U^\dagger$. As $U_E ^{(\dagger)}$ only contains one system operator, which is $A_s$, it is replaced by the eigenvalue of the appropriate eigenstate. After this replacement the system and environment parts can be factorised without introducing errors as the system eigenvalue in the environment is just a number
 \begin{equation}\label{eq:rhonm}
 \begin{aligned}
 \rho_{nm}(T)&=\sum_{\mathclap{\substack{s_0^+...s_{N_1-1}^+\\ s_0^-...s_{N_1-1}^-}}}\bra{s_n}U_s \ket{s_{N_1-1}^+}\bra{s_{N_1-1}^+}U_s\ket{s_{N_1-2}^+}\bra{s_{N_1-2}^+}U_s\ket{s_{N_1-3}^+}...\bra{s_{1}^+}U_s\ket{s_{0}^+}\\
 &\quad\quad\times \bra{s_{0}^+}\rho_0\ket{s_{0}^-}\bra{s_{0}^-} U_s^\dagger\ket{s_{1}^-}...\bra{s_{N_1-1}^-} U_s^\dagger\ket{s_m}\\
 &\quad\quad\times\Tr_E U_E(s_{N_1-1}^+)U_E(s_{N_1-2}^+)...U_E(s_{0}^+)\mu_\beta U_E^\dagger(s_{0}^-)...U_E^\dagger(s_{N_1-1}^-),\\
&=\sum_{\mathclap{\substack{s_0^+...s_{N_1-1}^+\\ s_0^-...s_{N_1-1}^-}}}\bra{s_n}U_s \ket{s_{N_1-1}^+}\bra{s_{N_1-1}^+}U_s\ket{s_{N_1-2}^+}\bra{s_{N_1-2}^+}U_s\ket{s_{N_1-3}^+}...\bra{s_{1}^+}U_s\ket{s_{0}^+}\\
 &\quad\quad\times \bra{s_{0}^+}\rho_0\ket{s_{0}^-}\bra{s_{0}^-} U_s^\dagger\ket{s_{1}^-}...\bra{s_{N_1-1}^-} U_s^\dagger\ket{s_m}\mathcal{I}\{s_i^{\pm},\Delta\},
 \end{aligned}
\end{equation} 
where $\mathcal{I}$ is similar to what Feynman and Vernon \cite{Feynman} denotes the influence functional. This contains all the effects of the environment and it gives an effective starting point for numerical investigations of the reduced system. 
Feynman and Vernons calculation was published in 1963 and therefore uses some uncommon methods. The first thing done in this note is therefore to rederive their results using methods more appropriate to the setting, namely Keldysh field theory. For comparison Feynman and Vernons result is 
\begin{equation}\label{eq:FV_result}
\mathcal{I}=\exp\left[-\int_{0}^{T}dt\int_{0}^{t}dz s^+(t)\alpha(t-z)s^+(z)+s^-(t)\bar{\alpha}(t-z)s^-(z)-s^-(t)\alpha(t-z)s^+(z)-s^+(t)\bar{\alpha}(t-z)s^-(z)\right],
\end{equation}
where $\alpha$ is the environment response function
\begin{equation}
\alpha(t)=\int_0^\infty d\nu J(\nu)\left(\coth \frac{\beta \nu}{2}\cos \nu t-i \sin \nu t\right).
\end{equation}