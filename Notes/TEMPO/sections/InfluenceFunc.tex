\begin{figure} 
	\centering
	\includegraphics[width=0.6\linewidth]{s_parameterisation.png}
	\caption{\textit{The discretised evolution operator $U_E$ for the influence functional. A similar evolution is applied in the opposite direction by $U_E^\dagger$.}}
	\label{fig:s_param}
\end{figure}
\section{Influence functional}
To compute the influence functional the first simplification is realising that the environment consists of non-interacting particles, one can therefore consider a single mode and take the product over all modes in the end. Next it is necessary to separate the following computation from the system Trotter splitting. This is done by parametrising $s^{\pm}$. The purpose of the parametrisation is to make it possible to do a new Trotter expansion for the environment evolution which is independent of the system Trotter expansion. This is important because this allows one to find an exact effective evolution.
The effect of the parametrisation is that the discrete arrays ($s^\pm_i$) becomes two functions of time. They are piecewise functions defined over the entire time range of the evolution 
\begin{equation}
s^{\pm}(t)=\begin{cases}
s_0^\pm & 0\le t < \Delta\\
s_{1}^\pm & \Delta \le t < 2\Delta\\
s_2^\pm & 2\Delta \le t < 3\Delta\\
\cdot \\
\cdot\\
s_{N_1-1}^{\pm}&(N_1-1)\Delta\le t \le N_1\Delta
\end{cases}.
\end{equation}
The structure allowing for this parametrisation is visualized in figure \ref{fig:s_param}. 
The effect of having introduced this parametrisation is that the environment evolution operator for a single mode takes on a continuum form
\begin{equation}
\begin{gathered}\label{eq:UE_T}
U_E (T)= \exp \left[-i\nu T b^\dagger b-i\int_{0}^{T}s^+(t)dt\left(g b+\bar{g}b^\dagger\right)\right],\\
U_E^\dagger (T)= \exp \left[i\nu T b^\dagger b+i\int_{0}^{T}s^-(t)dt\left(g b+\bar{g}b^\dagger\right)\right].
\end{gathered}
\end{equation}    
With this evolution operator the influence functional is given by
\begin{equation}\label{eq:InfluenceTrace}
\mathcal{I}=\Tr U_E(T)\mu_\beta U_E^\dagger (T)=\Tr U_E^\dagger (T) U_E(T)\mu_\beta.
\end{equation} 
Introducing the time contour in figure \ref{fig:Time_contour} the influence functional can be evaluated by Trotter expanding the evolution operator along this contour. This approach guarantees that no restrictions are put on the final state of the environment, such that true non-equilibrium behaviour is allowed.
\par 
To evaluate $\mathcal{I}$ using a path integral approach the operators in \eqref{eq:UE_T} must be changed into complex numbers. As the three different terms in \eqref{eq:UE_T} do not commute they have no mutual eigenstates. The right approach is instead to use coherent states as they are right (left) eigenstates of the annihilation (creation) operator. This works when the exponential can be split and when the Hamiltonian is put on a normal ordered form. The unnormalised coherent state is given by
\begin{equation}
 b\ket{\phi}=\phi\ket{\phi},\quad\ket{\phi}=e^{\phi b^\dagger}\ket{0}.
\end{equation}
The reason for using unnormalised coherent states is that the final expression will involve having infinitely many coherent states. The normalisation factors would then give rise to unnecessary infinities.  
The coherent states are not orthogonal but instead have the overlap
\begin{equation}
\bra{\theta}\ket{\phi}=e^{\bar{\theta}\phi},
\end{equation}
which means that the coherent states constitute an over-complete basis for the Hilbert space. The identity resolution takes this into account by an overcounting factor
\begin{equation}
\mathds{1}=\int d[\bar{\phi},\phi]e^{-\bar{\phi}\phi}\ket{\phi}\bra{\phi},
\end{equation}  
where integral is over the entire complex region $d[\bar{\phi},\phi]=dRe(\phi)dIm(\phi)/\pi$. Using this identity one can easily find the form of the trace in the coherent state basis.\par
\begin{figure} 
	\centering
	\includegraphics[width=0.5\linewidth]{TimeContour.png}
	\caption{\textit{The time contour used to evaluate the Influence functional \eqref{eq:InfluenceTrace}}}
	\label{fig:Time_contour}
\end{figure}
This is all the fundamental framework necessary to find the influence functional. The first step is Trotter expanding the evolution operator \eqref{eq:UE_T} into $N$ pieces such that $T=\delta N$. The $k$'th factor in the Trotter expansion then takes the form
\begin{equation}
U_E(k\delta)=e^{-i\nu b^\dagger b \delta}e^{-i\delta s^+(k\delta) g b }e^{-i\delta s^+(k\delta) \bar{g} b^\dagger}.
\end{equation}
Splitting the exponential into three and shuffling them between each other introduces errors of $\mathcal{O}(\delta^2)$. To obtain this degree of freedom it is necessary to take the limit $N\to\infty$. This will formally be done towards the end of the calculation but is useful to keep in mind. 
Inserting coherent state identity resolutions in between the evolution operators in $\mathcal{I}$ 
\begin{equation}
\begin{aligned}
\mathcal{I}=\int \prod_{k=0}^N d[\bar{\phi}_k^\pm,\phi_k^\pm]e^{-\bar{\phi}_k^\pm\phi_k\pm}&\bra{\phi_0^-}U_E^\dagger(0)\ket{\phi_1^-}\bra{\phi_1^-}U_E^\dagger(\delta)\ket{\phi_2^-}...\bra{\phi_N^-}\mathds{1}\ket{\phi_N^+}...\\
&\times\bra{\phi_1^+}U_E(0)\ket{\phi_0^+}\bra{\phi_0^+}\mu_\beta\ket{\phi_0^-},
\end{aligned}
\end{equation} 
where the trace results in the most left state and right state being identical. This means that at $t=0$ the two fields $\phi_0^+$ and $\phi_0^-$ are correlated through the initial state. Similarly the jump from one contour to the other at $T$ does not actually evolve the system in time, which gives rise to the identity correlation between the fields $\phi_N^-$ and $\phi_N^+$. 
Having used coherent states the overlaps are
\begin{equation}
\begin{aligned}
\bra{\phi_k^-}U_E^\dagger(k\delta)\ket{\phi_{k+1}^-}&=\exp\left[\bar{\phi}_{k}^-(1+i\nu\delta)\phi_{k+1}^-+i\delta s^-(k\delta)(g \phi_{k+1}^-+\bar{g}\bar{\phi}_k^-)\right],\\
\bra{\phi_{k+1}^+}U_E(k\delta)\ket{\phi_{k}^+}&=\exp\left[\bar{\phi}_{k+1}^+(1-i\nu\delta)\phi_{k}^+-i\delta s^+(k\delta)(g \phi_{k}^++\bar{g}\bar{\phi}_{k+1}^+)\right],\\
\bra{\phi_N^-}\mathds{1}\ket{\phi_N^+}&=\exp\left[\bar{\phi}_N^-\phi_N^+\right],\\
\bra{\phi_0^+}\mu_\beta\ket{\phi_0^-}&=(1-\kappa)\exp\left[\bar{\phi}_0^+\phi_0^-\kappa\right],
\end{aligned}
\end{equation}
having defined the thermal state factor $\kappa=e^{-\beta\nu}$.
Using these relations $\mathcal{I}$ can be written as a multidimensional Gaussian integral
\begin{equation}\begin{aligned}
\mathcal{I}&=\int \prod_{k=0}^{N} d[\bar{\phi}_k^\pm,\phi_k^\pm]\exp\left[-\sum_{\alpha,\beta}\bar{\phi}_\alpha M^{-1}_{\alpha,\beta}\phi_\beta+\sum_{\alpha}\bar{A}_\alpha\phi_\alpha+\bar{\phi}_\alpha B_\alpha\right](1-\kappa),\\
&=\frac{\exp\left[\sum_{\alpha,\beta}\bar{A}_\alpha M_{\alpha,\beta}B_\beta\right]}{\det(M^{-1})}(1-\kappa),
\end{aligned}
\end{equation}
where the sums are over both $k$ and the $\pm$ indices. The order is such that first the $k$ sum is done from 0 to $N$ with $+$ and afterwards the same sum with $-$.
To illustrate the form of the matrix and the vectors consider $N=2$ which corresponds to having two time evolutions on each leg
\begin{equation}
M^{-1}=\begin{pmatrix}
1&  0& 0 &-\kappa  &  0&0  \\ 
-h_-&1  &0  &0  &0  &0  \\ 
0&  -h_-&1  & 0 & 0 & 0 \\ 
0&0  &0  &  1&-h_+  &0  \\ 
0& 0 & 0 & 0 &  1&-h_+  \\ 
0& 0 &  -1& 0 & 0 & 1
\end{pmatrix},\quad
\bar{A}=i\delta g\begin{pmatrix}
-s^+(0)\\ 
-s^+(\delta)\\ 
0\\ 
0\\
s^-(0)\\ 
s^-(\delta)
\end{pmatrix},\quad  
B=i\delta \bar{g}\begin{pmatrix}
0\\ 
-s^+(0)\\ 
-s^+(\delta)\\ 
s^-(0)\\ 
s^-(\delta)\\ 
0
\end{pmatrix}, 
\end{equation}
where $h_\pm=1\pm i\nu \delta$. To solve the Gaussian integral the inverse of $M^{-1}$ and its determinant must be computed. The determinant can be found using $\det(A)=\exp\left[\Tr \log(M^{-1}-\mathds{1}+\mathds{1})\right]$ and the result is
\begin{equation}
\det(M^{-1})=1-(h_+ h_-)^{N+1}\kappa.
\end{equation}
Considering the form of $h_\pm$, the fact that $T=\delta N$ and the limit $N\to\infty$ the determinant is found to be
\begin{equation}
\det(M^{-1})=1-(1+\delta^2\nu^2)^{N+1}\kappa \overset{N\to\infty}{=}1-e^{T^2\nu^2/N}\kappa=1-\kappa,
\end{equation}
such that it exactly cancels the similar factor in $\mathcal{I}$ from the initial state overlap.\par
To evaluate the inverse of $M^{-1}$ one can write it as a $2\times2$ block matrix, which means that one just have to invert the two diagonal blocks \cite{Strang}. As these are triangular this is straightforward though a bit time consuming. To find the full inverse one just has to do matrix multiplication. 
The result for $N=2$ is 
\begin{equation}
M=\begin{pmatrix}
1 &h_-h_+^2\kappa  &h_+^2\kappa&\kappa  &h_+\kappa  &h_+^2\kappa  \\ 
h_-&1  &h_-h_+^2\kappa  &h_-\kappa  &h_+h_-\kappa  &h_+^2h_-\kappa  \\ 
h_-^2&h_-  &1  &h_-^2\kappa  &h_+h_-^2 \kappa &h_-^2h_+^2\kappa  \\ 
h_-^2h_+^2&h_-h_+^2  &h_+^2  &1  &h_+  &h_+^2  \\ 
h_-^2h_+& h_-h_+ &h_+  &h_-^2h_+\kappa  &1  &h_+  \\ 
h_-^2&h_-  &1  &h_-^2\kappa  &h_-^2h_+\kappa  &1 
\end{pmatrix}/\det(M^{-1}). 
\end{equation}
As two different class' of fields are considered ($\pm$) it is advantageous to think of $M$ as consisting of four blocks, where each block describe correlations among different fields
\begin{equation}
M=\begin{pmatrix}
M^{++}& M^{+-} \\ 
M^{-+}& M^{--}
\end{pmatrix}. 
\end{equation}
These four blocks can by inspection be expanded to arbitrary $N$
\begin{equation}
\begin{aligned}
M^{++}_{kl}&=\left(\theta(k\ge l)h_-^{k-l}+\theta(l>k)(h_+h_-)^Nh_-^{k-l} \kappa\right)/\det(M^{-1}),\\
M^{--}_{k,l}&=\left(\theta(l\ge k)h_+^{l-k}+\theta(k>l)(h_+h_-)^N h_+^{l-k}\kappa\right)/\det(M^{-1}),\\
M^{+-}_{k,l}&=\left( h_+^{l-1}h_-^{k-1}\kappa \right)/\det(M^{-1}),\\
M^{-+}_{k,l}&=\left( (h_-h_+)^{N+1} h_-^{-l}h_+^{-k} \right)/\det(M^{-1}).
\end{aligned}
\end{equation} 
Taking the limit $N\to\infty$  gives the continuum form of the different blocks and is done using that $\lim\limits_{N\to\infty}h_\pm=e^{\pm i\nu \delta}$
\begin{equation}
\begin{aligned}
M^{++}(t,t')&=\left(\theta(t-t')\exp\left[-i\nu(t-t')\right]+\theta(t'-t)\exp\left[-i\nu (t-t')\right] \kappa\right)/(1-\kappa)\\&=\exp\left[-i\nu(t-t')\right]\biggl(\theta(t-t')(1+n_B)+\theta(t'-t)n_B\biggr),\\
M^{--}_(t,t')&=\exp\left[-i\nu(t-t')\right]\biggl(\theta(t'-t)(1+n_B)+\theta(t-t')n_B\biggr),\\
M^{+-}(t,t')&=\exp\left[-i\nu(t-t')\right]n_B,\\
M^{-+}(t,t')&=\exp\left[-i\nu(t-t')\right]\left( 1+n_B\right).
\end{aligned}
\end{equation}
Taking the continuum limit of the vectors $\bar{A}$ and $B$ means that the boundary effects (the zeros) vanish as the interval becomes infinitesimal small (measure zero) so
\begin{equation}
\sum_{k}\bar{A}_{k}\phi_k=i g\int_0^T dt\begin{pmatrix}
-s^+(t)\\ 
s_-(t)
\end{pmatrix}^T  \begin{pmatrix}
\phi^+(t)\\ 
\phi_-(t)
\end{pmatrix},
\end{equation}
 and similar for the sum over $B$. The multidimensional Gaussian integral then becomes a functional Gaussian integral instead which is but a small extension \cite{Altland} 
 \begin{equation}
 \begin{aligned}
  \mathcal{I}&=\exp\Biggl[-\lvert g\rvert^2\int_0^T dt dt' s^+(t)M_{++}(t,t')s^+(t')+s^-(t)M^{--}s^{-}(t')\\
  &\qquad\qquad-s^+(t)M^{+-}(t,t')s^-(t')-s^-(t)M^{-+}(t,t')s^+(t')\Biggr],\\
  &=\exp\Biggl[-\lvert g\rvert^2\int_0^T\int_0^T dt dt' s^+(t)\exp\left[-i\nu(t-t')\right]\biggl(\theta(t-t')(1+n_B)+\theta(t'-t)n_B\biggr)s^+(t')\\&\qquad\qquad+s^-(t)\exp\left[-i\nu(t-t')\right]\biggl(\theta(t'-t)(1+n_B)+\theta(t-t')n_B\biggr)s^{-}(t')\\
  &\qquad\qquad-s^+(t)\exp\left[-i\nu(t-t')\right]n_Bs^-(t')-s^-(t)\exp\left[-i\nu(t-t')\right](1+n_B)s^+(t')\Biggr],\\
   &=\exp\Biggl[-\lvert g\rvert^2\int_0^T\int_0^t dt dt'\biggl\{ s^+(t)\biggl((1+n_B)e^{-i\nu(t-t')}+e^{i\nu(t-t')}n_B\biggr)s^+(t')\\&\qquad\qquad+s^-(t)\biggl(e^{i\nu(t-t')}(1+n_B)+e^{-i\nu(t-t')}n_B\biggr)s^{-}(t')\biggr\}\\
   &\qquad\qquad-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)e^{-i\nu(t-t')}n_Bs^-(t')-s^-(t)e^{-i\nu(t-t')}(1+n_B)s^+(t')\biggr\}\Biggr].\\
   %&=\exp\Biggl[-\lvert g\rvert^2\int_0^T\int_0^t dt dt' s^+(t)\biggl(e^{-i\nu(t-t')}+2n_B\cos\Bigl(\nu(t-t')\Bigr)\biggr)s^+(t')\\&\qquad\qquad+s^-(t)\biggl(e^{i\nu(t-t')}+2\cos\Bigl(\nu(t-t')\Bigr)n_B\biggr)s^{-}(t')\\
   %&\qquad\qquad-s^+(t)e^{-i\nu(t-t')}n_Bs^-(t')-s^-(t)e^{-i\nu(t-t')}(1+n_B)s^+(t')\Biggr],\\
 \end{aligned}
 \end{equation} 
Considering the form of the single bath mode autocorrelation function
 \begin{equation}\label{eq:alphaSinglemode}
 \begin{aligned}
 \alpha(t-t')&=\lvert g\rvert^2\left(\coth \frac{\beta \nu}{2}\cos\left(\nu (t-t')\right) -i \sin\left(\nu (t-t')\right)\right)\\&=\lvert g\rvert^2\left(n_B e^{-i\nu(t-t')}+n_B e^{i\nu (t-t')}+e^{-i\nu(t-t')}\right),
 \end{aligned}
 \end{equation} 
 one sees that the diagonal terms have exactly this form while the two off-diagonal terms do not
 \begin{equation}
 \begin{aligned}\label{eq:I_expression1}
  \mathcal{I}&=\exp\Biggl[-\int_0^T\int_0^t dt dt' s^+(t)\alpha(t-t')s^+(t')+s^-(t)\alpha(t-t')s^{-}(t')\\
  &\qquad\qquad-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)e^{-i\nu(t-t')}n_Bs^-(t')-s^-(t)e^{-i\nu(t-t')}(1+n_B)s^+(t')\biggr\}\Biggr].
 \end{aligned}
 \end{equation}
This differs from the correct expression \eqref{eq:FV_result} only by the off-diagonal elements being wrong. Notice that the off-diagonal terms in \eqref{eq:I_expression1} are integrated over the entire "square" area while the correct expression has a dynamic cut-off in one integral. As the $dt$ and $dt'$ integrals are identical the variables can be renamed the variables without affecting the integrals. Writing the off-diagonal elements in terms of $\alpha$ one finds
\begin{equation}
\begin{aligned}
\chi&=-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)e^{-i\nu(t-t')}n_Bs^-(t')-s^-(t)e^{-i\nu(t-t')}(1+n_B)s^+(t')\biggr\},\\&=-\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)\bar{\alpha}(t-t')s^-(t')-s^-(t)\alpha(t-t')s^+(t')\biggr\}\\
&-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{s^+(t)\left(n_B+1\right) e^{i\nu(t-t')}s^-(t')+s^-(t)e^{i\nu(t-t')}n_Bs^+(t')\biggr\}.
\end{aligned}
\end{equation}  
Exchanging the names of the variables gives
  \begin{equation}
  \begin{aligned}
  \chi&=-\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)\bar{\alpha}(t-t')s^-(t')-s^-(t)\alpha(t-t')s^+(t')\biggr\}\\
  &\qquad\qquad-\lvert g\rvert^2\int_0^T\int_0^T dt dt'\biggl\{s^+(t)\bigl(\left(n_B+1\right) e^{i\nu(t-t')}+e^{-i\nu(t-t')}n_B\bigr)s^-(t')\biggr\},\\
  &=-\int_0^T\int_0^T dt dt'\biggl\{-s^+(t)\bar{\alpha}(t-t')s^-(t')-s^-(t)\alpha(t-t')s^+(t')\biggr\}\\&\qquad\qquad-\int_0^T\int_0^T dt dt'\biggl\{s^+(t)\bar{\alpha}(t-t')s^-(t')\biggr\},\\
  &=-\int_0^T\int_0^T dt dt'-s^-(t)\alpha(t-t')s^+(t').
  \end{aligned}
  \end{equation}
Next the integration limit can be changed by splitting up the integral in two parts
 \begin{equation}
 \chi=-\int_0^T\int_0^t dt dt'-s^-(t)\alpha(t-t')s^+(t')-\int_0^T\int_t^T dt dt'-s^-(t)\alpha(t-t')s^+(t').
 \end{equation}
The first term has an integration region which corresponds to the blue area in figure \ref{fig:flip}, while the second term has an integration region corresponding to the red one. Because the total region is quadratic transforming the points from the red region to the blue region is done by exchanging $t$  and $t'$. 
\begin{figure} 
	\centering
	\includegraphics[width=0.3\linewidth]{flip.png}
	\caption{\textit{To transform a point from the red region to the blue region, by flipping it along the black line $a(t)=t$, one simply has to exchange the $t$ and $t'$ values.}}
	\label{fig:flip}
\end{figure}
Doing this transformation and using that $\alpha(t'-t)=\bar{\alpha}(t-t')$, which is seen from the definition \eqref{eq:alphaSinglemode}, one arrives at
\begin{equation}
\chi=-\int_0^T\int_0^t dt dt'-s^-(t)\alpha(t-t')s^+(t')-s^+(t)\bar{\alpha}(t-t')s^-(t').
\end{equation}
Inserting this result in \eqref{eq:I_expression1} the multimode influence functional now takes the form
\begin{equation}\label{eq:InfluenceFunc}
\begin{aligned}
\mathcal{I}&=\prod_i \exp\left[-\int_0^T dt\int_0^t dt'\Bigl(s^+(t)-s^-(t)\Bigr)\Bigl(\alpha_i(t-t')s^+(t')-\bar{\alpha_i}(t-t')s^-(t')\Bigr)\right],\\
&=\exp\left[-\int_0^T dt\int_0^t dt'\Bigl(s^+(t)-s^-(t)\Bigr)\Bigl(\alpha(t-t')s^+(t')-\bar{\alpha}(t-t')s^-(t')\Bigr)\right],
\end{aligned}
\end{equation}
where $\alpha_i$ is the single mode autocorrelation function \eqref{eq:alphaSinglemode} and and $\alpha$ is the autocorrelation function for the entire environment given by
\begin{equation}\label{eq:alpha}
\alpha(t)=\sum_i \lvert g_i\rvert^2\left(\coth \frac{\beta \nu_i}{2}\cos\left(\nu_i t\right) -i \sin\left(\nu_i t\right)\right)=\int_0^\infty d\omega J(\omega)\left(\coth \frac{\beta \omega}{2}\cos\left(\omega t\right) -i \sin\left(\omega t\right)\right),
\end{equation} 
with $J(\omega)$ being the environment spectral density. This is exactly the same result as the one derived by Feynman and Vernon \eqref{eq:FV_result}.\par 
The finally thing left to do before the influence functional is compatible with the reduced system computation is to enforce the original time discretisation
\begin{equation}
\begin{aligned}
\mathcal{I}&=\exp\left[-\sum_ {j=1}^{N_1}\sum_{k=1}^{j} \Bigl(s^+_j-s^-_j\Bigr)\Bigl(\eta_{j-k}s^+_k-\bar{\eta}_{j-k}s^-_k\Bigr)\right]=\prod_{j=1}^{N_1} \prod_{k=1}^{j} e^{-\phi_{j,k}},
\end{aligned}
\end{equation} 
with the influence phase given by
\begin{equation}\label{eq:influencePhase}
\phi_{j,k}=\Bigl(s^+_j-s^-_j\Bigr)\Bigl(\eta_{j-k}s^+_k-\bar{\eta}_{j-k}s^-_k\Bigr).
\end{equation} 
The $\eta$ coefficient is just the integral over the autocorrelation function between two different steps and has absorbed the $\Delta$ (time step length) factor from the discretisation to build the integrals
\begin{equation}
\eta_{j-k}=\begin{cases}
\int_{\Delta(j-1)}^{\Delta j}\int_{\Delta(k-1)}^{\Delta k}\alpha(t-t')d t'\,d t&j\neq k\\
\int_{\Delta(j-1)}^{\Delta j}\int_{\Delta(k-1)}^{t}\alpha(t-t')d t'\,d t&j\neq k\\
\end{cases}.
\end{equation}