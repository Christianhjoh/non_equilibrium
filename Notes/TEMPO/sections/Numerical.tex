\section{Numerical evaluation scheme}
Having found a closed form of the influence functional the environment effects can now be captured. What remains is to find an efficient numerical method for actually computing the reduced density matrix \eqref{eq:rhonm}. The method which will be described is one based on the tensor network formalism \cite{Orus}.For the present purpose a specific kind of tensor networks will be used, namely Matrix Product States (MPS). These give an efficient representation of 1-D states if long range entanglement is not present. After having described the necessary basics for MPS we will show how to formulate the time evolution of the reduced density matrix in this new language closely following the work presented in \cite{TEMPO}.  
\subsection{Matrix Product States}
\begin{figure} 
	\centering
	\includegraphics[width=0.50\linewidth]{MPS_MPO.png}
	\caption{\textit{Tensor network diagrammatic representation of $a)$ a MPS and $b)$ a MPO. The black lines correspond to physical inputs (specific state). The two grey boxes represent the regular tensor representation. The green circles is the matrices found by SVD of the tensor representation and the blue lines represent the bond indices.}}
	\label{fig:MPSMPO}
\end{figure}
Consider a one dimensional system with $N$ different modes 
\begin{equation}
\ket{\psi}=\sum_{i_1,...,i_N}C_{i_1,..,i_N}\bigotimes_{j=1}^N\ket{i_j},\quad \sum_{i_1,...,i_N}\abs{C_{i_1,..,i_N}}^2=1.
\end{equation}
One can represent this state in a way which makes the entanglement between the different modes very transparent.
The starting point is to think of $C_{i_1,...,i_N}$ as an $N$-rank array where the different index takes on $d_{k}$ different values. Here $d_{k}$ is the dimension of the $k$'th Hilbert space. The task at hand then corresponds to representing this array in a way which makes the entanglement between the different subspaces accessible. This is exactly what the Schmidt decomposition does \cite{Breuer}. The only thing is that the Schmidt decomposition is usually used when considering bipartite systems. To get around this successive Schmidt decompositions can be used.\par
The Schmidt decomposition is based on the mathematical concept of the Singular Value Decomposition (SVD) \cite{Strang}. This can be thought of as a generalised eigenvalue decomposition applicable for any matrix. The idea is that one allows two different orthogonal transformations, which are not linked by transposition, on either side of the diagonal (but possibly rectangular) "eigenvalue" matrix. Any matrix can then be decomposed in terms of these three transformations. Considering an arbitrary matrix $M$
\begin{equation}
M=U \Sigma V^T,
\end{equation}
Where $\Sigma$ is a diagonal matrix with real positive entries and $^T$ means the transposed matrix. These elements are denoted the singular values (instead of eigenvalues). As mentioned both $U$ and $V$ are orthogonal transformations but are not necessarily of the same dimension. $U$ consists of the eigenvectors of $MM^T$ and $V$ is build from eigenvectors of $M^TM$.\par 
Consider now a bipartite system where the two systems can be of different dimensionality 
\begin{equation}
\ket{\Theta}=\sum_{i,j} \alpha_{i,j} \ket{\phi_i^{(1)}}\otimes\ket{\phi_j^{(2)}}, 
\end{equation}   
Using the singular value decomposition of the $\alpha$ matrix
\begin{equation}
\ket{\Theta}=\sum_{i,j,k} U_{i,k}\Sigma_kV_{j,k}^T \ket{\phi_i^{(1)}}\otimes\ket{\phi_j^{(2)}}= \sum_{i,j,k}\Sigma_k \left(U_{i,k}\ket{\phi_i^{(1)}}\right)\otimes\left(V_{k,j}\ket{\phi_j^{(2)}}\right)=\sum_k\Sigma_k \ket{\tau_k^{(1)}}\otimes\ket{\tau_k^{(2)}}.
\end{equation}
This is  exactly the Schmidt decomposition where the number of non-zero elements in $\Sigma$ quantifies how entangled system 1 and 2 is (Schmidt number).\par Considering now the original $N$-constituents state, it can be written as a Schmidt decomposition between the $i_1$ system and the rest. Using the above notation the state is represented as
\begin{equation}
\ket{\psi}=\sum_{k_1} \Sigma_{k_1}\ket{\tau_{k_1}^{(1)}}\otimes\ket{\tau_{k_1}^{(2..N)}}=\sum_{k_1,i_1} U_{k1}^{i_1}\Sigma_{k_1}\ket{i_1}\otimes V_{k_1}^{i_2,..,i_N}\bigotimes_{j=2}^N\ket{i_j}.
\end{equation}
Consider the subsystem $\ket{i_2,..,i_N}$ and find a SVD of the right transformation ($V_{k_1}$)
\begin{equation}
V_{k_1}^{i_2,..,i_N}\ket{i_2,..,i_N}=\sum_{k_2}U_{k_1,k_2}^{i_2}\Sigma_{k_2}\ket{i_2}\otimes V_{k_2}^{i_3,...,i_N} \ket{i_3,..,i_N}.
\end{equation}
Repeatedly performing SVD's between all the $N$ subspaces the state takes the form
\begin{equation}
\ket{\psi}=\sum_{\{k_j\},\{i_j\}}U_{k_1}^{i_1}\Sigma_{k_1}U_{k_1,k_2}^{i_2}\Sigma_{k_2}....\Sigma_{k_{N-1}}U_{k_{N-1}}^{i_N}\bigotimes_{j=1}^N\ket{i_j},
\end{equation}
where the state is now represented as a product of matrices with a size that depends on the Schmidt number (number of non-zero singular values), which in this representation is denoted the bond number. $k_j$ is called the $j'th$ bond index. When the bond number corresponds to the Schmidt number the representation is named the canonical Matrix Product State (MPS) representation. It is obvious that the computational complexity of this representation is directly related to how entangled the different subsystems are. Furthermore by neglecting singular values below some appropriate cut-off (named the precision), one can approximate the state in a controlled manner. This is a key point for numerical computations.\par 
Changing representation from a single large array ($C_{i_1,..,i_N}$) to a MPS is graphical shown in figure \ref{fig:MPSMPO}.$a$. Here the tensor network diagrammatic language is used. The large array is decomposed into matrices (green ball which has two lines (indices) attached to it). The price for this decomposition is the introduction of the "unphysical" connections between the matrices (the bond indices). when a line is attached to a ball at both its ends it this index is contracted over.   
\subsubsection{Matrix Product Operators}
Computing the MPS for an arbitrary 1-D state directly is usually not possible as it requires the SVD of the full array ($C_{i_1,..,i_N}$). Instead the approach is to start of with some state for which the MPS is analytically known (or a product state) and then evolve this state into the wanted state. The reason this is efficient is that operators can be represented in a way that is compatible with the MPS representation. \par 
This concept can be explained very simply if one uses tensor network diagrams and is shown in figure \ref{fig:MPSMPO}.b. Applying the MPO is then done by connecting the state one wishes to evolve at the upper legs. These contractions gives the evolved state of interest. One can then perform a SVD of the system to find the most efficient presentation of the state. Constructing the specific MPO representation of an operator can be done very similarly as the MPS procedure using SVD but below we will use a different procedure more appropriate for our specific purpose.   
\subsection{Reduced system evolution using Matrix Product Operators} 
Having given a brief description of the wanted numerical structure we now seek to describe the time evolution of the reduced system as a MPS being evolved by a MPO.
The first step is to move to Liouville space as this changes many of the sums to matrix products which are much closer to the form of interest. The Liouville basis is spanned by outer products of the eigenstates ($s_i$) of the Hermitian interaction operator($A_s$). Let Liouvillian vectors be represented by a slightly modified Dirac notation   
\begin{equation}
\Lket{S_l}=\ket{s_i}\bra{s_j},\quad i,j\in d,\quad l\in D=d^2,
\end{equation} 
where $d$ is the dimension of the (reduced) system Hilbert space. In this space the evolution of the density matrix is described by the Liouvillian
\begin{equation}
\Lket{\rho(T)}=\Tr_E e^{\mathcal{L}T}\Lket{\rho_0}\Lket{\mu_\beta},
\end{equation}
which can be Trotter expanded ($T=\Delta N_1$). Taking the overlap with one of the basis states one arrive at the Liouville representation of \eqref{eq:rhonm}
\begin{equation}
\Lbraket{S_N}{\rho(\Delta N_1)}=\sum_{S_0...S_{N_1-1}}\prod_{j=1}^{N_1}\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{j-1}}\mathcal{I}(\{s_k\})\Lbraket{S_0}{\rho_0}.
\end{equation} 
$\mathcal{I}$ is the influence functional and the system evolution is found by comparing to \eqref{eq:rhonm}
\begin{equation}
\prod_{j=1}^{N_1}\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{j-1}}=\prod_{j=1}^{N_1}\bra{s^+_j}e^{-iH_0\Delta}\ket{s^+_{j-1}}
\bra{s^-_{j-1}}e^{iH_0\Delta}\ket{s^-_{j}}.
\end{equation}
Here notation have been changed from $s_{n/m}$ \eqref{eq:rhonm} to $s^{+/-}_{N_1}$.
Using the influence phase representation \eqref{eq:influencePhase} the overlap can be rewritten as
\begin{equation}
\begin{aligned}
\prod_{j=1}^{N_1}\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{j-1}}\mathcal{I}(\{s_k\})&=\prod_{j=1}^{N_1}\prod_{k=1}^{j}\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{j-1}}e^{-\phi_{j,k}}\\
&=\prod_{j=2}^{N_1}\prod_{k=1}^{j}\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{j-1}}\Lbra{S_1}e^{\mathcal{L}_0\Delta}\Lket{S_0}e^{-\phi_{1,1}}e^{-\phi_{j,k}}\\
&=\prod_{j=2}^{N_1}\prod_{k=1}^{j}\left[\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{k}}\delta_{j-1,k}+(1-\delta_{j-1,k})\right]e^{-\phi_{j,k}}e^{-\phi_{1,1}}\Lbra{S_1}e^{\mathcal{L}_0\Delta}\Lket{S_0}\\
&=\prod_{j=2}^{N_1}\prod_{\gamma=1}^{j-1}\left[\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{j-\gamma}}\delta_{\gamma,1}+(1-\delta_{\gamma,1})\right]e^{-\phi_\gamma(S_j,S_{j-\gamma})}e^{-\phi_0(S_j,S_j)}\\&\qquad\qquad\times e^{-\phi_0(S_1,S_1)}\Lbra{S_1}e^{\mathcal{L}_0\Delta}\Lket{S_0},
\end{aligned}
\end{equation}
where the influence phase has been redefined to 
\begin{equation}
\phi_{\gamma}(S_i,S_j)=\Bigl(s^+_i- s^-_i\Bigr)\Bigl(\eta_{\gamma}s^+_j-\bar{\eta}_{\gamma}s^-_j\Bigr),
\end{equation} 
and in the last step the product have been changed to be over the difference $\gamma=j-k$ instead of $k$.
Inserting this into the overlap computation one finds
\begin{equation}
\begin{aligned}
\Lbraket{S_N}{\rho(\Delta N_1)}=\sum_{S_0...S_{N_1-1}}\prod_{j=2}^{N_1}\prod_{\gamma=1}^{j-1}&\left[\Lbra{S_j}e^{\mathcal{L}_0\Delta}\Lket{S_{j-\gamma}}\delta_{\gamma,1}+(1-\delta_{\gamma,1})\right]e^{-\phi_\gamma(S_j,S_{j-\gamma})}e^{-\phi_0(S_j,S_j)}\\&\times e^{-\phi_0(S_1,S_1)}\Lbra{S_1}e^{\mathcal{L}_0\Delta}\Lket{S_0}\Lbraket{S_0}{\rho_0}.
\end{aligned}
\end{equation}
This can be simplified by realising that the ordering in the Trotter expansion determines whether the environment or system is one step behind. Choosing it so the environment is one step behind, the $S_0$ sum can be used to generate an identity and the initial state can be evolved one step forward by the system Liouvillian
\begin{equation}\label{eq:DensityMatrixComponent}
\begin{aligned}
\Lbraket{S_N}{\rho(\Delta N_1)}=\sum_{S_1...S_{N_1-1}}\prod_{j=2}^{N_1}\prod_{\gamma=1}^{j-1}I_\gamma(S_j,S_{j-\gamma}) I_0(S_1,S_1)\Lbraket{S_1}{\rho(\Delta)},
\end{aligned}
\end{equation}
with 
\begin{equation}
I_\gamma(S_i,S_j)=\begin{cases}
\Lbra{S_i}e^{\mathcal{L}_0\Delta}\Lket{S_j}e^{-\phi_1(S_i,S_j)-\phi_0(S_i,S_i)},&\gamma=1\\
e^{-\phi_\gamma(S_i,S_j)},&\gamma\neq1
\end{cases}.
\end{equation}
The $j$-sum can be thought of as what actually evolves the system the $N_1$ time steps, while the $\gamma$-sum contains all the memory effects of the environment due to previous interactions.
\subsubsection{Augmented Density Tensor}
\begin{figure} 
	\centering
	\includegraphics[width=0.9\linewidth]{LambdaMPS.png}
	\caption{\textit{A figure illustrating how to get from a general tensor ($a$) to a MPS presentation ($b$) and finally to a MPO $(c)$. Connected legs and a leg ending with a black dot means the index is contracted.}}
	\label{fig:lambdaMP}
\end{figure}
The goal is to put \eqref{eq:I_expression1} on a form that is similar to a 1-D state, as this will allow for a MPS representation. The insight from the previous section made it apparent that the $j$ product in \eqref{eq:DensityMatrixComponent} was what evolved the reduced system. With this in mind a new object $\lambda_j$ is defined which is an array with $j$ indices, each taking $D$ different values
\begin{equation}
(\lambda_j)_{S_j,S_{j-1},..S_1}=\prod_{\gamma=1}^{j-1}I_\gamma(S_j,S_{j-\gamma})\rightarrow\Lbraket{S_N}{\rho(\Delta N_1)}=\sum_{S_1..S_{N_1-1}}\prod_{j=1}^{N_1}(\lambda_j)_{S_j..S_1},
\end{equation}
where $(\lambda_1)_{S_1}=I_0(S_1,S_1)\Lbraket{S_1}{\rho(\Delta)}$. \par Considering $\lambda_i$ this is a tensor that takes as input a specific configuration of the previous states of the system (choice of $S_1,..,S_{i-1}$ values) and then outputs the overlap with a specific current configuration (the choice of $S_4$). Physically it connects the previous configurations with the present configuration. To make this connection apparent $\lambda_i$ is written in the tensor network language.\par 
$\lambda_1$ being a vector means that it is already MPS. This does not translate to $\lambda_{j>1}$ which must be translated to a form that is compatible with the MPS structure. To describe this translation consider the specific element
\begin{equation}
(\lambda_4)_{S_4,S_3,S_2,S_1}=I_{3}(S_4,S_1)I_2(S_4,S_2)I_1(S_4,S_3).
\end{equation} 
This tensor is illustrated in figure \ref{fig:lambdaMP}.a. To change the form of this tensor into one similar to figure \ref{fig:lambdaMP}.b one can use the Kronecker-Delta to introduce internal horizontal indices 
\begin{equation}
(\lambda_4)_{S_4,S_3,S_2,S_1}=\sum_{\alpha_1,\alpha_2,\alpha_3}I_3(\alpha_1,S_1)\delta_{\alpha_1,\alpha_2}I_2(\alpha_2,S_2)\delta_{\alpha_2,\alpha_3}I_1(\alpha_3,S_3)\delta_{\alpha_3,S_4}.
\end{equation}
This element has a form similar to a MPS and not a MPO. This means it only gives a scalar output, whereas a state containing previous histories is the sought element. This can be achieved by representing $\lambda$ as a MPO like figure \ref{fig:lambdaMP}.c. Again this can be achieved using the Kronecker-Delta but this time to create vertical indices
\begin{equation}
\begin{aligned}
(\lambda_4)_{S_4,S_3,S_2,S_1}=\sum_{\alpha_i,S_i'}&\left[I_3(\alpha_1,S_1')\delta_{\alpha_1,\alpha_2}\delta_{S_1',S_1}\right]\left[I_2(\alpha_2,S_2')\delta_{\alpha_2,\alpha_3}\delta_{S_2',S_2}\right]\\&\times\left[I_3(\alpha_3,S_3')\delta_{\alpha_3,\alpha_4}\delta_{S_3',S_3}\right]\left[\delta_{\alpha_4,S_4'}\delta_{S_4',S_4}\right]=\sum_{S_i'}(\Lambda_{4})_{S_1,S_2,S_3,S_4}^{S_1',S_2',S_3'}.
\end{aligned}
\end{equation}
\begin{figure} 
	\centering
	\includegraphics[width=0.7\linewidth]{EvolutionMPO.png}
	\caption{\textit{Building the augmented density tensor for 4 time steps. Each time step corresponds to lengthening the MPS once. To Find the MPS one has to contract over the previous evolutions (grey box). This gives the MPS representation of $\mathcal{A}$ for the given time.}}
	\label{fig:A_evolution}
\end{figure}

The tensor $\Lambda$ takes as input an element from a Hilbert space consisting of $i-1$ copies of the system space and moves it into a space consisting of $i$ copies. One can think of the product over $j$ as a large array
\begin{equation}
\sum_{S_{N_1-1}'..S_1'}\prod_{j=1}^{N_1}(\Lambda_j)_{S_{j-1}'..S_1'}^{S_j,S_{j-1}..S_1}=\mathcal{A}^{S_1..S_{N_1}}.
\end{equation}
This array contains the overlaps between different time configurations and can be used to create an element named the  augmented density tensor (ADT) \cite{TEMPO}
\begin{equation}
\mathcal{A}=\sum_{S_1..S_{N_1}}\mathcal{A}^{S_1..S_{N_1}}\bigotimes_{j=1}^{N_1}\Lket{S_j}.
\end{equation}
With the current construction this element is in an exact MPS describing a 1-D chain of $N_1$ reduced systems. 

Physically one considers each time step of the system evolution as a separate reduced system which is entangled with the others due to the evolution. So in this picture there is correlations and entanglement between different time steps. To translate between this picture and the physical density matrix one just have to do the sum over all the different $S_i$'s of the coefficient tensor $\mathcal{A}^{S_1..S_{N_1}}$. \par 

In practice $\mathcal{A}$ is build by applying $\Lambda$ to the previous state. This leads to a triangular array as shown in figure \ref{fig:A_evolution}. To get the MPS form of $\mathcal{A}$ one contracts the internal vertical indices. At this point the representation is completely exact which means that even minuscule correlations are kept. To make the representation efficient a SVD with truncation is performed. This means that after each time step the internal indices are contracted to get the MPS form. The truncated bond dimension is then found individually for each bond (red lines in figure \ref{fig:A_evolution}) and truncated to a chosen precision. This means the representation of the state is not exact, so it is necessary to choose a converged precision value which is easiest done by trial and error (starting low and working up). For this reason the precision is kept constant throughout the entire evolution. 
\subsubsection{Finite memory}
The presented computation can be made a lot more efficient if the environment does not have an infinitely long memory. An environment with a finite memory is easily recognised by having an autocorrelation which decays to a negligible value after some finite time ($\tau_c$). This means that $\eta_\gamma=0\rightarrow I_\gamma=1$ when $\Delta\cdot\gamma >\tau_c$. These factors therefore do not affect the time evolution. One can then perform the $S_i$ sums for the Hilbert spaces that are further away than $\tau_c/\Delta=\epsilon$ from the present state. Graphically this means that once the chain has reached a length of $\epsilon$ it will not be any longer as each time step adds a link to the right but also removes a link to the left. 
%       
%This product over arrays can be turned into matrix multiplication by defining a new element
%\begin{equation}
%\begin{gathered}
%(\lambda_j)_{S_j,..,S_1}(\lambda_{j-1})_{S_{j-1},..,S_1}=(\Lambda_j)^{S'_{j-1},..,S'_{1}}_{S_j,S_{j-1},..,S_1}(\lambda_{j-1})_{S'_{j-1},..,S'_1},\\ (\Lambda_j)^{S'_{j-1},..,S'_{1}}_{S_j,S_{j-1},..,S_1}=\left(\prod_{\gamma=1}^{j-1}\delta^{S'_{j-\gamma}}_{S_{j-\gamma}}\right)(\lambda_j)_{S_j,..,S_1}=\left(\prod_{\gamma=1}^{j-1}\delta^{S'_{j-\gamma}}_{S_{j-\gamma}}I_\gamma(S_j,S_{j-\lambda})\right),
%\end{gathered}
%\end{equation} 
%where the Einstein summation convention have been used. This notation can also be used on $I_0$ such that 
%\begin{equation}
%(\Lambda_1)^S_{S_1}=\delta^S_{S_1}I_0(S_1,S_1).
%\end{equation} 
%This is a square matrix where each index take $D$ values. Evolving the system one time step further then corresponds to multiplying with $(\Lambda_2)^{S'_1}_{S_2,S_1}$ which is a rectangular matrix with an lower index taking $D^2$ values. The overlap is therefore given as
%\begin{equation}
%\Lbraket{S_N}{\rho(T)}=\sum_{S_{1},..,S_{N_1-1}}\Lambda_{N_1}...\Lambda_{1}\Lbraket{S_1}{\rho(\Delta)}=\sum_{S_1,...,S_{N-1}}\mathcal{A}_{S_N,..,S_1}\Lket{S_1}\otimes\Lket{S_2}\otimes...\otimes\Lket{S_N}.
%\end{equation}   
%where each new $\Lambda$ tensor increases the dimension of the system. This means that we can think of the all the matrix products as a large tensor ($\mathcal{A}$) acting a 1-D chain of N reduced systems. Physically we are now considering each timestep of the system evolution as a separate reduced system which is entangled with the others due to the evolution. So in this picture we have correlations and entanglement between different time steps. To translate between this picture and the physical density matrix one just have to do the sum over all the different $S_i$'s. \par 
%$\mathcal{A}$ is named the augmented density tensor \cite{TEMPO} and it is this element we want to construct in an efficient manner. In theory one could perform the SVD and construct the MPS but as this requires working with the full tensor $\mathcal{A}$ this is rarely feasible and certainly not efficient. Instead we will consider describe $\mathcal{A}$ using MPO's. 
%\subsubsection{MPO description}
%The approach we take is to write the $\Lambda$ matrices as MPO's. To do this we need to construct a structure for each $\Lambda$ as shown in figure \ref{fig:MPSMPO}.b. This can be done in a similar manner as how the $\Lambda$ were turned into matrices. We use more Kronecker-Deltas! To make it transparent, consider the specific evolution element $\Lambda_4$
%\begin{equation}
%(\Lambda_4)_{S_4,S_3,S_2,S_1}^{S_3,S_2,S_1}=\delta_{S_3}^{S'_3}I_{1}(S_4,S_3)\delta_{S_2}^{S'_2}I_{2}(S_4,S_2)\delta_{S_1}^{S'_1}I_{1}(S_4,S_1).
%\end{equation}
%At this point we have a construction like figure 