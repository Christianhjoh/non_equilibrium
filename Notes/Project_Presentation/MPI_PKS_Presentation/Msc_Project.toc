\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Non-Equilibrium and Non-Markovianity}{5}{0}{1}
\beamer@sectionintoc {2}{Analytical approach: non-equilibrium field theory}{8}{0}{2}
\beamer@sectionintoc {3}{Numerical method}{20}{0}{3}
\beamer@sectionintoc {4}{Preliminary results}{35}{0}{4}
\beamer@sectionintoc {5}{Analytical results}{37}{0}{5}
\beamer@sectionintoc {6}{Numerical results}{43}{0}{6}
\beamer@sectionintoc {7}{Summary and outlook}{46}{0}{7}
