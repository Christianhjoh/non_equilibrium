\section{The Free Boson}
The physical system considered is a single bosonic harmonic oscillator. This is one of the simplest cases one can consider but, for the present formalism, stills highlights many non-trivial features of the formalism. The Hamiltonian is given by (ignoring the vacuum energy and having set $\hbar=1$)  
\begin{equation}\label{eq:H_free}
H=\omega_0 b^\dagger b.
\end{equation}
Had one instead been considering free fermions the results would have been very similar except for sign changes at various places \cite{Altland}, due to the anti-commutator relations for fermionic creation and annihilation operators.
At the time $t_0$ the system is defined to be in a specific state which can contain arbitrary correlations and super-positions
\begin{equation}\label{eq:Initial_state}
\rho_0=\sum_{n,m}\rho_{nm}\ket{n}\bra{m},
\end{equation}
where the initial state will usually be normalised such that $\Tr \rho_0 = \sum_n \rho_{nn}=1$. For the special case of a thermal state at inverse temperature $\beta$ the density matrix is given by
\begin{equation}
\rho_{\beta}=\frac{e^{-\beta H}}{\Tr e^{-\beta H}}=e^{-\beta \omega_0 b^\dagger b}(1-\kappa),
\end{equation} 
where $\kappa=e^{-\beta\omega_0}$. To find the density matrix at a later time it is necessary to evolve the density matrix both forward and backwards in time.\par 
This is easily seen considering the dynamical equation for the density matrix (the Von Neumann equation)\footnote{throughout $\hbar=1$.}
\begin{equation}\label{eq:VonNeumann}
\dot{\rho}(t)=-i\left[H(t),\rho(t)\right],
\end{equation} 
which is formally solved by the unitary evolution operator $\rho (t) = U_{t,t_0}\rho_0 \left(U_{t,t_0}\right)^\dagger=U_{t,t_0}\rho_0 U_{t_0,t}$.  
The idea of the Keldysh field theory formalism is to create an element which gives access to expectation values of operators at any time. In regular equilibrium theory this is the element known as the partition function and it is the trace over the thermal state. As this is clearly not valid out of equilibrium a slightly more complicated element is considered instead.  
The element is the initial state evolved around a closed time contour $\mathcal{C}$ as seen on figure \ref{fig:time_contour}\todo{change point name in figure from $t_i$ to $p_i$ to avoid confusion} which means that no restrictions on the final state have been made. All statistics are then described by the evolved initial state which makes the results valid independently of whether the system is in equilibrium or not.
\begin{figure} 
	\centering
	\includegraphics[width=0.6\linewidth]{TimeContour.png}
	\caption{\textit{The closed time contour $\mathcal{C}$ used to evolve the density matrix in the Keldysh formalism. The small black dots reflects that time has been discretised into 2N-1 pieces.}}
	\label{fig:time_contour}
\end{figure} 
This element is what plays the role of the equilibrium partition function
\begin{equation}
\mathcal{Z} = \frac{\Tr U_\mathcal{C} \rho_0}{\Tr \rho_0},
\end{equation}
where $U_\mathcal{C}$ is the evolution operator ordered along $\mathcal{C}$. For the free time independent Hamiltonian considered here \eqref{eq:H_free} the evolution operator is easily found to be $U_{t}=e^{-i\omega_0 b^\dagger b t}$.
One important departure from equilibrium results is that if the evolution is the same along both the positive and negative leg of $\mathcal{C}$ then $\mathcal{Z}=1$.
This, together with comparing to the known thermal state result \cite{Kamenev}, will be the main check to ensure the validity of the representation. While considering the evolution along this contour one also changes the Hamiltonian of the system \cite{Altland}. Initially the Hamiltonian is assumed to be free and then any interactions is adiabatically turned on. For the free theory this has no effect as no interactions are present. An in-depth discussion is therefore postponed.\par
To create this partition function a number of tricks will be introduced. The final goal is to arrive at a continuum form of the action for the theory. The first technical trick is a Trotter expansion of the evolution operators
\begin{equation}
U(T)=\left[U(\delta)\right]^{N-1}.
\end{equation}
For the free field there is only one operator in the Hamiltonian which commutes with itself for all times. The Trotter expansion is therefore exact. Had the Hamiltonian contained non-commuting terms then the Trotter expansion would give rise to an error of $\mathcal{O}(\delta^2)$. As $T=\delta (N-1)$ this error will vanish in the limit where infinitely small time steps are used. 
In a continuum theory the limit $N\to\infty$ is used which means that the above error vanishes. This is one reason why a continuum theory is very important for interacting theories.  
Splitting $\mathcal{C}$ into $2N-1$ parts as visualised on figure \ref{fig:time_contour} and defining the elements $p_1=p_{2N}=t_0$ and $t_N=t_{N+1}=-\infty$. 
Had the theory been interacting the Trotter expansion amounts to obtaining the freedom to treat the different exponentials as commuting. Exchanging them only introduces errors of order $\delta^2$. \par The next technical trick is to exchange the operators in the exponential with complex numbers. To do this the first guess is to use eigenstates of the operators. This only works as long as all operators commute and therefore have shared eigenstates. Another alternative which is much more general is to use unnormalised coherent states. Let the unormalised coherent states be defined as right eigenstates of the annihilation operator 
\begin{equation}\label{eq:coherent_states}
b\ket{\phi}=\phi \ket{\phi},\quad\rightarrow \quad \ket{\phi}=\exp\left(\phi b^\dagger\right)\ket{0}.
\end{equation}    
These states are not orthogonal but constitutes an over-complete set of basis states. Their overlap is 
\begin{equation}\label{eq:coherent_overlap}
\bra{\theta}\ket{\phi}=\exp(\bar{\theta}\phi),
\end{equation}
where the bar signifies conjugation of the complex number $\theta$. This means that the identity resolution in this basis is 
\begin{equation}\label{eq:coherent_identity}
\mathds{1}=\int d[\bar{\phi},\phi] e^{-\bar{\phi}\phi}\ket{\phi}\bra{\phi},
\end{equation}
where the integration measure is $d[\bar{\phi},\phi]=d\,\text{Re}(\phi)d\,\text{Im}(\phi)/\pi$.
The main purpose of introducing the coherent states, even though they are not orthogonal, is that they are right eigenstates of the annihilation operator and therefore also left eigenstates of the creation operator. Any overlap with normal-ordered operator then equals a function of complex values which is much easier to manipulate than non-commuting operators.\par
At each point in time (the $2N$ black points in figure \ref{fig:time_contour}) an identity in the unormalised coherent state representation is inserted. The partition function then takes the form
\begin{equation}
\begin{aligned}
\mathcal{Z}=\Tr \int\left( \prod_{j=1}^N d[\bar{\phi}^+_j,\phi^+_j]d[\bar{\phi}^-_j,\phi^-_j]e^{-\lvert\phi^+_j\rvert^2-\lvert\phi^-_j\rvert^2}\right)&\ket{\phi^-_1}\bra{\phi^-_1}U_{-\delta}\ket{\phi^-_{2}}
...\bra{\phi^-_{N-1}}U_{-\delta}\ket{\phi^-_N}\bra{\phi^-_N}\mathds{1}\ket{\phi^+_N}\\
&\times\bra{\phi^+_N}U_{+\delta}\ket{\phi^+_{N-1}}...\ket{\phi^+_1}\bra{\phi^+_1}\rho_0.
\end{aligned}
\end{equation}
Here the plus and minus index indicates which leg ($t\rightarrow +\infty$ or $t\rightarrow t_0$) of $\mathcal{C}$ the identity has been inserted on. Similar the index on the evolution operator gives the time argument in the evolution where $\delta=t_{n+1}-t_n$ is the length of a time interval. Reading from right, $rho_0$ is first evolved N times from $t_0$ to $t=+\infty$. From $t_N$ to $t_{N+1}$ there is no time evolution as the states are identical as seen on figure \ref{fig:time_contour}. This is important as this leads to correlations between the two fields $\phi^+$ and $\phi^-$. The system is then evolved $N$ times backwards all the way to $t=t_0$.   
The trace can easily be written in the coherent states basis using the identity resolution
\begin{equation}
\begin{aligned}
\Tr \mathcal{O}&=\sum_n \bra{n}\mathcal{O}\ket{n}=\sum_n\bra{n}\int d[\bar{\phi},\phi]e^{-\bar{\phi}\phi}\ket{\phi}\bra{\phi}\mathcal{O}\ket{n}\\
&=\int d[\bar{\phi},\phi]e^{-\bar{\phi}\phi}\sum_n\bra{\phi}\mathcal{O}\ket{n}\bra{n}\ket{\phi}=\int d[\bar{\phi},\phi]e^{-\bar{\phi}\phi}\bra{\phi}\mathcal{O}\ket{\phi}.
\end{aligned}
\end{equation}
The partition function is then seen to be on the form
\begin{equation}
\begin{aligned}
\Tr \int d[\bar{\phi}^-,\phi^-] e^{-\bar{\phi}^-_1\phi^-_1} \ket{\phi^-_1}\bra{\phi^-_1}\chi&=\int d[\bar{\theta},\theta] e^{-\bar{\theta}\theta}d[\bar{\phi}^-_1,\phi^-_1] e^{-\bar{\phi}^-_1\phi^-_1} \bra{\theta}\ket{\phi^-_1}\bra{\phi^-_1}\chi\ket{\theta}\\
&=\int d[\bar{\phi}^-_1,\phi^-_1] e^{-\bar{\phi}^-_1\phi^-_1} \bra{\phi^-_1}\chi\int d[\bar{\theta},\theta] e^{-\bar{\theta}\theta}\ket{\theta}\bra{\theta}\ket{\phi^-_1}\\
&=\int d[\bar{\phi}^-_1,\phi^-_1] e^{-\bar{\phi}^-_1\phi^-_1} \bra{\phi^-_1}\chi\ket{\phi^-_1}.
\end{aligned}
\end{equation}
From this simple computation it is obvious that resolving the trace in the bosonic partition function results in 
\begin{equation}\label{eq:Z_step1}
\begin{aligned}
\mathcal{Z}=\int \left(\prod_{j=1}^N d[\bar{\phi}^+_j,\phi^+_j]d[\bar{\phi}^-_j,\phi^-_j]e^{-\lvert\phi^+_j\rvert^2-\lvert\phi^-_j\rvert^2}\right)&\bra{\phi^-_1}U_{-\delta}\ket{\phi^-_{2}}
...\bra{\phi^-_{N-1}}U_{-\delta}\ket{\phi^-_N}\bra{\phi^-_N}\mathds{1}\ket{\phi^+_N}\\
&\times\bra{\phi^+_N}U_{+\delta}\ket{\phi^+_{N-1}}...\bra{\phi^+_1}\rho_0\ket{\phi^-_1}.
\end{aligned}
\end{equation}
One observes that, just like correlations arose between $\phi^-_N$ and $\phi^+_N$ due to the shift in direction at $t=+\infty$, the initial state gives rise to correlations between $\phi^+_1$ and $\phi^-_1$. 
Throughout literature this initial correlation seems to be treated by choosing one of two specific initial states. The most common is to consider times that are very far from $t=t_0$. This is done by letting $t_0=-\infty$. It is then acceptable to assume the initial state to be a equilibrium state of the non-interacting system (\cite{Haehl} (see p. 8), \cite{Chakraborty}, \cite{Altland}, \cite{Kamenev}). This means that the expectation value is simple $\bra{\phi}\rho_{thermal}\ket{\theta}=e^{\bar{\phi}\theta \kappa}$. The consequence is that questions considering the system to be prepared in some specific initial state is not accessible. One way of dealing with these question with $t_0=-\infty$ would be to first evolve the system from its thermal state to the wanted state and then afterwards change the evolution to the Hamilitonian of interest. This procedure would be very faithful to the actual physical procedure. The procedure seems not to be tractable as one then needs to derive the microscopic evolution which gives rise to the wanted initial state. In reality this evolution could be non-unitary as it likely is result of interactions with more environments. Besides this difficulty there is also question of how to change Hamiltonian during the contour in a way that keeps the formalism tractable. \par
The other approach taken (\cite{Siebierer} p. 14) is to only consider stationary states and argue that in such cases there should be a complete loss of information about the initial state so the expectation value should just be unity (vacuum initial state). This approach seems to be even more restrictive than the equilibrium assumption.\par
In this note a continuum partition function is derived for arbitrary initial states. There is however one important note concerning using the result for interacting theories. In the thermal initial state theory the free theory gives the basis for the interacting theory and all initial state information, is contained in the free propagators. The difficulty arises because of how one turns on the interactions. If one considers $t_0=-\infty$ one can adiabatically turn on the interaction which is not possible when $t_0$ is finite. Turning on the interaction adiabatically is also not necessary the most physical procedure. If on turns on the interaction immediately at $t_0$, then one can not incorporate the initial state information into the free propagators. This is not possible because the Hamiltonian would then be of a non quadratic form which does not allow for exact incorporation into the free theory. Properly understanding this and finding a procedure which is physically appropriate is essential. It is not considered in depth during this note because it is believed to be an independent problem. Independent the sense that it will not affect the derived results form but instead determine the validity of the foundation of the results.     