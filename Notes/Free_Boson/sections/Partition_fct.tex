\section{Physicality of the Partition Function}
As mentioned initially one of the main properties of the partition function is its unit normalisation. To discuss this feature consider the matrix $iG^{-1}$. Its form is found straight forwardly by using the results \eqref{eq:Z_step1} and \eqref{eq:Z_step2}
\begin{equation}\label{mat:Ginv}
iG^{-1}=\left(
\begin{array}{ccccc|ccccc}
-1 & & & & & & & & &	\\
h_+& -1& & & & & & & &	\\
& h_+ & -1 & & & & & & & \\
& & \cdot &\cdot & & & & & & \\
& & & \cdot &\cdot & & & & & \\
\hline
& & & & &-1& h_-& & & \\
& & & & & &-1&h_- & &\\
& & & & & & & -1 & \cdot&\\
& & & & & & &  &\cdot &\cdot\\
& & & & 1& & & & &\cdot \\
\end{array}
\right)=\left(\begin{array}{c|c}
-A & B \\ \hline
C & -D\\
\end{array}\right).
\end{equation}       
This matrix is partitioned into four blocks which corresponds to which leg of the contour the field resides on. $A$ contains the correlations between fields on the forward leg while $B$ only describes correlations between fields on the backwards leg. $C$ has only one non-zero element which is due to the equivalence of the system when one jumps from the forward leg to the backwards leg \ref{fig:time_contour}. 
Dealing with an arbitrary initial state as describes in the previous section means that all elements of $B$ vanish. Considering instead a thermal state, as initial state, one can include this in the $B$ block by having one non-zero element with the value $\kappa$. \par
Investigating the structure of the partition function it is recognised that it has the form of a multidimensional complex Gaussian integral. These integrals can be solved exactly. The prototypical integral is \cite{Kamenev}
\begin{equation}\label{eq:GaussianInt}
Z[\bar{J},J]=\int \prod_{j=1}^N d[\bar{z}_j,z_j] e^{-\sum_{i,j}^N \bar{z}_jM_{ji}z_i+\sum_{j}^N(\bar{z}_j J_j+\bar{J}_j z_j)} =e^{\sum_{i,j}\bar{J}_i (M^{-1})_{ij}J_j}/\det(M).
\end{equation}
\subsection{Proof of Gaussian Integral}
As it has been found that the details surrounding the proof of Gaussian integral can be very important in interacting scenarios, the derivation is included here. For the presented partition function calculation the details of the proof can however be skipped without serious consequences. \par To solve this integral one starts out with an Hermitian matrix because they have the property of being diagonalised by an unitary transformation \cite{Strang}
\begin{equation}
M = U^\dagger \Lambda U, \text{ with } \Lambda=\text{diag}\{\lambda_i\}.
\end{equation}
As this transformation is unitary it means that its determinant is unity ($\pm1$), so making a change of variables to
\begin{equation}
x_j= \sum_i U_{ji}z_i,\quad \bar{x}_j=\sum_i \bar{z}_i, U_{ij}^\dagger
\end{equation}   
does not affect the measure of the integral as the Jacobian is the determinant of $U$. This change of variable leads to 
\begin{equation}\label{eq:gauss1}
Z[\bar{J},J]=\int \prod_{j=1}^N d[\bar{x}_j,x_j] e^{-\sum_{j}^N \bar{x}_j \lambda_{j}x_j+\sum_{j,i}^N(\bar{x}_j U_{ji}J_i+\bar{J}_i U_{ij}^\dagger x_j)}.
\end{equation}
This is  just a product of 1-D Gaussian integrals with the form
\begin{equation}
\int_{-\infty}^{\infty} \frac{d\text{Re}(x)d\text{Im}(x)}{\pi} e^{-\lambda \text{Re}(x)^2-\lambda \text{Im}(x)^2+(\alpha+\beta)\text{Re}(x)+i(\beta-\alpha)\text{Im}(x)},
\end{equation}
where $\alpha=U_{ji} J_i$ and $\beta=\bar{J}_i U^\dagger_{ij}$. The integral
\begin{equation}
I = \int_{-\infty}^{\infty} d\tilde{x} e^{-a\tilde{x}^2+b\tilde{x}},
\end{equation} 
is solved by first taking advantage of the translational invariance (due to the boundaries) by employing a shift $\tilde{x}=x+b/2a\to \tilde{x}^2=x^2 + b^2/4a^2 +xb/a$. As the shift also has unit Jacobian the integral is 
\begin{equation}
I=\int_{-\infty}^{\infty} dx \exp\left(-ax^2 -\frac{b^2}{4 a}-x b+xb+\frac{b^2}{2a}\right)=e^{b^2/4a}\int_{-\infty}^{\infty}dxe^{-ax^2}=e^{b^2/4a}\sqrt{\frac{\pi}{a}}\quad \forall a>0.
\end{equation} 
The last integral is just the simplest Gaussian integral which can be solved straight forwardly by squaring and changing to polar coordinates. Assuming $\lambda_i>0$ the integral is convergent. Using this result in \eqref{eq:gauss1} the proof is completed for the wanted integral
\begin{equation}
Z[\bar{J},J]=\prod_i\exp\left(\bar{J}_i U^\dagger_{ij}\lambda^{-1}_jU_{j,i}J_i\right)\frac{1}{\lambda_i}=\exp\left(\sum_{i,j}\bar{J}_i (M^{-1})_{ij} J_j\right)\frac{1}{\det(M)}.
\end{equation} 
There is however still one important detail. The proof has only been done for Hermitian matrices but inspecting the result it is established that it has no poles in complex plane, as it is a function of the real and imaginary part of $M^{-1}$. AN analytic continuation to the entire complex plane is then allowed and one then reaches any complex matrix which has eigenvalues with a positive definite real part \cite{Kamenev}.    
\subsection{Normalisation of Partition Function}
At this point it is possible to test that the partition function satisfies unity normalisation property. To this extent it is necessary to compute the full field integral appearing in the partition function. Defining the $2N$ long vectors
\begin{equation}
\begin{aligned}
\Phi&=\left(
\begin{array}{cccccccccc}	
\phi_1^+ & \phi_2^+&\cdot&\cdot&\phi_N^+&\phi_1^-&\phi_2^-&\cdot&\cdot&\phi_N^-\\
\end{array}\right)^T,\\
\textbf{K}& =\big(\underbrace{\begin{array}{ccccc}	
\kappa & 0&\cdot&\cdot&0\\
\end{array}}_{2N}\big)^T,
\\
\textbf{V}& =\left(\begin{array}{ccccc}	
	\underbrace{0\quad\cdot\quad\cdot\quad 0}_{N}\quad \nu\quad \underbrace{0\quad \cdot\quad \cdot\quad 0}_{N-1}\\
	\end{array}\right)^T,
\end{aligned}
\end{equation}    
the multidimensional Gaussian integral can be used as partition function \eqref{eq:partitionfct1} then takes the form
\begin{equation}
\mathcal{Z}=\lim\limits_{\kappa,\nu\to 0}\sum_{n,m}\rho_{nm}\frac{1}{\sqrt{n!m!}}\frac{\partial^n}{\partial \kappa^n}\frac{\partial^m}{\partial \bar{\nu}^m}\int d[\bar{\Phi},\Phi]\exp\left(i \bar{\Phi}^T G^{-1}\Phi+\bar{\Phi}^T\textbf{K}+\bar{\textbf{V}}^T\Phi
\right).
\end{equation}
Comparing to the Gauss integral \eqref{eq:GaussianInt} the components are identified as $M=-iG^{-1}$, $J = \textbf{K}$ and $\bar{J}=\bar{\textbf{V}}$.
The necessary computations is to calculate the determinant and inverse of $iG^{-1}$.\par The determinant can be computed using the general relation
\begin{equation}
\det(A)=\exp(\Tr \ln(A)).
\end{equation} 
Taylor expanding the logarithm around $-iG^{-1}-\mathds{1}$
\begin{equation}
\begin{aligned}
\det(-iG^{-1})&=\det(-iG^{-1}-\mathds{1}+\mathds{1})=\exp\left[\Tr \ln(-iG^{-1}-\mathds{1}+\mathds{1})\right]\\
&=\exp\left[\Tr \sum_{n=1}^{\infty}\frac{(-1)^{n+1}}{n}\left(-iG^{-1}-\mathds{1}\right)^n\right].
\end{aligned}
\end{equation}
Doing the matrix powers using the form of $-iG^{-1}-\mathds{1}$ from \eqref{mat:Ginv} one finds that the diagonal always remains zero and for $n=N$ all elements vanish. The infinite sum therefore truncates at $n=N$. This clearly shows that the trace is zero which gives rise to the determinant
\begin{equation}
\det(-iG^{-1})=\exp(0)=1.
\end{equation} 

%In our case we recognise that $-iG^{-1}$ \eqref{mat:Ginv} is a lower triangular matrix with ones in the diagonal. This means that all the eigenvalues are +1 and so is the determinant \cite{Strang}.
Having found the determinant the next important element is the inverse. This calculation is simplified significantly by using that $M$ is a $2\times 2$ block matrix. Because $A$ and $D$ \eqref{mat:Ginv} are invertible the inverse of $-iG^{-1}$ is given by \cite{BlockMatInv}
\begin{equation}
\begin{aligned}
(-iG^{-1})^{-1}=i G&=\left(\begin{array}{c|c}
A & -B \\ \hline
-C & D\\
\end{array}\right)^{-1},\\
&=\left(\begin{array}{c|c}
(A-B D^{-1} C)^{-1} & (A-BD^{-1}C)^{-1}BD^{-1} \\ \hline
D^{-1}C(A-BD^{-1}C)^{-1} & D^{-1}+D^{-1}C(A-BD^{-1}C)^{-1}BD^{-1}\\
\end{array}\right),\\
&=\left(\begin{array}{c|c}
A^{-1} & 0 \\ \hline
D^{-1}CA^{-1} & D^{-1}\\
\end{array}\right),
\end{aligned}
\end{equation} 
where it has been used that $B=0$. To compute $iG$ one needs the inverse' of $A$ and $D$. These can be found by exploiting that they are both triangular matrices with ones in the diagonal (denote such a matrix $L_T$). Such matrices can be written as $L_T=\mathds{1}+T$, where $T$ is a strictly triangular (zero in diagonal). If the system is $N$ dimensional then $T^N=0$.
For any matrix it further holds
\begin{equation}
\left(\mathds{1}-x^N\right)=(\mathds{1}-x)\left(\mathds{1}+x+x^2+x^3+..+x^{N-1}\right).
\end{equation}  
Letting $x=-T$ a simple formula for the inverse' is found  
\begin{equation}
\begin{gathered}
\mathds{1}=(\mathds{1}+T)\left(\mathds{1}+\sum_{i=1}^{N-1}(-1)^i T^i\right)\\
\Rightarrow (\mathds{1}+T)^{-1}=L_T^{-1}=\mathds{1}+\sum_{i=1}^{N-1}(-1)^i T^i.
\end{gathered}
\end{equation}
%Using that $A=L_{T+}$ and $D=L_{T-}$ where the $+(-)$ refers to the value of the off-diagonal element $h_+(h_-)$, one finds 
%\begin{equation}
%\left(L_{T\pm}^{-1}\right)_{ij}=\theta(i-j)h_{\pm}^{i-j},\quad \theta(n)=\begin{cases}
%1 & n\geq 0\\
%0 & \text{otherwise}
%\end{cases}.
%\end{equation}
$iG$ is now easily computed and the 6 dimensional case takes the form
\begin{equation}\label{mat:G_vacuum}
iG=\left(
\begin{array}{ccc|cccc}
1 & & & & & 	\\
h_+& 1& & & & \\
h_+^2& h_+ & 1 &  & &\\
\hline
 h_+^2 h_-^2& h_+h_-^2&h_-^2 &1&h_- &h_-^2 \\
 h_+^2 h_-& h_+h_-& h_-& &1&h_- \\
 h_+^2 & h_+& 1& &  & 1 \\
\end{array}
\right),
\end{equation}
which is straightforwardly extended to dimension 2N. With the inverse and determinant computed the partition function takes the form
\begin{equation}
\begin{aligned}
\mathcal{Z}&=\lim\limits_{\kappa,\nu\to 0}\sum_{n,m}\rho_{nm}\frac{1}{\sqrt{n!m!}}\frac{\partial^n}{\partial \kappa^n}\frac{\partial^m}{\partial \bar{\nu}^m}\int d[\bar{\Phi},\Phi]\exp\left(i \bar{\Phi}^T G^{-1}\Phi+\bar{\Phi}^T\textbf{K}+\bar{\textbf{V}}^T\Phi
\right),\\
&=\lim\limits_{\kappa,\nu\to 0}\sum_{n,m}\rho_{nm}\frac{1}{\sqrt{n!m!}}\frac{\partial^n}{\partial \kappa^n}\frac{\partial^m}{\partial \bar{\nu}^m}\frac{1}{\det(-iG)}\exp\left(\bar{\textbf{V}}^TiG\textbf{K}\right),\\
&=\lim\limits_{\kappa,\nu\to 0}\sum_{n,m}\rho_{nm}\frac{1}{\sqrt{n!m!}}\frac{\partial^n}{\partial \kappa^n}\frac{\partial^m}{\partial \bar{\nu}^m}\exp\left(i\bar{\nu}\,G_{(1-,1+)}\,\kappa\right),\\
&=\lim\limits_{\kappa,\nu\to 0}\sum_{n,m}\rho_{nm}\frac{1}{\sqrt{n!m!}}\frac{\partial^n}{\partial \kappa^n}\frac{\partial^m}{\partial \bar{\nu}^m}\exp\left(\bar{\nu}\,(h_+h_-)^N\,\kappa\right).
\end{aligned}
\end{equation}
At some point during the calculation the limit $\nu,\, \kappa\to0$ has to be done which will remove all terms with $n\neq m$. The non-vanishing terms are
\begin{equation}
\begin{aligned}
\mathcal{Z}
&=\lim\limits_{\kappa,\nu\to 0}\sum_{n}\rho_{nn}\frac{1}{n!}\frac{\partial^n}{\partial \kappa^n}\frac{\partial^n}{\partial \bar{\nu}^n}\exp\left(\bar{\nu}\,(h_+h_-)^N\,\kappa\right),\\
&=\lim\limits_{\kappa,\nu\to 0}\sum_{n}\rho_{nn}\frac{1}{n!}\frac{\partial^n}{\partial \kappa^n}\left((h_+h_-)^N\kappa\right)^n\exp\left(i\bar{\nu}\,(h_+h_-)^N\,\kappa\right),\\
&=\lim\limits_{\kappa,\nu\to 0}\sum_{n}\rho_{nn}\left((h_+h_-)^N\right)^n\exp\left(i\bar{\nu}\,(h_+h_-)^N\,\kappa\right),\\
&=\sum_{n}\rho_{nn}\left((h_+h_-)^N\right)^n.\\
\end{aligned}
\end{equation} 
Taking advantage of the specific form of $h_\pm$
\begin{equation}
[h_+h_-]^N=\left[(1-i\delta \omega_0)(1+i\delta \omega_0)\right]^N=\left[1+\delta^2\omega_0^2\right]^N\overset{N\to\infty}{=}e^{N\delta^2\omega_0^2}.
\end{equation} 
At first glance this looks divergent but recall that when the time-slicing was introduced, $\delta$ and $N$ was defined such that $\delta N$ would give the full extent of the time axis. This means that $\delta\sim N^{-1}$ such that 
\begin{equation}
[h_+h_-]^N\overset{N\to\infty}{=}e^{\omega_0^2/N}=1.
\end{equation}  
This gives the result $\mathcal{Z}=\sum_{n}\rho_{nn}=1$. This proves that the present construction has the expected properties.\par Had a initial state in thermal equilibrium been considered instead, the determinant and inverse of $M$ would have been different (having absorbed the initial state into the $B$ block) but would also result in $\textbf{K}$ and $\textbf{V}$ being zero vectors. Using the exactly the  same methods to compute the determinant and inverse one finds that \cite{Kamenev}, in the continuum limit, the determinant exactly cancels the thermal normalization factor. This again means that the partition function is normalised to unity as demanded.