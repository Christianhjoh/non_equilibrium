\section{Propagators of the Theory}
At this point an element which have been denoted the partition function have been established. It evolves the system from any initial state at $t_0$ forward to infinity and all the way back. The goal is now to use this element to compute propagators for the theory. To this extent it actually seems more transparent to do the derivatives related to the initial state such that the partition function is on the form \eqref{eq:partitionfct2}
\begin{equation}
\mathcal{Z}=\sum_{n,m}\rho_{nm}\frac{1}{\sqrt{n!m!}}\int \prod_{j=1}^N d[\bar{\phi}_j^+,\phi_j^+]d[\bar{\phi}_j^-,\phi_j^-]\left(\bar{\phi}_1^+\right)^n \left(\phi_1^-\right)^m\exp\left(\sum_{\alpha,\beta} \bar{\phi}_\alpha i G^{-1}_{\alpha,\beta}\phi_{\beta}
\right).
\end{equation}   
This is advantageous because it allows for direct application of Wick's theorem. Using functional field theory Wick's theorem is a clear consequence of the Gaussian integrals.\par Consider again the derived Gaussian integral with sources \eqref{eq:GaussianInt}. 
Much like how the number state overlaps was written as derivatives, averages with respect to a Gaussian distribution 
\begin{equation}
\left<z_{\alpha}\bar{z}_{\beta}\right> = \int \prod_{j=1}^N d[\bar{z}_j,z_j] z_{\alpha}\bar{z}_{\beta} e^{-\sum_{i,j}^N \bar{z}_jM_{ji}z_i},
\end{equation}
can be evaluated by taking derivatives with respective to some fictional source
\begin{equation}
\begin{aligned}
\left<z_\alpha \bar{z}_\beta\right>&=\left.\frac{\delta }{\delta \bar{J}_\alpha}\frac{\delta}{\delta J_\beta}\int \prod_{j=1}^N d[\bar{z}_j,z_j] e^{-\sum_{i,j}^N \bar{z}_jM_{ji}z_i+\sum_{j}^N(\bar{z}_j J_j+\bar{J}_j z_j)}\right\vert_{J=\bar{J}=0},\\&=\frac{\delta }{\delta \bar{J}_\alpha}\frac{\delta}{\delta J_\beta}\left.\exp\left(\sum_{i,j}\bar{J}_i (M^{-1})_{ij} J_j\right)\frac{1}{\det(M)}\right\vert_{J=\bar{J}=0},\\
&=\frac{\left(M^{-1}\right)_{\alpha,\beta}}{\det(M)}.
\end{aligned}
\end{equation}
Generalising this result to more general averages leads to products  of all possible permutation of matrix elements
\begin{equation}
\left<z_{\alpha_1}... z_{\alpha_n}\bar{z}_{\beta_1}..\bar{z}_{\beta_n}\right>=\sum_{\mathcal{P}}\left(M^{-1}\right)_{\alpha_1,\mathcal{P}_1}...\left(M^{-1}\right)_{\alpha_n,\mathcal{P}_n},
\end{equation} 
where $\mathcal{P}$ denotes the set of all possible permutations of the set $\{\beta_i\}$.
This allows one to evaluate the propagators of the free theory even with arbitrary initial states
\begin{equation}\label{eq:full_InitState}
\begin{aligned}
i\mathcal{G}_{\alpha,\beta}=\left<\phi_\alpha \bar{\phi}_\beta\right>&=\sum_{n,m} \frac{\rho_{nm}}{\sqrt{n!m!}}\left<\phi_\alpha\left(\phi_1^- \right)^m\bar{\phi}_\beta \left(\bar{\phi}_1^+\right)^n\right>_v,\\
&=\sum_{n,m} \frac{\rho_{nm}}{\sqrt{n!m!}}\int \prod_{j=1}^N d[\bar{\phi}_j^+,\phi_j^+]d[\bar{\phi}_j^-,\phi_j^-]\phi_\alpha\left(\phi_1^- \right)^m\bar{\phi}_\beta \left(\bar{\phi}_1^+\right)^n\exp\left(\sum_{a,b} \bar{\phi}_a i G^{-1}_{a,b}\phi_{b}
\right),
\end{aligned}
\end{equation}
where the expectation value with respect to an initial vacuum state have been denoted by the subscript $v$. Also notice that unless the initial state is vacuum, the true propagator ($\mathcal{G}$) is different from the current matrix in the action ($G$). The first to notice is that, for the free theory, the propagator is insensitive to quantum correlations (off-diagonal elements in $\rho_0$). This is true because an odd number of derivatives will lead to a term depending on the fictional source in a linear fashion. Letting the source vanish then removes these terms. Physically this seems sensible as the propagator is related to the occupation of the field, so only the diagonal elements are relevant. Had one instead considered a higher order correlation function one would see effects of the coherence elements (non-diagonal elements)!\par
The above considerations means that the propagators consists of a sum over diagonal elements weighted by the initial probability ($\rho_{nn}$) and an over-counting correction ($1/n!$). The first term in this sum is the vacuum initial state contribution
\begin{equation}
i\mathcal{G}_{v(\alpha,\beta)}=\rho_{00}\left<\phi_\alpha\bar{\phi}_\beta\right>_v=iG_{\alpha,\beta}.
\end{equation}
Given the structure of the action there are four distinctive groups of propagators. Those that propagate the system on the same leg of $\mathcal{C}$ ($\left<\phi_i^\pm\bar{\phi}_j^\pm\right>$) and those that describe propagation between legs ($\left<\phi_i^\pm\bar{\phi}_j^\mp\right>$). 
With a the vacuum initial state it is clear that one of these cross-coupling propagators must vanish as it is the initial state that couples the legs together in the $t_0$ end of the contour (figure \ref{fig:time_contour}). This feature is also shown directly in the theory as the different vacuum propagators are given by the four different blocks in \eqref{mat:G_vacuum}. By direct inspection of these blocks the propagators are found to be
\begin{equation}
\begin{aligned}
i \mathcal{G}_{v(ij)}^{++}&=\rho_{00}\left<\phi_i^+\bar{\phi}_j^+\right>_v=\rho_{00}iG_{i,j}^{++}=\rho_{00}\Theta (i-j) h_+^{i-j},\\
i \mathcal{G}_{v(ij)}^{--}&=\rho_{00}\left<\phi_i^-\bar{\phi}_j^-\right>_v=\rho_{00}iG_{i,j}^{--}=\rho_{00}\Theta (j-i) h_-^{j-i},\\
i \mathcal{G}_{v(ij)}^{+-}&=\rho_{00}\left<\phi_i^+\bar{\phi}_j^-\right>_v=\rho_{00}iG_{i,j}^{+-}=0,\\
i \mathcal{G}_{v(ij)}^{-+}&=\rho_{00}\left<\phi_i^-\bar{\phi}_j^+\right>_v=iG_{i,j}^{-+}=h_-^{N-i}h_+^{N-j}=(h_-h_+)^N h_-^{-i}h_+^{-j},
\end{aligned}
\end{equation}
here the discrete Heavisides are to be interpreted as unity whenever their argument is greater or equal to zero and they vanish otherwise. 
The path integral approach is only exact in the limit where $N\to\infty$.The relations $\delta N=\text{const},$ $h_\pm=1\mp i\delta \omega_0$ and $i \delta= t$ can then be used to write the propagators in this limit. The first relation gives an inverse relation between $N$ and $\delta$, such that $\delta$ goes to zero in the limit. Then $h_\pm=e^{\mp i\delta \omega_0}$ which directly leads to the continuum form of the vacuum propagators
\begin{equation}
\begin{aligned}
i \mathcal{G}_{v(tt')}^{++}&=\rho_{00}\left<\phi(t)^+\bar{\phi}(t')^+\right>_v=\rho_{00}\Theta (t-t') e^{-i\omega_0(t-t')},\\
i \mathcal{G}_{v(tt')}^{--}&=\rho_{00}\left<\phi(t)^-\bar{\phi}(t')^-\right>_v=\rho_{00}\Theta (t'-t) e^{-i\omega_0(t-t')},\\
i \mathcal{G}_{v(tt')}^{+-}&=\rho_{00}\left<\phi(t)^+\bar{\phi}(t')^-\right>_v=0,\\
i \mathcal{G}_{v(tt')}^{-+}&=\rho_{00}\left<\phi(t)^-\bar{\phi}(t')^+\right>_v= \rho_{00}e^{-i\omega_0(t-t')}.
\end{aligned}
\end{equation}
The propagators are not independent of each other in the sense that $\mathcal{G}^{++}_{v(tt')}+\mathcal{G}^{--}_{v(tt')}=\mathcal{G}^{+-}_{v(tt')}+\mathcal{G}^{-+}_{v(tt')}$. To understand this redundancy properly, it is advantageous to revert back to the discrete case. This is a generally the procedure. Whenever the continuum form is not properly understood one reverts back to the discrete case. The continuum form can be thought of as a representation, that has the purpose of giving the same result as the discrete case, in the limit, and not in itself "physically trustworthy". In the continuum the redundancy takes the form
\begin{equation}
\mathcal{G}^{++}_{v(ij)}+\mathcal{G}^{--}_{v(ij)}-\mathcal{G}^{+-}_{v(ij)}-\mathcal{G}^{-+}_{v(ij)}=\Theta(i-j)(1-i\delta\omega_0)^{i-j}+\Theta(j-i)(1+i\delta\omega_0)^{j-i}-(1+\delta^2\omega_0^2)^N(1-i\delta\omega_0)^{-j}(1+i\delta\omega_0)^{-i}.
\end{equation} 
In the limit where $N\to \infty$ and $\delta\to 0$ there is three different regions depending on the relation between $i$ and $j$
\begin{equation}
\begin{gathered}
\underline{i>j}\\
(1-i\delta \omega_0)^{i-j}-(1+\delta^2\omega_0^2)^N(1-i\delta\omega_0)^{-j}(1+i\delta\omega_0)^{-i}\underset{\delta\to 0}{=}1^{i-j}-1^N 1^{-j}1^{-i}=0,
\\
\underline{i=j}\\
(1-i\delta \omega_0)^{0}+(1+i\delta \omega_0)^{0}-(1+\delta^2\omega_0^2)^N(1-i\delta\omega_0)^{-i}(1+i\delta\omega_0)^{-i}\underset{\delta\to 0}{=}1+1-1^N 1^{-j}1^{-i}=1,\\
\underline{i<j}\\
(1+i\delta \omega_0)^{j-i}-(1+\delta^2\omega_0^2)^N(1-i\delta\omega_0)^{-j}(1+i\delta\omega_0)^{-i}\underset{\delta\to 0}{=}1^{j-i}-1^N 1^{-j}1^{-i}=0,
\end{gathered}
\end{equation}
This shows that in the discrete representation the redundancy takes the form 
\begin{equation}
\mathcal{G}_{v(ij)}^{++}+\mathcal{G}_{v(ij)}^{--}-\mathcal{G}_{v(ij)}^{-+}-\mathcal{G}_{v(ij)}^{+-}=\delta_{ij}.
\end{equation} 
Taking this relation into the continuum form results in the Kronecker delta becoming a delta function with a scaling prefactor \cite{KroneckerDeltaScaling} such that $\delta_{ij}\to(\delta) \delta(t-t')$ which vanishes as $\delta\to 0$. This shows that the real continuum limit the redundancy relation
\begin{equation}\label{eq:RedundancyRelation}
\mathcal{G}^{++}_{v(tt')}+\mathcal{G}^{--}_{v(tt')}-\mathcal{G}^{+-}_{v(tt')}-\mathcal{G}^{-+}_{v(tt')}=0,
\end{equation}
is true for all $t,t'$.   
This redundancy is very important for the further development as it leads to specific properties of the continuum action.\par The main question we now seek to answer is whether having an arbitrary initial state, interferes with these relations between the propagators. To begin answering this question consider the next term in the full initial state propagator \eqref{eq:full_InitState}. This term is the one occupation contribution
\begin{equation}
i\mathcal{G}_{1(\alpha,\beta)}/\rho_{11}=\left<\phi_\alpha\phi_1^-\bar{\phi}_\beta\bar{\phi}_1^+\right>_v=iG_{\alpha,\beta} \,iG_{1,1}^{-+}+iG_{\alpha,1+} \,iG_{1-,\beta}.
\end{equation}
In the continuum limit the relation $iG_{1,1}^{-+}=(h_-h_+)^{N-1}=1$ can be used. The first term is then only the vacuum propagator while the second term is a "correction" due to the 1 boson initial occupation. As the vacuum contribution satisfies the redundancy relation it is only necessary to look at the correction terms. Consider the first of the four different corrections (which is the difference between the 1-occupation and vacuum propagators)
\begin{equation}
(i \mathcal{G}_{1(ij)}^{++}-i\mathcal{G}_{v(ij)}^{++})\rho_{11}=iG_{i,1}^{++} \,iG_{1,j}^{-+}=\Theta(i-1)h_+^{i-1}(h_-h_+)^Nh_-^{-,1}h_+^{-j}.
\end{equation}   
The discrete Heaviside is always unity as $i\geq1$ and the only other new element is $h_\pm^{\pm1}=(1\pm i\delta\omega_0)$. In the continuum limit $\delta\to 0$ so these elements are just unity. This is physically reasonable as they correspond to one evolution step which in the continuum limit is a vanishingly vanishingly small step. The continuum limit of the $++$ correction is then
\begin{equation}\label{eq:+correction}
(i \mathcal{G}_{1(ij)}^{++}-i\mathcal{G}_{v(ij)}^{++})\rho_{11}=iG_{i,1}^{++} \,iG_{1,j}^{-+}=e^{-i\omega_0(t-t')}.
\end{equation}
Similarly for the three other corrections
\begin{equation}\label{eq:other_corrections}
\begin{aligned}
(i \mathcal{G}_{1(ij)}^{--}-\mathcal{G}_{v(ij)}^{--})\rho_{11}&=iG_{i,1}^{-+} \,iG_{1,j}^{--}=(h_-h_+)^N h_-^{-i} h_+^{-1} \Theta (j-1)h_-^{j-1}=e^{-i\omega_0(t-t')},\\
(i \mathcal{G}_{1(ij)}^{+-}-\mathcal{G}_{v(ij)}^{+-})\rho_{11}&=iG_{i,1}^{++} \,iG_{1,j}^{--}=\Theta(i-1)h_+^{i-1}\Theta(j-1)h_-^{j-1}=e^{-i\omega_0(t-t')},\\(i \mathcal{G}_{1(ij)}^{-+}-\mathcal{G}_{v(ij)}^{-+})\rho_{11}&=iG_{i,1}^{-+} \,iG_{1,j}^{-+}=(h_-h_+)^{2N}h_-^{-i}h_+^{-1}h_-^{-1}h_+^{-j}=e^{-i\omega_0(t-t')}.
\end{aligned}
\end{equation}
As all the corrections are identical the redundancy relation is obviously satisfied.
This procedure is simple but at first glance it does not seem tractable for the general proof as the boson density matrix is infinite. Luckily the present case is highly structured which is seen by considering the form of the 2-boson initial state propagator
\begin{equation}
\begin{aligned}
i\mathcal{G}_{2(\alpha,\beta)}/2!\rho_{22}&=\left<\phi_\alpha\phi_1^-\phi_1^-\bar{\phi}_\beta\bar{\phi}_1^+\bar{\phi}_1^+\right>_v=2iG_{\alpha,\beta} \,iG_{1,1}^{-+}\,iG_{1,1}^{-+}+4iG_{\alpha,1+} \,iG_{1-,\beta} \,i G_{1,1}^{-+}\\
&=2iG_{\alpha,\beta}+4iG_{\alpha,1+} \,iG_{1-,\beta},
\end{aligned}
\end{equation} 
Because only two-point correlations are of concern the only thing that will change for different initial occupation is the numerical prefactors due to the combinatorics. As both vacuum and 1-boson terms satisfy the redundancy relation "individually", the structure of the entire propagators proves that bosonic free field the redundancy relation is satisfied for arbitrary initial states! \par 
Going one step further the complete propagators, including the numerical factors, are now derived. The relevant quantities are
\begin{equation}
i\mathcal{G}_{n(\alpha,\beta)}=\frac{\rho_{nn}}{n!}\left<\phi_\alpha \left(\phi_1^-\right)^n\bar{\phi}_\beta\left(\phi_1^+\right)^n\right>_v.
\end{equation}
As the theory is quadratic Wick's theorem is exact so the only necessary computation is to derive the combinators for all the different pairing possibilities. The first term is based on pairing $\phi_\alpha$ and $\bar{\phi}_\beta$. This means that pairing the remaining fields will give rise to equal terms. For the first $\phi_1^-$ one can pair it with $n$ different $\bar{\phi}_1^+$. The next $\phi_1^-$ then only has $n-1$ possibilities and so forth. The final factor for first term is then found to be $n!$. The second term is based on pairing $\phi_\alpha$ with one of $n$ $\phi_1^+$ fields (so gives a factor of $n$). This means that $\bar{\phi}_\beta$ has to be paired with one of the $n$ $\phi_1^-$ fields (again a factor $n$). What is left is $n-1$ of both $\phi_1^-$ and $\bar{\phi}_1^+$. As before these can be paired in $(n-1)!$ different ways. For the second term the numerical factor is therefore $n(n!)$. The common factor of $n!$ in both terms, cancels with the over-counting correction. The complete propagator of the theory is  finally found to be given by
\begin{equation}\label{eq:full_propagator_pm}
\begin{aligned}
i\mathcal{G}_{t,t'}^{\alpha,\beta}&=\sum_n \rho_{nn} (iG^{\alpha,\beta}_{t,t'}+n\; iG^{\alpha,+}_{t,0} iG_{0,t'}^{-,\beta})\\
&= iG^{\alpha,\beta}_{t,t'}+\left(\sum_n n \rho_{nn}\right)iG^{\alpha,+}_{t,0} iG_{0,t'}^{-,\beta}=iG^{\alpha,\beta}_{t,t'}+\left<n\right>_0iG^{\alpha,+}_{t,0} iG_{0,t'}^{-,\beta}
\end{aligned} 
\end{equation} 
where $\left<n\right>_0$ is the average boson number in the initial state. The continuum form of this free theory allows a quiet clear physically interpretation. The complete propagator is given by the propagation from vacuum and a "correction" term which is weighted by the initial state occupation. Physically the correction term is the result of propagating the initial field configuration $\beta$  anti-clockwise around the contour to the end of the backwards leg. Then jumping to the forwards leg at $t_0$ and propagating to the final state. This path is then weighted by the average occupation.  The correction term  describes the amplitude for moving from $\phi_\beta$ to $\phi_\alpha$ by going "through" the initial state in the "open" end of the contour. The result is simplified further because all correction terms was found to be identical for all four propagators in \eqref{eq:+correction} and \eqref{eq:other_corrections}.
The propagators takes the specific form 
\begin{equation}
\begin{aligned}
i \mathcal{G}_{(tt')}^{+-}&=\left<\phi(t)^+\bar{\phi}(t')^-\right>=\left<n\right>_0 e^{-i\omega_0(t-t')},\\
i \mathcal{G}_{(tt')}^{-+}&=\left<\phi(t)^-\bar{\phi}(t')^+\right>=(1+\left<n\right>_0)e^{-i\omega_0(t-t')},\\
i \mathcal{G}_{(tt')}^{++}&=\left<\phi(t)^+\bar{\phi}(t')^+\right>=(\Theta (t-t')+\left<n\right>_0) e^{-i\omega_0(t-t')}=\theta(t-t')i\mathcal{G}^{-+}_{(t,t')}+\theta(t'-t)i\mathcal{G}^{+-}_{(t,t')},\\
i \mathcal{G}_{(tt')}^{--}&=\left<\phi(t)^-\bar{\phi}(t')^-\right>=(\Theta (t'-t)+\left<n\right>_0) e^{-i\omega_0(t-t')}=\theta(t'-t)i\mathcal{G}^{-+}_{(t,t')}+\theta(t-t')i\mathcal{G}^{+-}_{(t,t')}.\\
\end{aligned}
\end{equation}
This result is qualitatively identical to the textbook result derived for a thermal initial state  \cite{Kamenev}. The only difference is the value of initial occupation $\left<n\right>_0$. 
This proves that the same qualitative properties are present for arbitrary initial states as for thermal states\par On top of this nice and simple result it is worthwhile to again mention that for interacting systems physical interpretations becomes a lot less clear. This has to do with the way the interactions are turned on in the system. Mentioned in \cite{Altland} is that the non-free part is adiabatically turned on which justifies using a thermal state for the free field as initial state. 
Intuitively I would expect that a similar argument can be made such that one can use Wicks theorem exactly before the interactions are turned on. In reality one would always have a gradual but finite time which is use to turn on the interaction. This means that the Hamiltonian is going to be time-dependent for this period. This can be handled in field theory but will affect the form of $-iG^{-1}$. Currently the right approach to this problem is not resolved. If one  can justify using Wicks theorem for the free part it means one can still incorporate all initial state information in the free propagator even for interacting systems.    
\subsection{Keldysh Rotation and the Continuum Action}
The propagators in the $\pm$ presentation had an inherently build redundancy relation between the different propagators \eqref{eq:RedundancyRelation}. This points to the fact that using the fields in the $\phi^\pm$ basis is not optimal for a continuum description. Instead the basis of the fields can be changed by a linear transform which has been named the Keldysh rotation \cite{Kamenev}
\begin{equation}\label{eq:Keldysh_rotation}
\begin{pmatrix}
\phi^{cl}\\
\phi^{q}
\end{pmatrix}=\frac{1}{\sqrt{2}}
\begin{pmatrix}
1&1\\
1&-1
\end{pmatrix}\begin{pmatrix}
\phi^+\\
\phi^-
\end{pmatrix}.
\end{equation}
The indices are historical introduced as $cl$ for classical and $q$ for quantum. This transformation is very useful as it moves to a basis, where the redundancy is used to reduce the number of propagators.
The propagators in this basis are given as 
\begin{equation}
\begin{aligned}
i\mathcal{G}^K_{t,t'}=\left<\phi^{cl}\bar{\phi}^{cl}\right>_{t,t'}=i\mathcal{G}_{t,t'}^{cl,cl}&=\frac{1}{2}\left(i \mathcal{G}^{++}_{t,t'}+i \mathcal{G}^{--}_{t,t'}+i \mathcal{G}^{+-}_{t,t'}+i \mathcal{G}^{-+}_{t,t'}\right)=i\mathcal{G}^{+-}_{(tt')}+i\mathcal{G}^{-+}_{(tt')}=(1+2\left<n\right>_0)e^{-i\omega_0(t-t')},\\
i\mathcal{G}^R_{t,t'}=\left<\phi^{cl}\bar{\phi}^{q}\right>_{t,t'}=i\mathcal{G}_{t,t'}^{cl,q}&=\frac{1}{2}\left(i \mathcal{G}^{++}_{t,t'}-i \mathcal{G}^{--}_{t,t'}-i \mathcal{G}^{+-}_{t,t'}+i \mathcal{G}^{-+}_{t,t'}\right)\\&=\theta(t-t')\left(i\mathcal{G}^{-+}_{(tt')}-i\mathcal{G}^{+-}_{(tt')}\right)=\theta(t-t')e^{-i\omega_0(t-t')},\\
i\mathcal{G}^A_{t,t'}=\left<\phi^{q}\bar{\phi}^{cl}\right>_{t,t'}=i\mathcal{G}_{t,t'}^{q,cl}&=\frac{1}{2}\left(i \mathcal{G}^{++}_{t,t'}-i \mathcal{G}^{--}_{t,t'}+i \mathcal{G}^{+-}_{t,t'}-i \mathcal{G}^{-+}_{t,t'}\right)\\&=\theta(t'-t)\left(i\mathcal{G}^{+-}_{(tt')}-i\mathcal{G}^{-+}_{(tt')}\right)=-\theta(t'-t)e^{-i\omega_0(t-t')},\\
\left<\phi^{q}\bar{\phi}^{q}\right>_{t,t'}=i\mathcal{G}_{t,t'}^{q,q}&=\frac{1}{2}\left(i \mathcal{G}^{++}_{t,t'}+i \mathcal{G}^{--}_{t,t'}-i \mathcal{G}^{+-}_{t,t'}-i \mathcal{G}^{-+}_{t,t'}\right)=0.\\
\end{aligned}
\end{equation}
Here the customary notation for Keldysh theory have been used $K$: Keldysh, $A$: Advanced and $R$: Retarded.    
The last unnamed propagator is the reason why this rotation is advantageous, namely it incorporates the redundancy by vanishing. Inserting the full propagator \eqref{eq:full_propagator_pm} one realises that, for both the advanced and retarded propagators, all the initial state correction terms vanish so they are unchanged by the systems initial state. This is not the case for the Keldysh propagator which means that all the initial state correction information is carried in this propagator.\par As mentioned in the Gaussian integral proof, a unitary transformation does not affect the measure of the integral, in turn the Keldysh rotation amounts renaming the fields variables in the functional integral. 
A continuum field theory can now be written which has the property of giving rise to the same propagators as the discrete representation. Its form is much simpler than the discrete one as the initial state information have been included into the propagators. The wanted form of the action is
\begin{equation}
i\mathcal{G}^{\alpha \beta}_{tt'}=\int \mathcal{D}\left[\bar{\phi}^{cl},\phi^{cl}\right]\mathcal{D}\left[\bar{\phi}^{q},\phi^{q}\right]\phi^{\alpha}(t)\bar{\phi}^{\beta}(t')\exp\left(iS\right)=i\begin{pmatrix}
\mathcal{G}^K_{(tt')}& \mathcal{G}^R_{(tt')}\\
\mathcal{G}^A_{(tt')}&0
\end{pmatrix}.
\end{equation}
With an action that is quadratic in fields on arrives at the form
\begin{equation}
S=\int_{-\infty}^{\infty}i\pmqty{\bar{\phi}^{cl} & \bar{\phi}^q}_t\pmqty{(\mathcal{G}^{-1})^a &(\mathcal{G}^{-1})^b  \\ (\mathcal{G}^{-1})^c & (\mathcal{G}^{-1})^d}_{(tt')}\pmqty{\phi^{cl}\\\phi^{q}}_{t'}dtdt'.
\end{equation}
The four elements of the matrix appearing in the action is the four elements of the inverse propagator matrix. Its elements is found by the relation
\begin{equation}
\begin{aligned}
&\int d\tau\pmqty{\mathcal{G}^K& \mathcal{G}^R\\
	\mathcal{G}^A&0}_{(t\tau)}\pmqty{(\mathcal{G}^{-1})^a &(\mathcal{G}^{-1})^b  \\ (\mathcal{G}^{-1})^c & (\mathcal{G}^{-1})^d}_{(\tau t')}\\&=\int d\tau\pmqty{\mathcal{G}^K_{t\tau}(\mathcal{G}^{-1})^a_{\tau t'}+\mathcal{G}^R_{t\tau}(\mathcal{G}^{-1})^c_{\tau t'}& \mathcal{G}^K_{t\tau}(\mathcal{G}^{-1})^b_{\tau t'}+\mathcal{G}^R_{t\tau}(\mathcal{G}^{-1})^d_{\tau t'}  \\ \mathcal{G}^A_{t\tau}(\mathcal{G}^{-1})^a_{\tau t'}& \mathcal{G}^A_{t\tau}(\mathcal{G}^{-1})^b_{\tau t'}}=\pmqty{1&0\\0&1}\delta(t-t').
\end{aligned}
\end{equation}
These four equations gives the elements of the inverse matrix and the action takes the form
\begin{equation}
\pmqty{(\mathcal{G}^{-1})^a &(\mathcal{G}^{-1})^b  \\ (\mathcal{G}^{-1})^c & (\mathcal{G}^{-1})^d}_{(tt')}=\pmqty{0 &(\mathcal{G}^A)^{-1}  \\ (\mathcal{G}^R)^{-1} & (\mathcal{G}^{-1})^K}_{(tt')}\rightarrow S=\int_{-\infty}^{\infty}i\pmqty{\bar{\phi}^{cl} & \bar{\phi}^q}_t\pmqty{0 &(\mathcal{G}^A)^{-1}  \\ (\mathcal{G}^R)^{-1} & (\mathcal{G}^{-1})^K}_{(tt')}\pmqty{\phi^{cl}\\\phi^{q}}_{t'}dtdt'.
\end{equation}
The two off diagonal elements in the matrix is simply the inverse of the respective propagators while the only non-zero diagonal element is given by
\begin{equation}
\begin{gathered}
\int\mathcal{G}^K_{(t\tau)}(\mathcal{G}^A)^{-1}_{(\tau t')}+\mathcal{G}^R_{(t\tau)}(\mathcal{G}^{-1})^K_{(\tau t')}d\tau=0,\\
\rightarrow (\mathcal{G}^{-1})^K_{(\tau' t')}=-\int (\mathcal{G}^R)^{-1}_{(\tau' t)}\mathcal{G}^K_{(t\tau)}(\mathcal{G}^A)^{-1}_{(\tau t')}dt d\tau.
\end{gathered}
\end{equation}
Dealing with the inverse matrices is simpler in the energy domain as the propagators are only a function of the time separation. This means that one can do the Fourier transform in the variable $t-t'$. This works assuming the Hamiltonian is time-independent and as long as the generating functional is defined along the entire time axis. It is necessary to find a work-around for the second condition to deal with finite initial times. 
The work-around is to define the Fourier transform and its inverse as
\begin{equation}
\begin{gathered}
f(E)=\int_{-\infty}^{\infty}e^{i t(E\pm i\eta )}f(t)dt,\qquad f(t)=\lim\limits_{\eta\to 0^\pm}\frac{1}{2\pi}\int_{-\infty}^{\infty}e^{-i E t}f(E)dE,
\end{gathered}
\end{equation} 
where the energy have been given a infinitesimal imaginary contribution. The $+(-)$ is depending on if $F(t)$ has a non-physical infinite correlation length on the positive (negative) time axis.  
This is necessary because the theory have not had a realistic mechanism that destroys infinite correlation time build into it \cite{Bruus}\footnote{See paragraphs concerning eq. 5.46}. With this definition of the Fourier transform the energy representations of the propagators are well defined
\begin{equation}
\begin{aligned}
i\mathcal{G}^K_{E}&=(1+2\left<n\right>_0)\int_{-\infty}^{\infty}e^{it\left(E-\omega_0\right)}dt=2\pi(1+2\left<n\right>_0)\delta(E-\omega_0),\\
i\mathcal{G}^R_{E}&=\int_{-\infty}^{\infty}\theta(t)e^{it(E-\omega_0+i\eta)}=\frac{i}{E-\omega_0+i\eta},\\
i\mathcal{G}^A_{E}&=-\int_{-\infty}^{\infty}\theta(-t)e^{it(E-\omega_0-i\eta)}=\frac{i}{E-\omega_0-i\eta}.
\end{aligned}
\end{equation}
One important note is that with this definition of the Fourier transform one an use a finite initial time $t_0$ and still consider the energy representation.  
