\section{Current Through Chain}
The main observable of interest is the steady-state current, $I$, through the chain. To compute this quantity we take advantage of the steady state fact and that the chain is 1-dimensional. Due to the number conserving dynamics of the chain itself, excitations can only appear and be removed by the environments. As these are only at the ends of the chain, the steady-state current has to be constant across the entire chain. The steady-state current can then be found, by computing the current in any link between any two sites in the chain. Due to this simplification it is not necessary to worry about the effect of the environments as it is already contained in the reduced chain dynamics. In the opposite case where one has some complex system and the environments are the simplest part of the system, The current can be described by just looking at what goes through one environment \cite{Antti}. \par 
Considering the i'th site, the current is calculated as the change of occupation
\begin{equation}
\begin{aligned}
I_i(t) &= -i\left[H,b_i^\dagger b_i\right](t) \\&=-ih\left[(b_i^\dagger b_{i+1}+b_{i+1}^\dagger b_{i}+b_i^\dagger b_{i-1}+b_{i-1}^\dagger b_{i}),b_i^\dagger b_i\right]  
\end{aligned} 
\end{equation}
Here there is two terms due to the current from the left and two terms that describes the effective current to the right. As these must be equal it is sufficient to consider the current leaving the site (the terms connecting to $i+1$).   
The particle current through the link $i,i+1$ is therefore given by
\begin{equation}\label{eq:current1}
I(t) = -i\left(\left<b_i(t) b_{i+1}^\dagger(t)\right>-\left<b_{i+1}(t)b_i^\dagger(t)\right>\right).
\end{equation}
To compute this using the derived propagators fo the theory, the above expectation values have to be translated into the "Keldysh" fields. We insert the two operators into the  closed time-contour at time $t$. As the two legs are identical the operators can be inserted on either the forward or the backwards one without affecting the result. In the spirit of the Keldysh rotation half of the operator is inserted on each leg. Using the coherent states the excitation current is given by
\begin{equation}
\begin{aligned}
I=&-\frac{i}{2}\bigg(\left<\phi^+_i(t) \bar{\phi}^+_{i+1}(t)\right>+\left<\phi^-_i(t) \bar{\phi}^-_{i+1}(t)\right>\\&-\left<\phi^+_{i+1}(t) \bar{\phi}^+_i(t)\right>-\left< \phi^-_{i+1}(t)\bar{\phi}^-_i(t)\right>\bigg).
\end{aligned}
\end{equation}      
Using the that Keldysh rotation for bosons is involutory and given by \eqref{eq:Keldysh_Rotation} the $+/-$ fields can be written in the $cl/q$ basis
\begin{widetext}
	\begin{equation}
	\begin{aligned}
	I=&-\frac{i}{4}\bigg(\left<\left(\phi^{cl}_i+\phi^{q}_i\right)\left(\bar{\phi}^{cl}_{i+1}+\bar{\phi}^{q}_{i+1}\right)\right>+\left<\left(\phi^{cl}_i-\phi^{q}_i\right)\left(\bar{\phi}^{cl}_{i+1}-\bar{\phi}^{q}_{i+1}\right)\right>-\left<\left(\phi^{cl}_{i+1}+\phi^{q}_{i+1}\right)\left(\bar{\phi}^{cl}_i+\bar{\phi}^{q}_i\right)\right>\\&-\left<\left(\phi^{cl}_{i+1}-\phi^{q}_{i+1}\right)\left(\bar{\phi}^{cl}_i-\bar{\phi}^{q}_i\right)\right>\bigg),\\
	=&-\frac{i}{2}\bigg(\left<\phi_i^{cl}\bar{\phi}_{i+1}^{cl}\right>+\left<\phi_i^{q}\bar{\phi}_{i+1}^{q}\right>-\left<\phi_{i+1}^{cl}\bar{\phi}_i^{cl}\right>-\left<\phi_{i+1}^{q}\bar{\phi}_i^{q}\right>\bigg),
	\end{aligned}
	\end{equation}
\end{widetext}
It was shown that the hopping interaction in the chain only affected the retarded and advanced propagators \eqref{eq:InteractionKeldyshform} which means that quantum-quantum correlations are still zero as expected from the causality structure of the theory. Furthermore the Keldysh propagator is known to be anti-Hermitian which means the current takes the form
\begin{equation}
\begin{aligned}
I&=\frac{-i}{2}\Big(i \mathds{G}^K_{i,i+1}(t,t)-i \mathds{G}^K_{i+1,i}(t,t)\Big),\\
&=\frac{1}{2}\Big(\mathds{G}^K_{i,i+1}(t,t)+ \bar{\mathds{G}}^K_{i,i+1}(t,t)\Big),\\
&=\text{Re}\left\{\mathds{G}^K_{i,i+1}(t,t)\right\},\\
&=\text{Re}\left\{\int_{-\infty}^{\infty}\frac{d\epsilon}{2\pi}\mathds{G}^K_{i,i+1}(\epsilon)\right\}\\&=\int_{-\infty}^{\infty}\frac{d\epsilon}{2\pi}\text{Re}\left\{\mathds{G}^K_{i,i+1}(\epsilon)\right\},\\
\end{aligned}
\end{equation}
The result is therefore that a single element of the Keldysh propagator gives  the current through the link. 