\section{Structure For Effective Partition Function}
When evaluating correlation functions in the interaction picture it leaves the initial state the same. The partition function is therefore still given by 
\begin{equation}
\mathcal{Z} = \Tr U_c \chi_i = \Tr U_{-\infty,\infty}U_{-\infty,\infty} \chi_i,
\end{equation}
where the time evolution $U_{t,t'}$ is evaluated along a closed time contour. The initial state of the system at $-\infty$ is factorising as $\chi_i=\mu_C\otimes\mu_1\otimes\mu_{N_C}$, where $\mu$ is a thermal state of inverse temperature $\beta$ for the chain and the two baths respectively. This initial state is justified by turning on the interaction adiabatically. It is relevant because we are only interested in the steady state current which should be independent of the initial state.\par 
The evolution operator, $U$, contains non-commuting terms and is not normal-ordered. Furthermore it is not given by simply exponentiating the effective Hamiltonian as this is a time-dependent operator. This means that the evolution operator is given by a time-ordered exponential with an integral over $\tilde{H}(t)$ in the argument. This would be the starting point for the operator based Keldysh method.\par In the path integral we avoid these time-ordered exponentials by time slicing\cite{Altland} the evolution. This time slicing is achieved by first employing the composition property \cite{Breuer} of the evolution operator to split the evolution into $N-1$ pieces. This is a general property of an unitary evolution. The length of each time step ($\Delta$) is then proportional to $N-1$ such that their product is a constant. 
Time slicing  means the evolution operator can be represented as the product
\begin{equation}
U(t,t')=\prod_{k=1}^{N-1}U(t'+k\Delta,t'+(k-1)\Delta),\; \Delta=\frac{t-t'}{N-1}.
\end{equation}
Taking the limit with infinitely small time steps and assuming the time dependence of $\tilde{H}(t)$ is continuous, $\tilde{H}(t)$ will look unchanged between infinitesimally separated neighbouring time steps. This means that the form of an infinitesimal evolution becomes
\begin{equation}
\begin{aligned}
\lim\limits_{\Delta\to 0}U(t'+k\Delta,t'+(k-1)\Delta)&=\exp\left[-i\tilde{H}(k\Delta)\Delta\right],\\
&=U(\Delta k),\\
&\approx \mathds{1}-i\tilde{H}(\Delta k)\Delta,
\end{aligned}
\end{equation}
where neglecting the second order term in $\Delta$ is justified by taking the continuum limit.   
Enforcing this time slicing procedure the partition function takes the form
\begin{equation}\label{eq:timesliced_partitionFct}
\mathcal{Z} = \Tr\left[\prod_{k=1}^{N-1}U^-(k\Delta)\prod_{L=N-1}^1 U^+(L\Delta)\chi_i\right],
\end{equation}
where the +/- is referring to evolving forwards or backwards in time respectively. Due to the time slicing procedure each kind of operator is present $N-1$ times.\par
The time ordering have now been taken care of explicitly. \par The coherent states are now useful because the partition function is a product of normal ordered operators. Coherent states then turn the operator into a complex number.  
\par Inserting identity resolutions between each evolution operator in \eqref{eq:timesliced_partitionFct} and using the properties of the coherent states the partition function takes the form
\begin{widetext}
	\begin{equation}
		\begin{aligned}
		\mathcal{Z}=&\int\prod_{i=1}^{N_c}\prod_{n=1}^{N}\prod_{\lambda,k}\left(\dm[^{\pm}_{i,n}]{\phi}\dm[^{\pm}_{\lambda,k,n}]{\theta}\me^{-\abs{\phi_{i,n}^\pm}^2-\abs{\theta_{\lambda,q,n}^\pm}^2}\right)\bra{\tilde{\phi}^-_{i,1}\theta^-_{\lambda,q,1}}U^-(1\Delta)\ket{\tilde{\phi}^-_{i,2}\theta^-_{\lambda,q,2}}\dots\\
		&\times\bra{\tilde{\phi}^-_{i,N-1}\theta^-_{\lambda,q,N-1}}U^-\Big((N-1)\Delta\Big)\ket{\tilde{\phi}^-_{i,N}\theta^-_{\lambda,q,N}}\bra{\tilde{\phi}^-_{i,N}\theta^-_{\lambda,q,N}}\mathds{1}\ket{\tilde{\phi}^+_{i,N}\theta^+_{\lambda,q,N}}\\
		&\times\bra{\tilde{\phi}^+_{i,N}\theta^+_{\lambda,q,N}}U^+\Big((N-1)\Delta\Big)\ket{\tilde{\phi}^+_{i,N-1}\theta^+_{\lambda,q,N-1}}\dots\bra{\tilde{\phi}^+_{i,2}\theta^-_{\lambda,q,2}}U^+(1\Delta)\ket{\tilde{\phi}^+_{i,1}\theta^+_{\lambda,q,1}}\\
		&\times\bra{\phi^+_{i,1}}\mu_C\ket{\phi^-_{i,1}}\bra{\theta^+_{\lambda,q,1}}\mu_1\otimes\mu_{N_c}\ket{\theta^-_{\lambda,q,1}}.
		\end{aligned}
	\end{equation}
\end{widetext}
Due to the normal ordering of all the evolution operators the creation and annihilation operators are turned into complex numbers with the price of having to include the overlaps for the different coherent states. One overlap takes the form
\begin{equation}
\begin{gathered}
\prod_{i,q,\lambda}\bra{\tilde{\phi}^+_{i,n}\theta^+_{\lambda,q,n}}U^+\Big((n-1)\Delta\Big)\ket{\tilde{\phi}^+_{i,n-1}\theta^+_{\lambda,q,n-1}}\\
=\prod_{i,q,\lambda}\exp\bigg[\me^{-i\tilde{\omega}\Delta}\bar{\phi}^+_{ i,n}\phi^+_{i,n-1}+\bar{\theta}^+_{\lambda,q,n}\theta^+_{\lambda,q,n-1}\\-i\Delta \tilde{H}(\Delta(n-1),\phi,\theta)\bigg].
\end{gathered}
\end{equation}
For the backwards leg one finds the conjugate of the above and with time indices reversed. One important thing to notice is that, as all the overlaps are between neighbouring fields in time, the interaction picture phase factor only is only depending on the time step length and not on the specific "absolute" time. \par To find an explicit form for the chain model it is necessary find the effective Hamiltonian. This is straight forward in this using \eqref{eq:time_dep_annihilation}. The only place where these phase factors do not cancel is in the bath-system interactions. This means that the rotating Hamiltonian has the exact same form as \eqref{eq:Schrödinger_Hamiltonian_terms} with the replacement $g_{\lambda,k}\to g_{\lambda,k}(t)=g_{\lambda,k}\me^{-it\tilde{\omega}}$. All the terms which contain two fields belonging to the same system (baths or chain) can be grouped together. This gives a partition function of the form 
\begin{widetext}
	\begin{equation}
	\begin{aligned}
		\mathcal{Z}=&\int\prod_{i=1}^{N_c}\prod_{n=1}^{N}\prod_{\lambda,k}\left(\dm[^{\pm}_{i,n}]{\phi}\dm[^{\pm}_{\lambda,k,n}]{\theta}\right)\exp\Bigg[\sum_{i,j;n,m}\bar{\phi}_{i,n}i\mathcal{G}^{-1}_{i,j;n,m}\phi_{j,m}+\sum_{\lambda,k,n,m}\bar{\theta}_{\lambda,k,n}i\mathcal{D}^{-1}_{\lambda,k,n,m}\theta_{\lambda,k,m}\\&+\sum_{\lambda,k,n}i\Delta \Big(-\bar{g}_{\lambda,k}(n\Delta)\bar{\phi}^{+}_{\lambda,n}\theta^+_{\lambda,k,n-1}-g_{\lambda,k}(n\Delta)\bar{\theta}^{+}_{\lambda,k,n}\phi^+_{\lambda,n-1}+\bar{g}_{\lambda,k}(n\Delta)\bar{\phi}^{-}_{\lambda,n-1}\theta^-_{\lambda,k,n}\\&+g_{\lambda,k}(n\Delta)\bar{\theta}^{-}_{\lambda,k,n-1}\phi^-_{\lambda,n}\Big)\Bigg]\Bigg/\mathcal{N},
	\end{aligned}
	\end{equation}
\end{widetext}
Where it has been used that in the continuum limit it will not matter if $g_{\lambda,k}(t)$ is evaluated at $t=n$ or $t=n-1$.
The normalisation factor
\begin{equation} \mathcal{N}=\prod_{\lambda,k}(1-\me^{-\beta_\lambda\nu_{\lambda,k}})
(1-\me^{-\beta_C \omega_C})^{N_c},
\end{equation}
originates from the thermal initial states. The purely quadratic parts and separable parts for the chain and the environments are contained in $\mathcal{G}$ and $\mathcal{D}$ respectively. $\mathcal{D}$ is just free propagators for the different bath modes \cite{Kamenev} because the environments are internally non-interacting. $\mathcal{G}$ contains the free propagators for each site but also the hopping between different sites. When the limit with infinitely many time slices is taken the partition function can be represented with a continuum form which gives rise to the same propagators as the discrete version \cite{Kamenev}. The sums over $n,m$, together with the different factors of $\Delta$, some of which are hidden inside $\mathcal{G}$ and $\mathcal{D}$, is then turned into integrals over time. This also means that the phase factors originating from the overlaps will not matter as $\me^{\pm i\tilde{\omega}\Delta}\approx1$. Another effect is that for the hopping interaction the time difference between two fields becomes negligible. Neglecting this time difference can in some specific cases give rise to convergence issues, but in that case the appropriate phase can be included in the continuum form \cite{Altland}. The continuum representation of the partition function can be written as
\begin{widetext}
	\begin{equation}
	\begin{split}
	\mathcal{Z} = \int& \prod_{i, \lambda,k}\Dm[^\pm_i]{\phi},\Dm[^\pm_{\lambda,k}]{\theta}\exp\Bigg[\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t i \mathcal{G}^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'}\\&+\int_{-\infty}^{\infty}\dd{t}\dd{t'}\sum_{\lambda,k}\pmqty{\bar{\theta}_{\lambda,k}^+ & \bar{\theta}_{\lambda,k}^-}_t i \mathcal{D}^{-1}_{\lambda,k;t,t'}\pmqty{\theta^+_{\lambda,k} \\ \theta^-_{\lambda,k}}_{t'}\\&+i  \int_{-\infty}^{\infty}\dd{t}\sum_{\lambda,k}\Bigg( g_{\lambda,k}\me^{-it\tilde{\omega}}\pmqty{-\bar{\phi}^+_\lambda & \bar{\phi}^-_\lambda}_t\pmqty{\theta_{\lambda,k}^+  \\\theta_{\lambda,k}^-}_t+\bar{g}_{\lambda,k}\me^{it\tilde{\omega}}\pmqty{\bar{\theta}_{\lambda,k}^+ &\bar{\theta}_{\lambda,k}^-}_t\pmqty{-\phi^+_\lambda \\ \phi^-_\lambda}_t\Bigg)\Bigg]\bigg/ \mathcal{N}.
	\end{split}
	\end{equation}
\end{widetext}
Where the propagators are now on a continuum form. \par As the bath-chain interaction is linear the Gaussian functional integral over the bath fields can be done by finding the inverse of $\mathcal{D}^{-1}$ and its determinant. The determinant directly cancels the normalisation factors from the bath. This is due to our closed time-contour construct and because no differences between forwards and backwards propagation has been introduced. In this case the density operator must keep its normalization such that $\mathcal{Z}=1$. \par The effect of integrating away the baths is an effective partition function which only depends on the chain fields $\mathcal{Z}=\int\prod_{i=1}^{N_c}\Dm[^{\pm}_i]{\phi}\exp\left[i\mathcal{S}\right]/(1-\me^{-\beta_C \omega_C})^{N_c}$ with the action given by
\begin{equation}\label{eq:pmAction}
\begin{split}
\mathcal{S}&=\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t  \mathcal{G}^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'} - \\ &\int \dd{t}\dd{t'}\sum_{\lambda,k}\abs{g_{\lambda,k}}^2 \pmqty{-\bar{\phi}^+_\lambda & \bar{\phi}^-_\lambda}_t\mathcal{D}_{\lambda,k;t,t'}\pmqty{-\phi^+_\lambda \\\phi^-_\lambda}_{t'}\\
&\times\me^{i\tilde{\omega}(t-t')}.
\end{split}
\end{equation}  
As the form of $\mathcal{D}$ is well-known \cite{Kamenev} it is only necessary to explicitly construct $\mathcal{G}$. To do this we consider the pure chain part of the Hamiltonian which $\mathcal{G}$ is constructed by
\begin{equation}
H_c = \sum_{i=1}^{N_c}\omega_i b_i^\dagger b_i+h\sum_{i=1}^{N_c-1}\left(b_i^\dagger b_{i+1}+b_{i+1}^\dagger b_i\right).
\end{equation}
The continuum action, derived using the same Keldysh technique as previously outlined, takes the form
\begin{equation}
\begin{aligned}
&\mathcal{S}_c =\sum_{i,j}\int \dd t\,\dd t' \pmqty{\bar{\phi}_i^+&\bar{\phi}_i^-}_t i \mathcal{G}^{-1}_{0;t,t'}\delta_{i,j}\pmqty{\phi^+_j\\\phi^-_j}_{t'}\\
&+ih\sum_i \int \dd t\left(-\bar{\phi}^+_i\phi^+_{i+1}-\bar{\phi}^+_{i+1}\phi^+_{i}+\bar{\phi}^-_i\phi^-_{i+1}+\bar{\phi}^-_{i+1}\phi^-_{i}\right).
\end{aligned}
\end{equation}  
So far we have been working in the $\pm$ basis with a field residing on each branch of the time contour. As the time contour is closed the ends correlate the two fields and therefore gives rise to an intrinsic redundancy. To handle this redundancy explicitly it is advantageous to change to a basis which mixes the two fields. This change of basis is known as the Keldysh rotation and for a single site it is defined like
\begin{equation}\label{eq:Keldysh_Rotation}
\pmqty{\phi^{cl} \\ \phi^q}=\frac{1}{\sqrt{2}}\pmqty{1 & 1 \\ 1 & -1}\pmqty{\phi^+ \\\phi-} = U_k \pmqty{\phi^+\\\phi^-}.
\end{equation} 
The nomenclature in this basis, for bosonic fields, is that the classical field ($\phi^{cl}$) is the sum while the quantum field ($\phi^q$) is the difference of the two branch fields ($\phi^{\pm}$). With several sites it is defined as a local transformation that only mixes each site. Considering just two sites the rotation takes the matrix form 
\begin{equation}
\label{eq:Keldysh2site}
U_k = \frac{1}{\sqrt{2}}\begin{pmatrix}
1 & 0 & 1 & 0 \\
0 & 1 & 0 & 1 \\
1 & 0 & -1 & 0 \\
0 & 1 & 0 &-1\\
\end{pmatrix}=U_k^\dagger, \quad U_k U_k^\dagger =\mathds{1}.
\end{equation}
Writing the hopping interaction for the same two site system it takes the form
\begin{equation}
\begin{pmatrix}
\phi^+_1 \\
\phi^+_2 \\
\phi^-_1 \\
\phi^-_2\\
\end{pmatrix}^\dagger
\begin{pmatrix}
0 & -1 &0 & 0\\
-1&0&0&0\\
0&0&0&1\\
0&0&1&0\\
\end{pmatrix}\begin{pmatrix}
\phi^+_1 \\
\phi^+_2 \\
\phi^-_1 \\
\phi^-_2\\
\end{pmatrix}=\Phi^\dagger \mathcal{I} \Phi.
\end{equation}
Now applying the Keldysh rotation is done by inserting the identity relation \eqref{eq:Keldysh2site}
\begin{equation}\label{eq:InteractionKeldyshform}
\Phi^\dagger U_k U_k^\dagger\mathcal{I} U_k U_k^\dagger\Phi
=\begin{pmatrix}
\phi^{cl}_1 \\
\phi^{cl}_2 \\
\phi^q_1 \\
\phi^q_2\\
\end{pmatrix}^\dagger
\begin{pmatrix}
0 & 0 &0 & -1\\
0&0&-1&0\\
0&-1&0&0\\
-1&0&0&0\\
\end{pmatrix}\begin{pmatrix}
\phi^{cl}_1 \\
\phi^{cl}_2 \\
\phi^q_1 \\
\phi^q_2\\
\end{pmatrix}.
\end{equation}
It is seen that changing to the Keldysh basis the interaction gives rise to hopping between the sites but to the opposite type of field. So a classical field excitation on a site can excite either of its neighbours quantum fields. This generalises to arbitrary long chain lengths.\\ When the Keldysh rotation is performed on the free propagators then it removes the intrinsic redundancy previously mentioned. This is seen in the action by the fact that the inverse propagators element for $cl-cl$ correlations vanishes. In the Keldysh basis the inverse of the free propagator takes the form \cite{Kamenev}
\begin{equation}
G^{-1}_{t,t'}\delta_{i,j} =\begin{pmatrix}
0 & (G^A)^{-1}_{t,t'}\delta_{i,j}\\
 (G^R)^{-1}_{t,t'}\delta_{i,j} &  (G^{-1})^K_{t,t'}\delta_{i,j}
\end{pmatrix}.
\end{equation}
Because of the anti-diagonal form of the interaction it will only affect the advanced and retarded elements ($A$ and $R$) and leaves the Keldysh element ($K$) unperturbed. Performing the Keldysh rotation on the full action \eqref{eq:pmAction} it takes the form 
\begin{equation}
S=\sum_{i,j}\int \dd t\;\dd t' \pmqty{\bar{\phi}^{cl}_i & \bar{\phi}^q_i}_t\mathds{G}^{-1}_{i,j;t,t'}\pmqty{\phi^{cl}_j\\\phi^q_j}_{t'},
\end{equation}
with the effective inverse propagator $\mathds{G}$ being given by 
\begin{equation}
\mathds{G}^{-1}_{i,j;t,t'}= \pmqty{0 & (G^A_{i,j;t,t'})^{-1}-\Sigma^A_{i,j;t,t'}\\(G^R_{i,j;t,t'})^{-1}-\Sigma^R_{i,j;t,t'}&(G^{-1}_{t,t'})^{K}\delta_{i,j}-\Sigma^K_{i,j;t,t'}}.
\end{equation}  
Here $\Sigma = \sum_{\lambda, k}\abs{g_{\lambda,k}}^2\sigma^x D_{\lambda, k; t,t'}\me^{i\tilde{\omega}(t-t')}\sigma_x$. As all the factors only depend on the time separation $t-t'$ \cite{Kamenev}, Fourier transforming removes one of the time integrals.\todo[inline]{add calculation to appendix} Using the form of the free propagator \todo{given in the above appendix} the self energies $\Sigma$, takes the following form in frequency space
\begin{equation}\label{eq:selfEnergies}
\begin{gathered}
\begin{aligned}
\Sigma_{\epsilon;i,j}^{R/A}&=\Sigma_{\epsilon,1}^{R/A}+\Sigma_{\epsilon,N_c}^{R/A},\\&=\sum_{\lambda=1,N_c}\int_{0}^{\infty}\dd \nu \frac{J_\lambda(\nu)}{\epsilon+\tilde{\omega}-\nu\pm i\eta}\delta_{i,j}\delta_{i,\lambda},
\end{aligned}\\
\begin{aligned}
\Sigma_{\epsilon;i,j}^{K}&=\Sigma_{\epsilon,1}^{K}+\Sigma_{\epsilon,N_c}^{K},\\&=-i2\pi\sum_{\lambda=1,N_c}\int_{0}^{\infty}\dd \nu J_\lambda(\nu)\delta(\epsilon+\tilde{\omega}-\nu)\\&\qquad\times\coth(\nu\mu_\lambda/2)\delta_{i,j}\delta_{i,\lambda},\\
&=-i2\pi\sum_{\lambda}J_\lambda(\epsilon+\tilde{\omega})\coth\Big((\epsilon+\tilde{\omega}) \mu_\lambda/2\Big)\delta_{i,j}\delta_{i,\lambda}.
\end{aligned}
\end{gathered}
\end{equation}
Here it has been assumed that the baths are very large and are dense. This means that the sum over bath modes can be replaced by an integral using the spectral density $J(\epsilon)=\sum_k \abs{g_{k}}^2\delta(\nu_k-\epsilon)$. Assuming the baths are infinitely large further means that the finite chains effect on the baths overall state is negligible. This means that the baths stay in their thermal equilibrium state.\par The chain elements in $\mathds{G}^{-1}$ is found similarly by using the form of the free propagator and the form of the interaction derived previously \eqref{eq:InteractionKeldyshform}. The result is 
\begin{equation}\label{eq:InverseChainPropagators}
(G_{\epsilon;i,j}^{R/A})^{-1}=\left(\epsilon-\omega_c+\tilde{\omega} \pm i\eta\right)\delta_{i,j}-h\left(\delta_{i,j+1}+\delta_{i+1,j}\right),
\end{equation}
To find the inverse Keldysh element for the isolated chain one uses the relation\todo[inline]{prove this relation without using F} 
\begin{equation}\label{eq:Keldyshprop_causality}
\begin{aligned}
(G^{-1})^K&=-(G^R)^{-1}\circ G^K\circ (G^A)^{-1},\\&=2 i \eta \coth(\epsilon \beta_c/2),
\end{aligned}
\end{equation}
The $\circ$ operator is matrix multiplication with a convolution. \todo[inline]{Understand what this convolution thing means in Fourier space} Notice that the this element is infinitesimal. This gives the full inverse propagator for the chain the form
\begin{equation}\label{eq:fullInversePropagator}
\mathds{G}^{-1}_{i,j;\epsilon}= \pmqty{0 & (G^A_{i,j;\epsilon})^{-1}-\Sigma^A_{i,j;\epsilon}\\(G^R_{i,j;\epsilon})^{-1}-\Sigma^R_{i,j;\epsilon}&-\Sigma^K_{i;\epsilon}\delta_{i,j}}.
\end{equation} 
The full retarded and advanced propagators can be found by inverting the two off diagonal blocks. However thanks to the causality structure \cite{Kamenev} of the Keldysh action it is known that they Hermitian conjugates. So finding the retarded propagator immediately gives access to the advanced propagator. The Keldysh propagator is defined by inverse of the above and then identifying its Keldysh element. As this is highly non-trivial one can again take advantage of the causality structure as done in \eqref{eq:Keldyshprop_causality}. The Keldysh propagator is therefore found by
\begin{equation}
\begin{aligned}
\mathds{G}^K_{i,j;\epsilon}=\mathds{G}^R\circ \Sigma^K\circ (\mathds{G}^R)^\dagger=\sum_{l}\mathds{G}_{i,l;\epsilon}^R \Sigma^K_{l;\epsilon}\bar{\mathds{G}}_{j,l;\epsilon}^R,
\end{aligned}
\end{equation}    
where $\Sigma^K_l$ is given by \eqref{eq:selfEnergies} for $l=1,N_c$ and \eqref{eq:Keldyshprop_causality} for all other sites. Due to $\Sigma^K$ being infinitesimal in the middle of the chain the expression for the full Keldysh propagator is reduced to just two terms
\begin{equation}
\begin{aligned}
\mathds{G}^K_{i,j;\epsilon}=\mathds{G}_{i,1;\epsilon}^R \Sigma^K_{1;\epsilon}\bar{\mathds{G}}_{j,1;\epsilon}^R+\mathds{G}_{i,N_c;\epsilon}^R \Sigma^K_{N_c;\epsilon}\bar{\mathds{G}}_{j,N_c;\epsilon}^R,
\end{aligned}
\end{equation}
\par Due to the Keldysh actions structure it is obvious that the only inversion we need to perform, is to invert the retarded block of the full propagator \eqref{eq:fullInversePropagator}. Specifically, for the Keldysh propagator, the only necessary elements are those in the first and last columns. 
\par Letting $\eta\to 0$ define the diagonal element in the retarded propagator as $\mathds{R}^{-1}=\epsilon-\omega_c+\tilde{\omega}$. The matrix representation of the inverse retarded propagator is given in \eqref{eq:inverseRetardedProp}.
Here the three matrix elements have been defined as
\begin{equation}
a\cdot h = \mathds{R}^{-1}-\Sigma^R_1;\; b\cdot h = \mathds{R}^{-1}; \; c \cdot h = \mathds{R}^{-1}-\Sigma^R_{N_c}
\end{equation}
\section{Form of Retarded Propagator}
The form of the inverse retarded propagator is known as a perturbed symmetric tridiagonal Toeplitz form. The perturbation is that the diagonal elements are not constant which makes it a bit more challenging to inverse than the regular symmetric tridiagonal Toeplitz matrix. One approach is to find the matrix elements recursively \cite{Meurant} but seeing that the elements of interest are all in the first an last columns it is much more convenient to us a closed form expression for the elements. This was found in \cite{Meek} with the complication that the solution is depending on the value of the element $b$. This is quite troublesome when the inverse Fourier transform has to be done as we then need to integrate over all values of $\epsilon$ for which the spectral density is non-negligible. At this point the rotating frame becomes useful as it allows us to add an arbitrary constant to $b$.\par Choose $\tilde{\omega}$ such that $\mathds{R}^{-1}>2$ for the range of $\epsilon$ where $J(\epsilon+\tilde{\omega})$ is supported. Here it is assumed that the spectral density is well described by a function with support only for a finite range of $\epsilon$.          
\begin{widetext}
	\begin{equation}\label{eq:inverseRetardedProp}
	(\mathds{G}^R)^{-1} = 
	\begin{pmatrix}
	\mathds{R}^{-1}-\Sigma^R_1 & -h &  & &&&\\
	-h & \mathds{R}^{-1} & -h && && \\
	& -h & \mathds{R}^{-1} & -h &  &\text{\huge{0}}& \\
	&&\ddots& \ddots& \ddots &&\\
	&\text{\huge{0}}& &-h & \mathds{R}^{-1} & -h & \\
	&&& &-h & \mathds{R}^{-1} & -h \\
	&& && & -h & \mathds{R}^{-1}-\Sigma^R_{N_C}
	\end{pmatrix}=h\begin{pmatrix}
	a & -1 &  & &&&\\
	-1 & b & -1 && && \\
	& -1 & b & -1 &  &\text{\huge{0}}& \\
	&&\ddots& \ddots& \ddots &&\\
	&\text{\huge{0}}& &-1 & b & -1 & \\
	&&& &-1 & b & -1 \\
	&& && & -1 & c
	\end{pmatrix}.
	\end{equation}
\end{widetext}
