\section{Structure For Effective Partition Function}
The model under study is defined by the Hamiltonian
\begin{equation}
	\begin{split}
	H =& \sum_{i=1}^{N_c} \omega_i b^\dagger_i b_i + h\sum_{i=1}^{N_c-1}\left(b_i^\dagger b_{i+1}+b_{i+1}^\dagger b_i\right)\\
	&+\sum_{\lambda=1,N_c}\sum_{k}g_{\lambda,k} \left(b_\lambda^\dagger d_{\lambda,k}+d_{\lambda,k}^\dagger b_\lambda\right)\\
	&+\sum_{\lambda,k}\nu_{\lambda,k} d_{\lambda,k}^\dagger d_{\lambda,k},
	\end{split}
\end{equation}
where the $\lambda$ subscript is indicating which end of the chain the specific bath connects to. Due to the bath interaction the system is not in equilibrium so to treat it properly the formulation must be based on the density matrix of  the chain. Keldysh field theory achieves this by defining the partition function 
\begin{equation}
\mathcal{Z} = \Tr U_c \chi_i = \Tr U_{-\infty,\infty}U_{-\infty,\infty} \chi_i,
\end{equation}
where the time evolution $U_{t,t'} =\me^{-i H (t-t')}$ is evaluated along a closed time contour. The initial state of the system at $-\infty$ is factorising as $\chi_i=\mu_C\otimes\mu_1\otimes\mu_{N_C}$, where $\mu$ is thermal state for the chain an the two baths respectively. This initial state is justified by turning on the interaction adiabatically. 
The evolution operator, $U$, contains non-commuting terms and is not normal-ordered. To circumvent this the path integral time slicing approach \cite{Altland} is used. This time slicing is achieved by first employing the composition property of the evolution operator to split the evolution into $N-1$ pieces. The length of each time step ($\Delta$) is then proportional to $N-1$ such that their product is a constant. Taking the limit with infinitely small time steps the exponential can be truncated to first order   
\begin{equation}
\begin{gathered}
U_T = \left(U_\Delta \right)^{N-1}=\left(e^{-iH \Delta}\right)^{N-1},\\=\left(1-i \Delta H+\mathcal{O}(\Delta^2)\right)^{N-1},\\
\underset{N\to\infty}{=}\left(1-i \Delta H\right)^{N-1}.
\end{gathered}
\end{equation}
Enforcing this time slicing procedure the partition function takes the form
\begin{equation}\label{eq:timesliced_partitionFct}
\mathcal{Z} = Tr\left[U^-..U^-U^+..U^+\chi_i\right],
\end{equation}
where the +/- is referring to evolving forwards or backwards in time respectively. Due to the time slicing procedure each kind of operator is present $N-1$ times.\par
The partition function is now a product of time ordered operators. To take advantage of this simplification we use the unnormalised coherent states defined as
\begin{equation}
\ket{\phi} = \me^{\phi a^\dagger}\ket{0},\quad \bra{\theta}\ket{\phi}=\me^{\bar{\theta}\phi},
\end{equation}
where $a^\dagger$ is the appropriate creation operator for the site/mode and $\phi$ is a complex number. These states are useful because they are the right eigenstates of the annihilation operator and the left eigenstate of the creation operator. With a normal-ordered operator they allows us to turn the operator into a complex number. Due to the nature of the coherent states, they constitute an over-complete basis. This complications is negotiated by including a correction factor in the identity resolution \cite{Altland}
\begin{equation}
\mathds{1}=\int\dm{\phi}\me^{-\abs{\phi}^2}\ket{\phi}\bra{\phi}.
\end{equation}  
Here the measure $\dm{\phi}=\frac{d\text{Re}(\phi)\text{Im}(\phi)}{\pi}$ is over the entire complex plane and includes a normalisation factor of $\pi$. \par Inserting identity resolutions between each evolution operator in \eqref{eq:timesliced_partitionFct} and using the properties of the coherent states the partition function takes the form
\begin{widetext}
	\begin{equation}
	\begin{aligned}
		\mathcal{Z}&=\int\prod_{i=1}^{N_c}\prod_{n=1}^{N}\prod_{\lambda,k}\left(\dm[^{\pm}_{i,n}]{\phi}\dm[^{\pm}_{\lambda,k,n}]{\theta}\right)\exp\Bigg[\sum_{i,j;n,m}\bar{\phi}_{i,n}i\mathcal{G}^{-1}_{i,j;n,m}\phi_{j,m}+\sum_{\lambda,k,n,m}\bar{\theta}_{\lambda,k,n}i\mathcal{D}^{-1}_{\lambda,k,n,m}\theta_{\lambda,k,m}\\&+\sum_{\lambda,k,n}i\Delta g_{\lambda,k}\left(-\bar{\phi}^{+}_{\lambda,n}\theta^+_{\lambda,k,n-1}-\bar{\theta}^{+}_{\lambda,k,n}\phi^+_{\lambda,n-1}+\bar{\phi}^{-}_{\lambda,n-1}\theta^-_{\lambda,k,n}+\bar{\theta}^{-}_{\lambda,k,n-1}\phi^-_{\lambda,n}\right)\Bigg]\Bigg/\mathcal{N}.
	\end{aligned}
	\end{equation}
\end{widetext}
The normalisation factor
\begin{equation} \mathcal{N}=\prod_{\lambda,k}(1-\me^{-\mu_\lambda\nu_{\lambda,k}})\prod_{i}(1-\me^{-\mu_C \omega_i}),
\end{equation}
originates from the thermal initial states. The purely quadratic parts and separable parts for the chain and the environments are contained in $\mathcal{G}$ and $\mathcal{D}$ respectively. $\mathcal{D}$ is just free propagators for the different bath modes \cite{Kamenev} because the environments are internally non-interacting. $\mathcal{G}$ contains the free propagators for each site but also the hopping between different sites. When the limit with infinitely many time slices is taken the partition function can be represented with a continuum form which gives rise to the same propagators as the discrete version \cite{Kamenev}. The sums over $n,m$, together with the different factors of $\Delta$, some of which are hidden inside $\mathcal{G}$ and $\mathcal{D}$, is then turned into integrals over time. The continuum representation of the partition function can be written as
\begin{widetext}
	\begin{equation}
	\begin{split}
	\mathcal{Z} = \int& \prod_{i, \lambda,k}\Dm[^\pm_i]{\phi},\Dm[^\pm_{\lambda,k}]{\theta}\exp\Bigg[\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t i \mathcal{G}^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'}\\&+\int_{-\infty}^{\infty}\dd{t}\dd{t'}\sum_{\lambda,k}\pmqty{\bar{\theta}_{\lambda,k}^+ & \bar{\theta}_{\lambda,k}^-}_t i \mathcal{D}^{-1}_{\lambda,k;t,t'}\pmqty{\theta^+_{\lambda,k} \\ \theta^-_{\lambda,k}}_{t'}\\&+i  \int_{-\infty}^{\infty}\dd{t}\sum_{\lambda,k}g_{\lambda,k}\Bigg(\pmqty{-\bar{\phi}^+_\lambda & \bar{\phi}^-_\lambda}_t\pmqty{\theta_{\lambda,k}^+  \\\theta_{\lambda,k}^-}_t+\pmqty{\bar{\theta}_{\lambda,k}^+ &\bar{\theta}_{\lambda,k}^-}_t\pmqty{-\phi^+_\lambda \\ \phi^-_\lambda}_t\Bigg)\Bigg]\bigg/ \mathcal{N}.
	\end{split}
	\end{equation}
\end{widetext}
Where the propagators are now on a continuum form. \par As the bath-chain interaction is linear the Gaussian functional integral over the bath fields can be done by finding the inverse of $\mathcal{D}^{-1}$ and its determinant. The determinant directly cancels the normalisation factors from the bath. This is due to our closed time-contour construct and because no differences between forwards and backwards propagation has been introduced. In this case the density operator must keep its normalization such that $\mathcal{Z}=1$. \par The effect of integrating away the baths is an effective partition function which only depends on the chain fields $\mathcal{Z}=\int\prod_{i=1}^{N_c}\Dm[^{\pm}_i]{\phi}\exp\left[i\mathcal{S}\right]/\prod_i(1-\me^{-\mu_C \omega_i})$ with the action given by
\begin{equation}\label{eq:pmAction}
\begin{split}
\mathcal{S}&=\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t  G^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'}\\& - \int \dd{t}\dd{t'}\sum_{\lambda,k}\abs{g_{\lambda,k}}^2 \pmqty{-\bar{\phi}^+_\lambda & \bar{\phi}^-_\lambda}_tD_{\lambda,k;t,t'}\pmqty{-\phi^+_\lambda \\\phi^-_\lambda}_{t'}.
\end{split}
\end{equation}  
As the form of $\mathcal{D}$ is well-known \cite{Kamenev} it is only necessary to explicitly construct $\mathcal{G}$. To do this we consider the pure chain part of the Hamiltonian which $\mathcal{G}$ is constructed by
\begin{equation}
H_c = \sum_{i=1}^{N_c}\omega_i b_i^\dagger b_i+h\sum_{i=1}^{N_c-1}\left(b_i^\dagger b_{i+1}+b_{i+1}^\dagger b_i\right).
\end{equation}
The continuum action, derived using the same Keldysh technique as previously outlined, for this theory takes the form
\begin{equation}
\begin{aligned}
&\mathcal{S}_c =\sum_{i,j}\int \dd t\,\dd t' \pmqty{\bar{\phi}_i^+&\bar{\phi}_i^-}_t i \mathcal{G}^{-1}_{0;t,t'}\delta_{i,j}\pmqty{\phi^+_j\\\phi^-_j}_{t'}\\
&+ih\sum_i \int \dd t\left(-\bar{\phi}^+_i\phi^+_{i+1}-\bar{\phi}^+_{i+1}\phi^+_{i}+\bar{\phi}^-_i\phi^-_{i+1}-\bar{\phi}^-_{i+1}\phi^-_{i}\right).
\end{aligned}
\end{equation}  
So far we have been working in the $\pm$ basis with a field residing on each branch of the time contour. As the time contour is closed the ends correlate the two fields and therefore gives rise to an intrinsic redundancy. To handle this redundancy explicitly it is advantageous to change to a basis which mixes the two fields. This change of basis is known as the Keldysh rotation and for a single site it is defined like
\begin{equation}
\pmqty{\phi^{cl} \\ \phi^q}=\frac{1}{\sqrt{2}}\pmqty{1 & 1 \\ 1 & -1}\pmqty{\phi^+ \\\phi-} = U_k \pmqty{\phi^+\\\phi^-}.
\end{equation} 
The nomenclature in this basis, for bosonic fields, is that the classical field ($\phi^{cl}$) is the sum while the quantum field ($\phi^q$) is the difference of the two branch fields ($\phi^{\pm}$). With several sites it is defined as a local transformation that only mixes each site. Considering just two sites the rotation takes the matrix form 
\begin{equation}
\label{eq:Keldysh2site}
U_k = \frac{1}{\sqrt{2}}\begin{pmatrix}
1 & 0 & 1 & 0 \\
0 & 1 & 0 & 1 \\
1 & 0 & -1 & 0 \\
0 & 1 & 0 &-1\\
\end{pmatrix}=U_k^\dagger, \quad U_k U_k^\dagger =\mathds{1}.
\end{equation}
Writing the hopping interaction for the same two site system it takes the form
\begin{equation}
\begin{pmatrix}
\phi^+_1 \\
\phi^+_2 \\
\phi^-_1 \\
\phi^-_2\\
\end{pmatrix}^\dagger
\begin{pmatrix}
0 & -1 &0 & 0\\
-1&0&0&0\\
0&0&0&1\\
0&0&1&0\\
\end{pmatrix}\begin{pmatrix}
\phi^+_1 \\
\phi^+_2 \\
\phi^-_1 \\
\phi^-_2\\
\end{pmatrix}=\Phi^\dagger \mathcal{I} \Phi.
\end{equation}
Now applying the Keldysh rotation is done by inserting the identity relation \eqref{eq:Keldysh2site}
\begin{equation}\label{eq:InteractionKeldyshform}
\Phi^\dagger U_k U_k^\dagger\mathcal{I} U_k U_k^\dagger\Phi
=\begin{pmatrix}
\phi^{cl}_1 \\
\phi^{cl}_2 \\
\phi^q_1 \\
\phi^q_2\\
\end{pmatrix}^\dagger
\begin{pmatrix}
0 & 0 &0 & -1\\
0&0&-1&0\\
0&-1&0&0\\
-1&0&0&0\\
\end{pmatrix}\begin{pmatrix}
\phi^{cl}_1 \\
\phi^{cl}_2 \\
\phi^q_1 \\
\phi^q_2\\
\end{pmatrix}.
\end{equation}
It is seen that changing to the Keldysh basis the interaction gives rise to hopping between the sites but to the opposite type of field. So a classical field excitation on a site can excite either of its neighbours quantum fields. This generalises to arbitrary long chain lengths.\\ When the Keldysh rotation is performed on the free propagators then it removes the intrinsic redundancy previously mentioned. This is seen in the action by the fact that the inverse propagators element for $cl-cl$ correlations vanishes. In the Keldysh basis the inverse of the free propagator takes the form \cite{Kamenev}
\begin{equation}
G^{-1}_{t,t'}\delta_{i,j} =\begin{pmatrix}
0 & (G^A)^{-1}_{t,t'}\delta_{i,j}\\
 (G^R)^{-1}_{t,t'}\delta_{i,j} &  (G^{-1})^K_{t,t'}\delta_{i,j}
\end{pmatrix}.
\end{equation}
Because of the anti-diagonal form of the interaction it will only affect the advanced and retarded elements ($A$ and $R$) and leaves the Keldysh element ($K$) unperturbed. Performing the Keldysh rotation on the full action \eqref{eq:pmAction} it takes the form 
\begin{equation}
S=\sum_{i,j}\int \dd t\;\dd t' \pmqty{\bar{\phi}^{cl}_i & \bar{\phi}^q_i}_t\mathds{G}^{-1}_{i,j;t,t'}\pmqty{\phi^{cl}_j\\\phi^q_j}_{t'},
\end{equation}
with the effective inverse propagator $\mathds{G}$ being given by 
\begin{equation}
\mathds{G}^{-1}_{i,j;t,t'}= \pmqty{0 & (G^A_{i,j;t,t'})^{-1}-\Sigma^A_{i,j;t,t'}\\(G^R_{i,j;t,t'})^{-1}-\Sigma^R_{i,j;t,t'}&(G^{-1}_{t,t'})^{K}\delta_{i,j}-\Sigma^K_{i,j;t,t'}}.
\end{equation}  
Here $\Sigma = \sum_{\lambda, k}\abs{g_{\lambda,k}}^2\sigma^x D_{\lambda, k; t,t'}\sigma_x$. As all the factors only depend on the time separation $t-t'$ \cite{Kamenev}, Fourier transforming removes one of the time integrals.\todo[inline]{add calculation to appendix} Using the form of the free propagator \todo{given in the above appendix} the self energies $\Sigma$, takes the following form in frequency space
\begin{equation}\label{eq:selfEnergies}
\begin{gathered}
\begin{aligned}
\Sigma_{\epsilon;i,j}^{R/A}&=\Sigma_{\epsilon,1}^{R/A}+\Sigma_{\epsilon,N_c}^{R/A},\\&=\sum_{\lambda=1,N_c}\int_{0}^{\infty}\dd \nu \frac{J(\nu)}{\epsilon-\nu\pm i\eta}\delta_{i,j}\delta_{i,\lambda},
\end{aligned}\\
\begin{aligned}
\Sigma_{\epsilon;i,j}^{K}&=\Sigma_{\epsilon,1}^{K}+\Sigma_{\epsilon,N_c}^{K},\\&=-i2\pi\sum_{\lambda=1,N_c}\int_{0}^{\infty}\dd \nu J(\nu)\delta(\epsilon-\nu)\\&\qquad\times\coth(\nu\mu_\lambda/2)\delta_{i,j}\delta_{i,\lambda},\\
&=-i2\pi\sum_{\lambda}J(\epsilon)\coth(\epsilon \mu_\lambda/2)\delta_{i,j}\delta_{i,\lambda}.
\end{aligned}
\end{gathered}
\end{equation}
Here it has been assumed that the baths are very large and are dense. This means that the sum over bath modes can be replaced by an integral using the spectral density $J(\epsilon)=\sum_k g_{k}\delta(\nu_k-\epsilon)$. Having no $\lambda$ index on $J$ means that we have assumed that the two baths couples the same way to the sites and have an identical DOS. The parameter which we use to distinguish the baths is their temperature. Assuming the baths are infinitely large further means that the finite chains effect on the baths overall state is negligible. This means that the baths stay in their thermal equilibrium state.\par The chain elements in $\mathds{G}^{-1}$ is found similarly by using the form of the free propagator and the form of the interaction derived previously \eqref{eq:InteractionKeldyshform}. The result is 
\begin{equation}\label{eq:InverseChainPropagators}
(G_{\epsilon;i,j}^{R/A})^{-1}=\left(\epsilon-\omega_c \pm i\eta\right)\delta_{i,j}-h\left(\delta_{i,j+1}+\delta_{i+1,j}\right),
\end{equation}
where we have assumed the chain has no disorder ($\omega_i=\omega_c$ $\forall\;i$). To find the inverse Keldysh element for the isolated chain one uses the relation\todo{prove this relation without using F} 
\begin{equation}\label{eq:Keldyshprop_causality}
\begin{aligned}
(G^{-1})^K&=-(G^R)^{-1}\circ G^K\circ (G^A)^{-1},\\&=2 i \eta \coth(\epsilon \beta_c/2),
\end{aligned}
\end{equation}
The $\circ$ operator is matrix multiplication with a convolution. \todo{Understand what this convolution thing means in Fourier space} This element is infinitesimal so it can be neglected. This gives the full inverse propagator for the chain the form
\begin{equation}\label{eq:fullInversePropagator}
\mathds{G}^{-1}_{i,j;\epsilon}= \pmqty{0 & (G^A_{i,j;\epsilon})^{-1}-\Sigma^A_{i,j;\epsilon}\\(G^R_{i,j;\epsilon})^{-1}-\Sigma^R_{i,j;\epsilon}&-\Sigma^K_{\epsilon}\delta_{i,j}}.
\end{equation} 
The full retarded and advanced propagators can be found by inverting the two off diagonal blocks. However thanks to the causality structure \cite{Kamenev} of the Keldysh action it is known that they Hermitian conjugates. So finding the retarded propagator immediately gives access to the advanced propagator. The Keldysh propagator is defined by inverse of the above and then identifying its Keldysh element. As this is highly non-trivial one can again take advantage of the causality structure as done in \eqref{eq:Keldyshprop_causality}. The Keldysh propagator is therefore found by
\begin{equation}
\mathds{G}^K=\mathds{G^R}\circ \Sigma^K\delta_{i,j}\circ \mathds{G^R}^\dagger.
\end{equation}    
Thanks to the Keldysh action structure it is obvious that the only inversion we need to perform, is to invert the retarded block of the full propagator \eqref{eq:fullInversePropagator}.
\par The matrix representation of the inverse retarded propagator is found to be on a perturbed symmetric Toeplitz form
%\begin{widetext}
%	\begin{equation}
%	(\mathds{G}^R)^{-1} = \begin{pmatrix}
%	(G^R)^{-1}-\Sigma^R_1 & -h & 0 & \dots & \dots&\dots&0\\
%	-h & (G^R)^{-1} & -h &0 & \dots&\dots&\vdots \\
%	0& -h & (G^R)^{-1} & -h & 0 &\dots& \vdots\\
%	\vdots &0& \ddots& \ddots& \ddots &&\vdots\\
%	\vdots&\vdots& 0&-h & (G^R)^{-1} & -h &0 \\
%	\vdots&\vdots&0&0 &-h & (G^R)^{-1} & -h \\
%	0 & \dots&\dots &\dots& 0 & -h & (G^R)^{-1}-\Sigma^R_{N_C}
%	\end{pmatrix}
%	\end{equation} 
%\end{widetext}
\begin{widetext}
	\begin{equation}
	(\mathds{G}^R)^{-1} = \begin{pmatrix}
	(G^R)^{-1}-\Sigma^R_1 & -h &  & &&&\\
	-h & (G^R)^{-1} & -h && && \\
	& -h & (G^R)^{-1} & -h &  &\text{\huge{0}}& \\
	 &&\ddots& \ddots& \ddots &&\\
	&\text{\huge{0}}& &-h & (G^R)^{-1} & -h & \\
	&&& &-h & (G^R)^{-1} & -h \\
	 && && & -h & (G^R)^{-1}-\Sigma^R_{N_C}
	\end{pmatrix}
	\end{equation} 
\end{widetext}