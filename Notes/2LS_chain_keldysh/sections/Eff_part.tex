\section{Structure For Effective Partition Function}
The structure of the model for which an effective action is wanted is the given by the Hamiltonian
\begin{equation}
	\begin{split}
	H =& \sum_{i=1}^{N_c} \omega_i b^\dagger_i b_i + J\sum_{i=1}^{N_c-1}\left(b_i^\dagger b_{i+1}+b_{i+1}^\dagger b_i\right)\\
	&+U\sum_{i=1}^{N_c}b_i^{\dagger\, 2} b_i^2 +g \left(b^\dagger_s d+d^\dagger b_s\right)+\nu d^\dagger d,
	\end{split}
\end{equation}
where the $s$ subscript is indicating a specific site in the chain and only a single bath mode is considered. This is sufficient because the baths are non-interacting both among each other but also between internal modes. The challenging features of this model as it is both interacting with an environment and has a non-quadratic term (the U term) which has to be taken to infinity for a proper modelling of the 2LS anti-commutation relations.
Due to the bath interaction the system is not in equilibrium. To treat it properly the formulation must therefore  be based on density matrix of chain. KFT does exactly this by defining the partition function 
\begin{equation}
\mathcal{Z} = \Tr U_c \chi_i = \Tr U_{-\infty,\infty}U_{-\infty,\infty} \chi_i,
\end{equation}
where the time evolution $U_{t,t'} =\me{-i H (t-t')}$ is evaluated along a closed time contour. As this evolution operator both contains non-commuting terms and is not normal-ordered the composition property is used to Trotter expand it
\begin{equation}
U_T = \left(U_\Delta \right)^{N-1}=\left(1-i \Delta H+\mathcal{O}(\Delta^2)\right)^{N-1},
\end{equation}
where the the evolution time has been split into $N-1$ pieces such that $T=\Delta(N-1)$. Using a tiny time steps, meaning $\Delta \rightarrow0$, the second order terms can be neglected. Noticing that $H$ is defined to be normal ordered we insert an identity resolution in coherent states between each time step. 
To make the result exact one needs to take the $N\to\infty$ limit. Taking this limit we arrive at a partition function with the form
\begin{widetext}
	\begin{equation}
	\begin{split}
	\mathcal{Z} = \int \prod_{i=1}^{N_c}\Dm[^\pm_i]{\phi},\Dm[^\pm]{\theta}\exp\Bigg[&\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t i G^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'}+i U \sum_i \int_{-\infty}^\infty \dd{t} \Big(\bar{\phi}^{-\,2}_i(t)\phi^{-\,2}_i(t) 
	\\&-\bar{\phi}^{+\,2}_i(t)\phi^{+\,2}_i(t)\Big)+i g \int_{-\infty}^{\infty}\dd{t}\Bigg(\pmqty{-\bar{\phi}^+_s & \bar{\phi}^-_s}_t\pmqty{\theta^+ \\ \theta^-}_t+\pmqty{\bar{\theta}^+ & \bar{\theta}^-}_t\pmqty{-\phi^+_s \\ \phi^-_s}_t\Bigg)\\&+\int_{-\infty}^{\infty}\dd{t}\dd{t'}\pmqty{\bar{\theta}^+ & \bar{\theta}^-}_t i D^{-1}_{t,t'}\pmqty{\theta^+ \\ \theta^-}_{t'}\Bigg]\bigg/ \prod_{i=1}^{N_c}(1-\kappa_{c,i})(1-\kappa_B).
	\end{split}
	\end{equation}
\end{widetext}
As the bath-chain interaction is linear the Gaussian functional integral over the bath field can be done immediately leaving a partition function which only depends on the chain fields $\mathcal{Z}=\int\prod_{i=1}^{N_c}\Dm[^{\pm}_i]{\phi}\exp\left[iS^{\pm}\right]/\prod_i(1-\kappa_{c,i})$ with the action given by
\begin{equation}
\begin{split}
S^{\pm}&=\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^+_i& \bar{\phi}^-_i}_t  G^{-1}_{i,j;t,t'}\pmqty{\phi^+_j \\ \phi^-_j}_{t'}\\&+ U \sum_i \int_{-\infty}^\infty \dd{t} \Big(\bar{\phi}^{-\,2}_i(t)\phi^{-\,2}_i(t) 
-\bar{\phi}^{+\,2}_i(t)\phi^{+\,2}_i(t)\Big)
\\& -\abs{g}^2 \int \dd{t}\dd{t'} \pmqty{-\bar{\phi}^+_s & \bar{\phi}^-_s}_tD_{t,t'}\pmqty{-\phi^+_s \\\phi^-_s}_{t'}.
\end{split}
\end{equation}  
To get rid of the intrinsic correlation between the fields one different legs, due to the boundary conditions, we do a unitary transformation of the field basis. As the transformation is unitary it has a unit Jacobian and thereby not affecting the functional integral. The so-called Keldysh rotation for bosonic fields is usually defined by $\phi^{cl}=\left(\phi^++\phi^-\right)/\sqrt{2}$ and  $\phi^{q}=\left(\phi^+-\phi^-\right)/\sqrt{2}$. This rotation results in the action
\begin{equation}
\begin{split}
S^{K}=&\int_{-\infty}^{\infty}\dd{t}\, \dd{t'}\sum_{i,j}\pmqty{\bar{\phi}^{cl}_i& \bar{\phi}^{q}_i}_t  \mathds{G}^{-1}_{i,j;t,t'}\pmqty{\phi^{cl}_j \\ \phi^{q}_j}_{t'}\\&- U \sum_i \int_{-\infty}^\infty \dd{t} \Big(\bar{\phi}^{q\,2}_i(t)\phi^{q}_i(t)\phi^{cl}_i(t)+c.c.\\&
+\bar{\phi}^{cl\,2}_i(t)\phi^{cl}_i(t)\phi^{q}_i(t)+c.c.\Big)
\\& -\abs{g}^2 \int \dd{t}\dd{t'} \pmqty{\bar{\phi}^{cl}_s & \bar{\phi}^{q}_s}_t\sigma_x \mathds{D}_{t,t'}\sigma_x\pmqty{\phi^{cl}_s \\\phi^{q}_s}_{t'}.
\end{split}
\end{equation}
 
