\section{Partition Function}
The usual contour \cite{FreeBoson} is used to set up the partition function 
\begin{equation}
\mathcal{Z}=\Tr\left[ \left(U_\mathcal{C}\right)^\dagger U_\mathcal{C} \chi_0\right],
\end{equation}
where $U_\mathcal{C}$ is the time evolution operator given by the Hamiltonian along the contour $\mathcal{C}$. The coherent states for the system and the single environment mode are denoted $\ket{\phi}$ and $\ket{\theta}$ respectively. 
Trotter expanding the time evolution operator
\begin{equation}
U(T)=(U(\Delta))^N=\Bigr(e^{-iH_0\Delta}e^{-iaH_E\Delta}e^{-ia^\dagger H_E\Delta}+\mathcal{O}(\Delta^2)\Bigl)^N,
\end{equation}
one can insert coherent state resolution identities for the closed system
\begin{equation}
\mathds{1}=\int \dc{\phi}\dc{\theta} e^{-\abs{\phi}^2-\abs{\theta}^2}\dyad{\phi}\otimes\dyad{\theta}.
\end{equation}
The partition function then takes the form
\begin{equation}
\begin{aligned}
\mathcal{Z}=\int\prod_{k=0}^N&\dc[^{\pm}_k]{\phi}\dc[^{\pm}_k]{\theta}e^{-\abs{\phi^{\pm}_k}-\abs{\theta^\pm_k}}\bra{\phi_0^-,\theta_0^-}U^-(\Delta)\ket{\theta_1^-,\phi_1^-}\bra{\phi_1^-,\theta_1^-}U^-(\Delta)\ket{\theta_2^-,\phi_2^-}...\\&\times\bra{\phi_N^-,\theta_N^-}\mathds{1}\ket{\theta_N^+,\phi_N^+}...\bra{\phi^+_2,\theta^+_2}U^+(\Delta)\ket{\theta^+_1,\phi_1^+}\bra{\phi_1^+,\theta_1^+}U^+(\Delta)\ket{\theta_0^+,\phi_0^+}\\
&\times\bra{\theta_0^+,\phi_0^+}\rho_0 \otimes\mu_\beta\ket{\theta_0^-,\phi_0^-}.
\end{aligned}
\end{equation}
Having used the Trotter splitting and conditioned on that the limit $N\to\infty$ is taken the factors in the partition takes the form
\begin{equation}
\begin{aligned}
\bra{\phi_k^-,\theta_k^-}U^-(\Delta)\ket{\theta_{k+1}^-,\phi_{k+1}^-}=\exp\Bigl[&i\Delta\left(\omega_0\bar{\phi}_k^-\phi_{k+1}^-+\nu \bar{\theta}_k^-\theta_{k+1}^-\right)+i\Delta g\theta_{k+1}^-\left(\bar{\phi}_{k}^-+\phi^-_{k+1}\right)\\&+i\Delta g\bar{\theta}_{k}^-\left(\bar{\phi}_{k}^-+\phi^-_{k+1}\right)\Bigr],\\
\bra{\phi_{k+1}^+,\theta_{k+1}^+}U^+(\Delta)\ket{\theta_{k}^+,\phi_{k}^+}=\exp\Bigl[&-i\Delta\left(\omega_0\bar{\phi}_{k+1}^+\phi_{k}^++\nu \bar{\theta}_{k+1}^+\theta_{k}^+\right)-i\Delta \bar{g}\theta_{k}^+\left(\bar{\phi}_{k+1}^++\phi^+_{k}\right)\\&-i\Delta \bar{g}\bar{\theta}_{k+1}^+\left(\bar{\phi}_{k+1}^++\phi^+_{k}\right)\Bigr].
\end{aligned}
\end{equation}
The initial state overlap takes the form 
\begin{equation}
\bra{\phi_0^+,\theta_0^+}\rho_0\otimes\mu_\beta\ket{\theta_0^-,\phi_0^-}=\bra{\phi^+_0}\rho_0\ket{\phi^-_0}\times\frac{\exp\left[\kappa \bar{\phi}_0^+\phi_0^-\right]}{1-\kappa}, \quad\kappa=e^{-\beta \nu}.
\end{equation}
Inserting this into the partition function one finds
\begin{equation}
\begin{aligned}
\mathcal{Z}=\int\prod_{k=0}^N&\dc[_k^\pm]{\phi}\dc[_k^\pm]{\theta}\exp\Biggl[\sum_{l,j}
\begin{pmatrix}
\bar{\phi}^+_l \bar{\phi}^-_l
\end{pmatrix}
 i G^{-1}_{0,(l,j)}
 \begin{pmatrix}
 \phi^+_j \\
 \phi^-_j
 \end{pmatrix}
+
 \begin{pmatrix}
 \bar{\theta}^+_l \bar{\theta}^-_l
 \end{pmatrix}
 i D^{-1}_{0,(l,j)}
 \begin{pmatrix}
 \theta^+_j \\
 \theta^-_j
 \end{pmatrix}
 \Biggr]\\&\times\exp\Biggl[i\Delta\sum_{j=0}^N g\begin{pmatrix}
 \bar{\phi}^+_{j+1}+\phi^+_j, \bar{\phi}^-_j+\phi^-_{j+1}
 \end{pmatrix}
 \begin{pmatrix}
 -\theta_j^+\\
 \theta^-_{j+1}
 \end{pmatrix}
 +\bar{g}\begin{pmatrix}
 -\bar{\theta}_{j+1}^+,
 \bar{\theta}^-_{j}
 \end{pmatrix}\begin{pmatrix}
 \bar{\phi}^+_{j+1}+\phi^+_j \\ \bar{\phi}^-_j+\phi^-_{j+1}
 \end{pmatrix}
 \Biggr]/\Tr\mu_\beta,
\end{aligned}
\end{equation}
where $G^{-1}_0\;(D^{-1}_0)$ is the free propagator for the system (environment) and $\Tr \mu_\beta$ is the normalisation of the environment thermal state.\par
Thanks to the previous derivation \cite{FreeBoson} we know a continuum form of the two free propagators so all that is left is the interaction. Taking the continuum limit means that $\Delta\sum=\int_{-\infty}^{\infty}$ and that the difference between neighbouring field configurations becomes vanishingly small such that we can approximate them as being identical
\begin{equation}
\begin{gathered}
\mathcal{Z}=\int\Dc[^\pm]{\phi}\Dc[^\pm]{\theta}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
\bar{\phi}^+,  \bar{\phi}^-
\end{pmatrix}_t
i G^{-1}_{0,(t,t')}
\begin{pmatrix}
\phi^+ \\
\phi^-
\end{pmatrix}_{t'} +\begin{pmatrix}
\bar{\theta}^+,  \bar{\theta}^-
\end{pmatrix}_t
i D^{-1}_{0,(t,t')}
\begin{pmatrix}
\theta^+ \\
\theta^-
\end{pmatrix}_{t'}\right]\\
\times\exp\left[i\int_{-\infty}^{\infty}g\begin{pmatrix}
\bar{\phi}^++\phi^+, \bar{\phi}^-+\phi^-
\end{pmatrix}_t
\begin{pmatrix}
-\theta^+\\
\theta^-
\end{pmatrix}_t
+\bar{g}\begin{pmatrix}
-\bar{\theta}^+,
\bar{\theta}^-
\end{pmatrix}_t\begin{pmatrix}
\bar{\phi}^++\phi^+ \\ \bar{\phi}^-+\phi^-
\end{pmatrix}_t\right]/(1-e^{-\beta\nu}).
\end{gathered}
\end{equation} 
Now the environment can be integrated away as  it is a Gaussian functional integral of the form
\begin{equation}
\begin{aligned}
I[\bar{J},J]&=\int \Dc[(t)]{z}\exp\left[-\int_{-\infty}^{\infty}\dd t\,\dd t' \bar{\textbf{z}}(t)\textbf{M}(t,t')\textbf{z}(t')+\int_{-\infty}^{\infty}\bar{\textbf{z}}(t)\cdot\textbf{J}(t)+\bar{\textbf{J}}(t)\cdot\textbf{z}(t)\right]\\
&=\frac{\exp\left[\int_{-\infty}^{\infty}\dd t\,\dd t' \bar{\textbf{J}}(t)\textbf{M}^{-1}(t,t')\textbf{J}(t')\right]}{\det M}.
\end{aligned}
\end{equation}
Using this previously derived result \cite{FreeBoson} in its functional form the partition function takes the form
\begin{equation}
\begin{aligned}
\mathcal{Z}=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
\bar{\phi}^+,  \bar{\phi}^-
\end{pmatrix}_t
i G^{-1}_{0,(t,t')}
\begin{pmatrix}
\phi^+ \\
\phi^-
\end{pmatrix}\right]\\
&\qquad\times\exp\left[-\abs{g}^2 i\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
-(\phi^+ +\bar{\phi}^+),\phi^-+\bar{\phi}^-
\end{pmatrix}_t
 D_{0,(t,t')}
\begin{pmatrix}
-(\phi^++\bar{\phi}^+) \\
\phi^-+\bar{\phi}^-
\end{pmatrix}_{t'}\right].
\end{aligned}
\end{equation}
To get rid of the intrinsic correlations between the four different propagators we use the Keldysh rotation defined as \cite{Altland}
\begin{equation}
K=\frac{1}{\sqrt{2}}\pmqty{1 &1  \\ 1 & -1}=K^{-1}, \quad\pmqty{\phi_{cl}  \\ \phi_q }=K\pmqty{\phi+\\\phi^-},\quad KGK^{-1}=\mathbb{G}.
\end{equation} 
Using that it is its own inverse we can insert identities between all products
\begin{equation}
\begin{aligned}
\mathcal{Z}=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
\bar{\phi}^{cl},  \bar{\phi}^q
\end{pmatrix}_t
i \mathbb{G}^{-1}_{0,(t,t')}
\begin{pmatrix}
\phi^{cl} \\
\phi^q
\end{pmatrix}\right]\\
&\qquad\times\exp\left[-\abs{g}^2 i\int_{-\infty}^{\infty}\dd t\dd t'
(-)\begin{pmatrix}
\phi^{q} +\bar{\phi}^q,\phi^{cl}+\bar{\phi}^{cl}
\end{pmatrix}_t
\mathbb{D}_{0,(t,t')}
(-)\begin{pmatrix}
\phi^{q}+\bar{\phi}^{q} \\
\phi^{cl}+\bar{\phi}^{cl}
\end{pmatrix}_{t'}\right],\\
=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
\bar{\phi}^{cl},  \bar{\phi}^q
\end{pmatrix}_t
i \mathbb{G}^{-1}_{0,(t,t')}
\begin{pmatrix}
\phi^{cl} \\
\phi^q
\end{pmatrix}\right]\\
&\qquad\times\exp\left[-\abs{g}^2 i\int_{-\infty}^{\infty}\dd t\dd t'
\begin{pmatrix}
\phi^{cl} +\bar{\phi}^{cl},\phi^{q}+\bar{\phi}^{q}
\end{pmatrix}_t
\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
\begin{pmatrix}
\phi^{cl}+\bar{\phi}^{cl} \\
\phi^{q}+\bar{\phi}^{q}
\end{pmatrix}_{t'}\right],
\end{aligned}
\end{equation}
where $\sigma_1$ is the first Pauli matrix. This is a good time reintroduce all the the other environment modes
\begin{equation}
\begin{aligned}
\mathcal{Z}=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
\bar{\phi}^{cl},  \bar{\phi}^q
\end{pmatrix}_t
i \mathbb{G}^{-1}_{0,(t,t')}
\begin{pmatrix}
\phi^{cl} \\
\phi^q
\end{pmatrix}\right]\\
&\qquad\times\exp\left[-i\int_{-\infty}^{\infty}\dd t\dd t'
\mathscr{R}e\left\{\begin{pmatrix}
\phi^{cl},\;\phi^{q}
\end{pmatrix}_t\right\}
\sum_j\abs{g_j}^2 \sigma_1\mathbb{D}_{j,0,(t,t')}\sigma_1
\mathscr{R}e\left\{\begin{pmatrix}
\phi^{cl} \\
\phi^{q}
\end{pmatrix}_{t'}\right\}\right].
\end{aligned}
\end{equation}
Now trouble appears. The difficulty is that the environment factor only contains the real part of the fields which means, that for the Gaussian integral to be solvable, the matrix can be complex but it must be symmetric! This is not case (see \cite{FreeBoson}) as in the continuum form the retarded and advanced propagators are not identical. The question is therefore how to approximate such a Gaussian integral in an analytic way? One approach is to change basis of the entire problem. If instead of working in the annihilation/creation operator basis one were working in position and momentum basis then $\mathds{D}$ would have been symmetric. However this would generate other problems. One would not be able to integrate out the momentum field, as is usually done, as the occupation number operator in that basis takes a form like $a^\dagger a\propto x^2+p^2$. A lot of difficulties would therefore be related to finding such values. Similarly this basis would be very challenging when one wants to include more interesting features like non-linearity ($a^\dagger a^\dagger a a$). It therefore seems to be a better approach to stay in the ladder operator picture and find a way to deal with this non-excitation conserving nature in an approximate way. The most helpful approximation would be to approximate the baths retarded and advanced propagators as equal. The physical impact of this approximation seems however very uncontrolled. \par Studying literature did not give any insight. Here the Rotating Wave Approximation (neglect the non-conserving terms) is either performed \cite{Chakraborty} or they work in the position-momentum basis and integrate the momentum away \cite{Kamenev}. The usually approach for non-perturbative methods is the Hubbard-Stratonovich transformation however this only does the very specific task of changing a four operator interaction into a bilinear form by introducing an axillary field. As the current problem is related to a two operator interaction HS does not seem useful.
This problem is most likely the reason why the Rabi model is categorised as non-integrable. It is however a bit unexpected that there is no literature on approximate methods beyond the JC model.   
%\begin{equation}
%\begin{aligned}
%\mathcal{Z}=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
%\bar{\phi}^{cl},  \bar{\phi}^q
%\end{pmatrix}_t
%i \mathbb{G}^{-1}_{0,(t,t')}
%\begin{pmatrix}
%\phi^{cl} \\
%\phi^q
%\end{pmatrix}\right]\\
%&\times\exp\left[-\abs{g}^2 i\int_{-\infty}^{\infty}\dd t\dd t'
%(-)\begin{pmatrix}
%\phi^{q} +\bar{\phi}^q,\phi^{cl}+\bar{\phi}^{cl}
%\end{pmatrix}_t
%\mathbb{D}_{0,(t,t')}
%(-)\begin{pmatrix}
%\phi^{q}+\bar{\phi}^{q} \\
%\phi^{cl}+\bar{\phi}^{cl}
%\end{pmatrix}_{t'}\right],\\
%=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
%\bar{\phi}^{cl},  \bar{\phi}^q
%\end{pmatrix}_t
%i \mathbb{G}^{-1}_{0,(t,t')}
%\begin{pmatrix}
%\phi^{cl} \\
%\phi^q
%\end{pmatrix}\right]\\
%&\times\exp\left[-\abs{g}^2 i\int_{-\infty}^{\infty}\dd t\dd t'
%\begin{pmatrix}
%\phi^{cl} +\bar{\phi}^{cl},\phi^{q}+\bar{\phi}^{q}
%\end{pmatrix}_t
%\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
%\begin{pmatrix}
%\phi^{cl}+\bar{\phi}^{cl} \\
%\phi^{q}+\bar{\phi}^{q}
%\end{pmatrix}_{t'}\right],
%\end{aligned}
%\end{equation}
%\begin{equation}
%\begin{aligned}
%\mathcal{Z}=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
%\bar{\phi}^{cl},  \bar{\phi}^q
%\end{pmatrix}_t\biggl(
%i \mathbb{G}^{-1}_{0,(t,t')}-i\sum_i \abs{g_i}^2 \sigma_1 \mathbb{D}_{0,i,(t,t')}\sigma_1\biggr)
%\begin{pmatrix}
%\phi^{cl} \\
%\phi^q
%\end{pmatrix}\right]\\
%&\times\exp\biggl[-\sum_i\abs{g_i}^2 i\int_{-\infty}^{\infty}\dd t\dd t'
%\begin{pmatrix}
%\phi^{cl} ,\phi^{q}
%\end{pmatrix}_t
%\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
%\begin{pmatrix}
%\phi^{cl} \\
%\phi^{q}
%\end{pmatrix}_{t'}+\begin{pmatrix}
%\phi^{cl},\phi^{q}
%\end{pmatrix}_t
%\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
%\begin{pmatrix}
%\bar{\phi}^{cl} \\
%\bar{\phi}^{q}
%\end{pmatrix}_{t'}
%\\&\qquad\quad+\begin{pmatrix}
%\bar{\phi}^{cl},\bar{\phi}^{q}
%\end{pmatrix}_t
%\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
%\begin{pmatrix}
%\bar{\phi}^{cl} \\
%\bar{\phi}^{q}
%\end{pmatrix}_{t'}
%\biggr],\\
%=&\int\Dc[^\pm]{\phi}\exp\left[\int_{-\infty}^{\infty}\dd t\dd t'\begin{pmatrix}
%\bar{\phi}^{cl},  \bar{\phi}^q
%\end{pmatrix}_t\biggl(
%i \mathbb{G}^{-1}_{0,(t,t')}-i\Gamma_{t,t'}\biggr)
%\begin{pmatrix}
%\phi^{cl} \\
%\phi^q
%\end{pmatrix}\right]\mathcal{W}(\{\bar{\phi}^\pm,\phi^\pm\}).
%\end{aligned}
%\end{equation} 
%$\Gamma$ is a self-energy due to single excitations hopping between system and environment. $\mathcal{W}$ contains the effect of double hopping, which is due to our interaction not being excitation preserving. The first exponential factor can be solved exactly at this point as it is a simple Gaussian integral. The main challenge is therefore the $\mathcal{W}$ factor. 
%\begin{equation}
%\begin{aligned}
%\mathcal{W}=&\int\Dc[^\pm]{\phi}\exp\Biggl[-\sum_i\abs{g_i}^2 i\int_{-\infty}^{\infty}\dd t\dd t'
%\begin{pmatrix}
%\phi^{cl} ,\phi^{q}
%\end{pmatrix}_t
%\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
%\begin{pmatrix}
%\phi^{cl} \\
%\phi^{q}
%\end{pmatrix}_{t'}\\&\qquad+\begin{pmatrix}
%\phi^{cl},\phi^{q}
%\end{pmatrix}_t
%\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
%\begin{pmatrix}
%\bar{\phi}^{cl} \\
%\bar{\phi}^{q}
%\end{pmatrix}_{t'}
%+\begin{pmatrix}
%\bar{\phi}^{cl},\bar{\phi}^{q}
%\end{pmatrix}_t
%\sigma_1\mathbb{D}_{0,(t,t')}\sigma_1
%\begin{pmatrix}
%\bar{\phi}^{cl} \\
%\bar{\phi}^{q}
%\end{pmatrix}_{t'}\Biggr].
%\end{aligned}
%\end{equation}   